
-- 诺石伙伴
create table `partner` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) DEFAULT NULL comment '公司名称',
  `thumb_path` varchar(255) default null comment '缩略图路径',
  `address` varchar(255) default null comment '地址',
  `phone` varchar(255) default null comment '电话',
  `create_time` datetime DEFAULT NULL comment '创建时间',
  `update_time` datetime DEFAULT NULL comment '修改时间',
  `is_delete` int(11) DEFAULT NULL comment '是否删除：0未删除、1删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 资料表
CREATE TABLE `materials` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL comment '视频标题',
  `thumb_path` varchar(255) default null comment '缩略图路径',
  `user_id` bigint(20) comment '发布人id',
  `brief` text default null comment '资料摘要',
  `content` text,
  `publish_date` datetime DEFAULT NULL comment '发布时间',
  `create_time` datetime DEFAULT NULL comment '创建时间',
  `update_time` datetime DEFAULT NULL comment '修改时间',
  `is_delete` int(11) DEFAULT NULL comment '是否删除：0未删除、1删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 视频表
CREATE TABLE `video` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL comment '视频标题',
  `path` varchar(255) DEFAULT NULL comment '视频地址',
  `sort` int(11) DEFAULT NULL comment '排序：值小排在前面',
  `publish_date` datetime DEFAULT NULL comment '发布时间',
  `create_time` datetime DEFAULT NULL comment '创建时间',
  `update_time` datetime DEFAULT NULL comment '修改时间',
  `is_delete` int(11) DEFAULT NULL comment '是否删除：0未删除、1删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER table `video` comment '视频表';

-- 分销商
CREATE TABLE `agent` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `agent_name` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `detail_address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `is_delete` int(11) DEFAULT NULL,
  `legal_person` varchar(255) DEFAULT NULL,
  `linkMan` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `qq` varchar(255) DEFAULT NULL,
  `telphone` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `distcrit_id` bigint(20) default null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE agent comment '分销商';


-- 图片轮换
CREATE TABLE `banner` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `is_delete` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE banner comment '图片轮换';


-- 企业信息
CREATE TABLE `company_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) default null comment '标题',
  `info_type` varchar(255) DEFAULT NULL,
  `content` text,
  `copyright` varchar(255) default null comment '版权',
  `record_no` varchar(255) default null comment '备案号',
  `phone` varchar(255) default null comment '联系电话',
  `mobile` varchar(255) default null comment '手机号码',
  `company_address` varchar(255) default null comment '公司地址',
  `zipcode` varchar(255) default null comment '邮编',
  `qq` varchar(255) default null comment 'QQ',
  `email` varchar(255) default null comment '邮箱',
  `sina_weibo` varchar(255) default null comment '新浪微博',
  `weixin` varchar(255) default null comment '微信公众号',
  `two_dimensional_code` varchar(255) default null comment '二维码图片路径',
  `statistics_code` text comment '统计代码',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE company_info comment '企业信息';


-- 企业动态
CREATE TABLE `company_news` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) default null comment '新闻标题',
  `show_front` int(11) default 0 comment '首页显示：0不显示、1显示',
  `thumb_path` varchar(255) default null comment '缩略图路径',
  `user_id` bigint(20) comment '发布人id',
  `brief` text default null comment '文章摘要',
  `content` text,
  `create_time` datetime DEFAULT NULL,
  `is_delete` int(11) DEFAULT NULL,
  `publishdate` datetime DEFAULT NULL comment '发布时间',
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE company_news comment '企业动态';

-- 行业新闻
CREATE TABLE `industory_news` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` text,
  `create_time` datetime DEFAULT NULL,
  `is_delete` int(11) DEFAULT NULL,
  `publishdate` datetime DEFAULT NULL,
  `reporter` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE industory_news comment '行业新闻';

-- 登录用户
CREATE TABLE `north_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL comment '姓名',
  `account` varchar(255) default null comment '账号',
  `status` int(11) default 0 comment '用户状态：0启用、1停用',
  `role_id` bigint(20),	
  `update_time` datetime DEFAULT NULL comment '更新时间',
  `create_time` datetime DEFAULT NULL comment '创建时间',
  `is_delete` int(11) DEFAULT NULL comment '是否删除：0未删除、1已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE north_user comment '用户表';


-- 角色
CREATE TABLE `north_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) DEFAULT NULL comment '角色名称',
  `admin` int(11) DEFAULT 0 comment '是否是超级管理员：0不是、1是',
  `update_time` datetime DEFAULT NULL comment '更新时间',
  `create_time` datetime DEFAULT NULL comment '创建时间',
  `is_delete` int(11) DEFAULT NULL comment '是否删除：0未删除、1已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `north_role` comment '角色表';

-- 菜单
CREATE TABLE menu_module (
  `id` bigint(20) NOT NULL,
  `menu_name` varchar(255) DEFAULT NULL comment '菜单名称',
  `url` varchar(255) DEFAULT NULL comment 'url',
  `sort` int(11),
  `is_leaf` bit default 0 comment '是否是叶子：0不是、1是',
  `parent_id` bigint(20) DEFAULT 0 comment '父菜单',  
  `update_time` datetime DEFAULT NULL comment '更新时间',
  `create_time` datetime DEFAULT NULL comment '创建时间',
  `is_delete` int(11) DEFAULT NULL comment '是否删除：0未删除，1已删除',
  `type` int(11) DEFAULT NULL comment '菜单类型：1后台菜单、2前台菜单',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



ALTER TABLE menu_module comment '菜单表';


-- 角色菜单关系表
CREATE TABLE `module_role` (
	`menu_id` BIGINT(20),
	`role_id` BIGINT(20),
	PRIMARY KEY (`menu_id`, `role_id`)
)	ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE module_role comment '角色菜单关系表';


-- 店铺
CREATE TABLE `shop` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `shop_name` varchar(255) DEFAULT NULL comment '店铺名称',
  `detail_address` varchar(255) DEFAULT NULL,
  `distcrit_id` bigint(20) default null comment '所属区、县，可往上逆推市、省', 
  `market` varchar(255) DEFAULT NULL,
  `telphone` varchar(255) DEFAULT NULL comment '创建时间',
  `zipcode` varchar(255) DEFAULT NULL comment '邮编',
  `description` varchar(255) DEFAULT NULL,
  `audit_opinion` varchar(255) DEFAULT NULL comment '审核意见',
  `pass` int(11) DEFAULT NULL comment '审核是否通过：0待审核、1通过、2未通过',
  `manager_name` varchar(255) default null comment '店长姓名',
  `mobile_phone` varchar(255) default null comment '店长手机',
  `agent_id` bigint(20) DEFAULT NULL comment '分销商ID',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL comment '更新时间',
  `is_delete` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK35DAF61C737272` (`agent_id`),
  CONSTRAINT `FK35DAF61C737272` FOREIGN KEY (`agent_id`) REFERENCES `agent` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE shop comment '店铺';


-- 省、市、区 级联表
CREATE TABLE `s_province` (
  `ProvinceID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ProvinceName` varchar(50) DEFAULT NULL comment '省份名称',
  `DateCreated` datetime DEFAULT NULL comment '创建时间',
  `DateUpdated` datetime DEFAULT NULL comment '更新时间',
  PRIMARY KEY (`ProvinceID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE s_province comment '省';


CREATE TABLE `s_city` (
  `CityID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CityName` varchar(50) DEFAULT NULL comment '城市名称',
  `ZipCode` varchar(50) DEFAULT NULL comment '邮编',
  `ProvinceID` bigint(20) DEFAULT NULL comment '省份ID',
  `DateCreated` datetime DEFAULT NULL comment '创建时间',
  `DateUpdated` datetime DEFAULT NULL comment '更新时间',
  PRIMARY KEY (`CityID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE s_city comment '市';


CREATE TABLE `s_district` (
  `DistrictID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DistrictName` varchar(50) DEFAULT NULL comment '区、县名称',
  `CityID` bigint(20) DEFAULT NULL comment '城市ID',
  `DateCreated` datetime DEFAULT NULL comment '创建时间',
  `DateUpdated` datetime DEFAULT NULL comment '更新时间',
  PRIMARY KEY (`DistrictID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE s_district comment '区、县表';
