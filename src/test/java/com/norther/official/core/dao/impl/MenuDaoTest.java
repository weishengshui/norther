//package com.norther.official.core.dao.impl;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Set;
//
//import org.junit.*;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import com.norther.official.core.dao.IMenuDao;
//import com.norther.official.core.entity.MenuModule;
//import com.norther.official.test.TestBase;
//
//public class MenuDaoTest extends TestBase {
//	
//	@Autowired
//	private IMenuDao menuDao;
//	
//	private MenuModule parent = new MenuModule();
//	
//	@Before
//	public void setUp() {
//		parent.setId(0l);
//		parent.setType(1);
//	}
//	
//	@Test
//	public void testGetMenusByParent() {
//		getMenusByParent();
//		getMenusByParent();
//	}
//	
//	private void getMenusByParent() {
//		List<MenuModule> menuModules = menuDao.getMenusByParent(parent);
//		iteratorMenus(menuModules);
//	}
//	
//	private void iteratorMenus(List<MenuModule> menuModules) {
//		for(MenuModule menuModule : menuModules) {
//			logger.info(menuModule.getMenuName());
//			logger.info("------------------------");
//			if(!menuModule.getIsLeaf()) {
//				Set<MenuModule> children = menuModule.getChildren();
//				if(children != null && !children.isEmpty()) {
//					iteratorMenus(new ArrayList<MenuModule>(children));
//				}
//			}
//		}
//	}
//
//}
