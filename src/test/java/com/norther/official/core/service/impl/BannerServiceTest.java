package com.norther.official.core.service.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.norther.official.core.entity.Banner;
import com.norther.official.core.service.IBannerService;
import com.norther.official.test.TestBase;

public class BannerServiceTest extends TestBase {
	
	@Autowired
	private IBannerService bannerService;
	
	private Banner banner;
	
	@Before
	public void setUp() throws Exception {
		banner = new Banner();
		banner.setLink("http://www.baidu.com");
		banner.setPath("/tmp/northor/file/111.jpg");
		banner.setSort(2);
		banner.setTitle("大数据");
	}

	@Test
	public void testAddBanner() {
		bannerService.saveBanner(banner);
		System.out.println(banner.getId());
		Assert.assertNotNull(banner.getId());
	}

}
