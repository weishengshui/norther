package com.norther.official.core.service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;

import com.norther.official.core.dto.NorthUserDto;
import com.norther.official.core.entity.NorthUser;
import com.norther.official.core.service.IUserService;
import com.norther.official.test.TestBase;

/**
 * user service test
 * 
 * @author sean
 * 
 */
public class UserServiceTest extends TestBase {

	@Autowired
	private IUserService userService;
	private NorthUserDto dto;
	private NorthUser user;

	@Before
	public void setUp() throws Exception {
		dto = new NorthUserDto();

		user = new NorthUser();
		user.setAccount("zhangsan");
		user.setIsDelete(0);
		user.setPassword("123456");
		user.setUsername("张三");
		user.setStatus(0);
	}

	@Test
	public void testPageQuery() {
		PageRequest pageable = new PageRequest(0, 10, Direction.DESC,
				"updateTime");

		Page<NorthUser> result = userService.pageQuery(dto, pageable);
		assertTrue(result.getTotalElements() > 0);
	}

	@Test
	public void testSaveUser() {
		user = userService.saveUser(user);
		assertNotNull(user.getId());
	}

	@Test
	public void testFindUserForLogin() {
		String account = "admin";
		String password = "123456";
		NorthUser user = userService.findUserForLogin(account, password);
		assertNotNull(user);
	}

	@Test
	public void testFindUserById() {

		Long id = 1l;
		NorthUser user = userService.findUserById(id);
		assertNotNull(user);
	}

	@Test
	public void testChangeUserPassword() {

		Long id = 1l;
		NorthUser user = userService.findUserById(id);
		String oldPassword = user.getPassword();
		userService.changeUserPassword(id, "654321");
		user = userService.findUserById(id);
		String newPassword = user.getPassword();

		assertFalse(oldPassword.equals(newPassword));
	}

	@Test
	public void testDeleteUsers() {

		user.setAccount("lisi");
		userService.saveUser(user);
		Long id = user.getId();
		userService.deleteUsers(new Long[] { id });

		// 删除之后，校验
		user = userService.findUserById(id);
		assertNull(user);
	}

}
