//package com.norther.official.core.service.impl;
//
//import java.util.Date;
//
//import org.junit.Assert;
//import org.junit.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import com.norther.official.core.entity.IndustoryNews;
//import com.norther.official.core.service.IIndustoryNewsService;
//import com.norther.official.test.TestBase;
//
//public class IndustoryNewsServiceTest extends TestBase {
//	
//	@Autowired
//	private IIndustoryNewsService industoryNewsService;
//	
//	@Test
//	public void testSaveIndustoryNews() throws Exception {
//		
//		IndustoryNews news = new IndustoryNews();
//		news.setContent("11");
//		news.setPublishdate(new Date());
//		news.setReporter("张三");
//		news.setTitle("重大新闻");
//		
//		industoryNewsService.saveIndustoryNews(news);
//		
//		Assert.assertTrue("张三".equals(news.getReporter()));
//	}
//
//}
