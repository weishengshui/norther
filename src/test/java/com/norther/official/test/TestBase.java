package com.norther.official.test;

import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.norther.official.config.AppConfig;
import com.norther.official.config.WebAppConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppConfig.class, WebAppConfig.class })
@WebAppConfiguration
public class TestBase {

	protected Logger logger = LoggerFactory.getLogger(getClass());
}
