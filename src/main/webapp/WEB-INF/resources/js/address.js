/**
 * 省、市、区级联
 */

/**
 * 省下拉选择框初始化
 */
var provinceSelectorInit = function(provinceSelectorId) {
	$.ajax({
		url : 'address/getProvinces.do',
		type : 'GET',
		dataType : 'json',
		async : true,
		success : function(data) {
			if (data && data.length > 0) {
				$('#' + provinceSelectorId).children('option:gt(0)').remove();
				for (var i = 0, length = data.length; i < length; i++) {
					var province = data[i];
					var optionHtml = "<option value='" + province.id + "'>"
							+ province.provinceName + "</option>";
					$('#' + provinceSelectorId).append(optionHtml);
				}
			}
		}
	});
};

/**
 * 当省份改变时
 */
var onProvinceSelectorChange = function(provinceId, citySelectorId,
		districtSelectorId) {
	$('#' + citySelectorId).children('option:gt(0)').remove();
	$('#' + districtSelectorId).children('option:gt(0)').remove();
	if (provinceId != '') {
		$.ajax({
			url : 'address/getCities.do',
			type : 'GET',
			data : {
				provinceId : provinceId
			},
			dataType : 'json',
			async : true,
			success : function(data) {
				if (data && data.length > 0) {
					for (var i = 0, length = data.length; i < length; i++) {
						var city = data[i];
						var optionHtml = "<option value='" + city.id + "'>"
								+ city.cityName + "</option>";
						$('#' + citySelectorId).append(optionHtml);
					}
				}
			}
		});
	}
};

/**
 * 当城市改变时
 */
var onCitySelectorChange = function(cityId, districtSelectorId) {
	$('#' + districtSelectorId).children('option:gt(0)').remove();
	if (cityId != '') {
		$.ajax({
			url : 'address/getDistricts.do',
			type : 'GET',
			data : {
				cityId : cityId
			},
			dataType : 'json',
			async : true,
			success : function(data) {
				if (data && data.length > 0) {
					for (var i = 0, length = data.length; i < length; i++) {
						var district = data[i];
						var optionHtml = "<option value='" + district.id + "'>"
								+ district.districtName + "</option>";
						$('#' + districtSelectorId).append(optionHtml);
					}
				}
			}
		});
	}
};