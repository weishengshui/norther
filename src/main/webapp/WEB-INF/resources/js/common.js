/**
 * 获取区域列表
 */
var reciveRegions = function() {
	var regions = [];
	$.ajax({
		url: 'getRegions.do',
		type: 'post',
		dataType: 'json', 
		async: false,
		contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
		success: function(data) {
			regions = data;
		}		
	});
	return regions;
};

/**
 * 获取代理商的店铺
 */
var reciveStores = function() {
	var stores = [];
	$.ajax({
		url: 'getStores.do',
		type: 'post',
		dataType: 'json', 
		async: false,
		contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
		success: function(data) {
			stores = data;
		}		
	});
	return stores;
};

function isEmail(strEmail) {
	if (strEmail
			.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1) {
		return true;
	} else {
		return false;
	}
}

function getTableLoadPic(colspan, info) {
	var col, contnent;
	col = colspan != undefined ? colspan : 9;
	con = info == undefined ? "正在加载，请等待..." : info;
	var _tr = "<tr><td colspan=" + col
			+ " align=\"center\"><div class=\"jiazai\"><span>" + con
			+ "</span></div></td></tr>";
	return _tr;
}

function getLoadPicDiv(info) {
	var contnent;
	con = info == undefined ? "正在处理，请稍候..." : info;
	var _tr = "<div class=\"crm_loading\"><span>" + con + "</span></div>";
	return _tr;
}

// 判断是否是汉字、字母组成
function checkZwOrLetter(str) {
	var reg = /((^[\u4E00-\u9FA5]{2,10}$)|(^[a-zA-Z]+[\s\.]?([a-zA-Z]+[\s\.]?){0,4}[a-zA-Z]$))/;
	str = str.replace(/^\s+/g, "");
	str = str.replace(/\s+$/g, "");
	if (str.length != 0) {
		if (reg.test(str))
			return true;
	}
	return false;
}

function checkZw(str) {
	var reg = /^[\u4E00-\u9FA5()]{2,50}$/;
	str = str.replace(/^\s+/g, "");
	str = str.replace(/\s+$/g, "");
	if (str.length != 0) {
		if (reg.test(str))
			return true;
	}
	return false;
}

function checkChineseAndBracket(str) {
	var reg = /(?=.*\(.*\)|.*（.*）)^[\u4e00-\u9fa5()（）]*$|^[\u4e00-\u9fa5]*$/;
	str = str.replace(/^\s+/g, "");
	str = str.replace(/\s+$/g, "");
	if (str.length != 0) {
		if (reg.test(str))
			return true;
	}
	return false;
}

// 判断用户添加账户名
function checkUser(val) {
	var filter = /^\s*[.A-Za-z0-9_-]{5,30}\s*$/;
	if (!filter.test(val)) {
		return false;
	}
	return true;
}

// 验证客户账户名称
function checkCustomerUser(val) {
	var filter = /^[0-9a-zA-Z\u4e00-\u9fa5\.@_]+$/;
	if (!filter.test(val)) {
		return false;
	}
	return true;
}

// 判断输入的字符是否为双精度
function IsDouble(val) {
	var reg = /^[-\+]?\d+(\.\d+)?$/;
	if (!reg.test(val))
		return false;
	return true;
}

function checkMobile(sMobile) {
	if (/^13\d{9}$/g.test(sMobile) || (/^15[0-35-9]\d{8}$/g.test(sMobile))
			|| (/^17[06-8]\d{8}$/g.test(sMobile))
			|| (/^18[0-9]\d{8}$/g.test(sMobile))) {
		return true;
	} else {
		return false;
	}
}

function checkTelCode(telCode) {
	if (!(/^0[0-9]{2,3}$/.test(telCode))) {
		return false;
	}
	return true;
}
// /^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$/
function checkTelPhone(phone) {
	if (!(/^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$/.test(phone))) {
		return false;
	}
	return true;
}

/*
 * function checkUrl(v) { return !isEmpty(v) &&
 * /^(http|https|ftp):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?/i.test(v); }
 */

function checkUrl(str_url) {
	var strRegex = "^((https|http|ftp|rtsp|mms)?://)"
			+ "?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?" // ftp的user@
			+ "(([0-9]{1,3}.){3}[0-9]{1,3}" // IP形式的URL- 199.194.52.184
			+ "|" // 允许IP和DOMAIN（域名）
			+ "([0-9a-z_!~*'()-]+.)*" // 域名- www.
			+ "([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]." // 二级域名
			+ "[a-z]{2,6})" // first level domain- .com or .museum
			+ "(:[0-9]{1,4})?" // 端口- :80
			+ "((\s?)|" // a slash isn't required if there is no file name
			+ "(/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+/?)$";
	var re = new RegExp(strRegex);
	if (re.test(str_url)) {
		return (true);
	} else {
		return (false);
	}
}

function isFax(str) {
	if (null == str) {
		return false;
	}
	return /^\d+-+\d+([;|；]\d+-+\d+)*$/.test(str);
}

function isFax1(str) {
	if (null == str) {
		return false;
	}
	return /^\d+([;|；]\d+-+\d+)*$/.test(str);
}

// 验证图片 gif jpg格式， 300k以内
var img = null;
function isImg(obj) {
	if (null == obj || obj.value.length == 0 || /^\s*$/.test(obj.value)) {
		return true;
	}
	if (!/(.jpg|.JPG|.gif|.GIF)$/.test(obj.value)) {
		alert("\u56fe\u50cf\u5fc5\u987b\u662fjpg\u6216gif\u683c\u5f0f");
		return false;
	}
	if (img) {
		img.removeNode(true);
	}
	img = document.createElement("img");
	img.attachEvent("onreadystatechange", checkSize);
	// img.attachEvent("onerror",alert("图片 "+obj.value+" 不能打开！"));
	img.src = obj.value;
	return true;
}

function checkImg(filePathName) {
	var array = filePathName.split('.');
	var ext = array[1].toLowerCase();
	var typeArray = new Array("jpg", "jpeg", "gif", "png", "rar", "zip");
	for ( var i = 0; i < typeArray.length; i++) {
		if (ext == typeArray[i])
			return true;
	}
	return false;
}

function checkSize() {
	if (img.readyState != "complete") {
		return false;
	}
	if (Math.round(img.fileSize / 1024) > 300) {
		alert("\u56fe\u50cf\u5927\u5c0f\u4e0d\u80fd\u8d85\u8fc7300KB\uff01");
		return false;
	}
	return true;
}

function isEmpty(value) {
	return (value == null || trim(value) == "");
}

function trim(strSrc) {
	if (strSrc == null) {
		return "";
	}
	return strSrc.replace(/(^\s+)|(\s+$)/g, "");
}

Date.prototype.pattern = function(fmt) {
	var o = {
		"M+" : this.getMonth() + 1, // 月份
		"d+" : this.getDate(), // 日
		"h+" : this.getHours() % 12 == 0 ? 12 : this.getHours() % 12, // 小时
		"H+" : this.getHours(), // 小时
		"m+" : this.getMinutes(), // 分
		"s+" : this.getSeconds(), // 秒
		"q+" : Math.floor((this.getMonth() + 3) / 3), // 季度
		"S" : this.getMilliseconds()
	// 毫秒
	};
	var week = {
		"0" : "\u65e5",
		"1" : "\u4e00",
		"2" : "\u4e8c",
		"3" : "\u4e09",
		"4" : "\u56db",
		"5" : "\u4e94",
		"6" : "\u516d"
	};
	if (/(y+)/.test(fmt)) {
		fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
				.substr(4 - RegExp.$1.length));
	}
	if (/(E+)/.test(fmt)) {
		fmt = fmt
				.replace(
						RegExp.$1,
						((RegExp.$1.length > 1) ? (RegExp.$1.length > 2 ? "\u661f\u671f"
								: "\u5468")
								: "")
								+ week[this.getDay() + ""]);
	}
	for ( var k in o) {
		if (new RegExp("(" + k + ")").test(fmt)) {
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
					: (("00" + o[k]).substr(("" + o[k]).length)));
		}
	}
	return fmt;
}

function showEditDialog(id, width, height) {
	$('#' + id).dialog({
		bgiframe : true,
		modal : true,
		width : width,
		height : height,
		title : '',
		closeText : 'hide',
		dialogClass : 'alert',
		show : 'slide',
		autoOpen : true,
		hide : 'slide',
		show : 'slide'

	});
}
// 关闭弹出框 type表示释放指定类型
function closeDialog(id) {
	$("#" + id).dialog("close");
	releaseButton();
}

// 打开弹出框
function openDialog(id, title, width, height) {
	$('#' + id).dialog({
		bgiframe : true,
		modal : true,
		width : width,
		height : height,
		title : title,
		closeText : 'hide',
		dialogClass : 'alert',
		show : 'slide',
		autoOpen : true,
		hide : 'slide',
		show : 'slide'
	});
}

function dialogAddButton(id, title, width, height) {
	$("#" + id).dialog({
		bgiframe : true,
		modal : true,
		width : width,
		height : height,
		title : title,
		autoOpen : false,
		show : 'slide',
		buttons : {
			'确认所选' : function() {
				$("#" + id).dialog('close');
			}
		}
	});
	$("#" + id).dialog('open');
}

// 角色
function operate(id, deep) {
	$("#roleId").val(id);
	$("#deep").val(deep);
	if (deep == 0) {
		$("#delbutton").attr("disabled", true);
		$("#editPurview").attr("disabled", true);
	} else {
		$("#delbutton").attr("disabled", false);
		$("#editPurview").attr("disabled", false);
	}
}

function blockButton() {
	$("#addbutton").attr("disabled", true);
	$("#delbutton").attr("disabled", true);
}

function releaseButton() {
	$("#addbutton").attr("disabled", false);
	$("#delbutton").attr("disabled", false);
}

function check_number(value) {
	var reg = /^\d+$/;
	if (value.constructor === String) {
		if (value.match(reg))
			return true;
	}
	return false;
}
function check_word(value) {
	var reg = /^\w+$/;
	if (value.constructor === String) {
		var re = value.match(reg);
		return true;
	}
	return false;

}
function check_phone(value) {
	var reg = /^(\d{3,4})-(\d{7,8})/;
	if (value.constructor === String) {
		var re = value.match(reg);
		return true;
	}
	return false;
}

// 打印
function preview(oper) {
	if (!isEmpty(oper)) {
		bdhtml = window.document.body.innerHTML;// 获取当前页的html代码
		sprnstr = "<!--startprint" + oper + "-->";// 设置打印开始区域
		eprnstr = "<!--endprint" + oper + "-->";// 设置打印结束区域
		prnhtml = bdhtml.substring(bdhtml.indexOf(sprnstr) + 18); // 从开始代码向后取html

		prnhtmlprnhtml = prnhtml.substring(0, prnhtml.indexOf(eprnstr));// 从结束代码向前取html
		window.document.body.innerHTML = prnhtml;
		window.print();
		window.document.body.innerHTML = bdhtml;
	} else {
		window.print();
	}
}

// 数字格式化
function FormatNumber(srcStr, nAfterDot)// nAfterDot小数位数
{
	var srcStr, nAfterDot;
	var resultStr, nTen;
	srcStr = "" + srcStr + "";
	strLen = srcStr.length;
	dotPos = srcStr.indexOf(".", 0);
	if (dotPos == -1) {
		resultStr = srcStr + ".";
		for (i = 0; i < nAfterDot; i++) {
			resultStr = resultStr + "0";
		}
		return resultStr;
	} else {
		if ((strLen - dotPos - 1) >= nAfterDot) {
			nAfter = dotPos + nAfterDot + 1;
			nTen = 1;
			for (j = 0; j < nAfterDot; j++) {
				nTen = nTen * 10;
			}
			resultStr = Math.round(parseFloat(srcStr) * nTen) / nTen;
			return resultStr;
		} else {
			resultStr = srcStr;
			for (i = 0; i < (nAfterDot - strLen + dotPos + 1); i++) {
				resultStr = resultStr + "0";
			}
			return resultStr;
		}
	}
}

function checkPrice(price) {
	return (/^(([1-9]\d*)|\d)(\.\d{1,2})?$/).test(price.toString());
}

// 行业信息选择
function getIndustry2(el, hostUrl) {
	if (el.value != null && el.value > 0) {
		var url = hostUrl + "/custBasicInfo/ajaxGetIndustry2Info";
		$.post(url, {
			industry1Id : el.value
		}, function(data) {
			loadIndustry2(data);
		}, "json");
	} else {
		$("#industry2").html("<option value=''>行业二级</option>");
	}
}

function loadIndustry2(jsonObj) {
	var industryList = jsonObj.industry2List;
	var str = "<option value=''>行业二级</option>";
	if (industryList != undefined) {
		$.each(industryList, function(num, industry) {
			str += "<option value='" + industry.id.id + "'>" + industry.id.name
					+ "</option>";
		});
		$("#industry2").html(str);
	}
}

// 生成缩略图
function changeImg(mypic) {
	var xw = 200;
	var xl = 200;
	var width = mypic.width;
	var height = mypic.height;
	var bili = width / height;
	var A = xw / width;
	var B = xl / height;
	if (!(A > 1 && B > 1)) {
		if (A < B) {
			mypic.width = xw;
			mypic.height = xw / bili;
		}
		if (A > B) {
			mypic.width = xl * bili;
			mypic.height = xl;
		}
	}
}

// 打开图片页面
/*
 * function picOpen(pic,path,encryptFileName) { var url = pic.src; //url =
 * url.substring(0, url.lastIndexOf("?") + 1) +
 * url.substring(url.lastIndexOf("&") + 1); url = url.substring(0,
 * url.lastIndexOf("?") + 1) + "&name=" + encryptFileName;
 * if(path.indexOf("rar") > 0 || path.indexOf("zip") > 0){ url +=
 * "&type=downLoad"; window.location.href = url; }else{ alert(url+"&type=show");
 * window.open(url+"&type=show", encryptFileName,
 * 'height=100,width=400,top=0,left=0,toolbar=no,menubar=no,scrollbars=no,
 * resizable=no,location=no, status=no') } }
 */

function picOpen(pic, path, encryptFileName) {
	var url = pic.src;
	url = url.substring(0, url.lastIndexOf("?") + 1) + "&name="
			+ encryptFileName;
	if (path.indexOf("rar") > 0 || path.indexOf("zip") > 0) {
		url += "&type=downLoad";
		window.location.href = url;
	} else {
		if (getOs() == 'MSIE') {
			var newWindow = window.open('about:blank');
			var img = newWindow.document.createElement("img");
			img.setAttribute("src", url + "&type=show");
			newWindow.document.body.appendChild(img);
		} else {
			window
					.open(
							url + "&type=show",
							encryptFileName,
							'height=100,width=400,top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no')
		}
	}
}

function checkAllCheckBox(el) {
	$("input[name='custCheckboxes']").each(function() {
		if (el.checked)
			$(this).attr("checked", true);
		else
			$(this).attr("checked", false);
	});
}
function operateCheckBox() {
	var checkBoxs = document.getElementsByName("custCheckboxes");
	var checkAllType = document.getElementById("checkAllType");
	var count = 0;
	for ( var i = 0; i < checkBoxs.length; i++) {
		if (!checkBoxs[i].checked) {
			count++;
		}
	}
	checkAllType.checked = (count == 0);
}
/**
 * 将时间戳转为String格式
 * 
 * @param format
 *            yyyy-MM-dd hh:mm:ss
 * @return {*}
 */
Date.prototype.format = function(format) {
	var o = {
		"M+" : this.getMonth() + 1, // month
		"d+" : this.getDate(), // day
		"h+" : this.getHours(), // hour
		"m+" : this.getMinutes(), // minute
		"s+" : this.getSeconds(), // second
		"q+" : Math.floor((this.getMonth() + 3) / 3), // quarter
		"S" : this.getMilliseconds()
	// millisecond
	};

	if (/(y+)/.test(format)) {
		format = format.replace(RegExp.$1, (this.getFullYear() + "")
				.substr(4 - RegExp.$1.length));
	}

	for ( var k in o) {
		if (new RegExp("(" + k + ")").test(format)) {
			format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
					: ("00" + o[k]).substr(("" + o[k]).length));
		}
	}
	return format;
}

// 判断文本框内容长度
function getTextContentLength(inputStar) {
	var bytesCount = 0;
	for ( var i = 0; i < inputStar.length; i++) {
		var c = inputStar.charAt(i);
		if (/^[\u0000-\u00ff]$/.test(c)) {
			bytesCount += 1;
		} else {
			bytesCount += 2;
		}
	}
	return bytesCount;
}

function isMoney(money) {
	var reg = /^([1-9][\d]{0,7}|0)(\.[\d]{1,2})?$/;
	if (money.constructor === String) {
		if (money.match(reg))
			return true;
	}
	return false;
}

function digit_uppercase(n) {
	var fraction = [ '角', '分' ];
	var digit = [ '零', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖' ];
	var unit = [ [ '元', '万', '亿' ], [ '', '拾', '佰', '仟' ] ];
	var head = n < 0 ? '欠' : '';
	n = Math.abs(n);

	var s = '';

	for ( var i = 0; i < fraction.length; i++) {
		s += (digit[Math.floor(n * 10 * Math.pow(10, i)) % 10] + fraction[i])
				.replace(/零./, '');
	}
	s = s || '整';
	n = Math.floor(n);

	for ( var i = 0; i < unit[0].length && n > 0; i++) {
		var p = '';
		for ( var j = 0; j < unit[1].length && n > 0; j++) {
			p = digit[n % 10] + unit[1][j] + p;
			n = Math.floor(n / 10);
		}
		s = p.replace(/(零.)*零$/, '').replace(/^$/, '零') + unit[0][i] + s;
	}
	return head
			+ s.replace(/(零.)*零元/, '元').replace(/(零.)+/g, '零').replace(/^整$/,
					'零元整');
}

/**
 * 将数值四舍五入(保留2位小数)后格式化成金额形式
 * 
 * @param num
 *            数值(Number或者String)
 * @return 金额格式的字符串,如'1,234,567.45'
 * @type String
 */
function formatCurrency(num) {
	num = num.toString().replace(/\$|\,/g, '');
	if (isNaN(num))
		num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num * 100 + 0.50000000001);
	cents = num % 100;
	num = Math.floor(num / 100).toString();
	if (cents < 10)
		cents = "0" + cents;
	for ( var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
		num = num.substring(0, num.length - (4 * i + 3)) + ','
				+ num.substring(num.length - (4 * i + 3));
	return (((sign) ? '' : '-') + num + '.' + cents);
}

/**
 * 将数值四舍五入(保留1位小数)后格式化成金额形式
 * 
 * @param num
 *            数值(Number或者String)
 * @return 金额格式的字符串,如'1,234,567.4'
 * @type String
 */
function formatCurrencyTenThou(num) {
	num = num.toString().replace(/\$|\,/g, '');
	if (isNaN(num))
		num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num * 10 + 0.50000000001);
	cents = num % 10;
	num = Math.floor(num / 10).toString();
	for ( var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
		num = num.substring(0, num.length - (4 * i + 3)) + ','
				+ num.substring(num.length - (4 * i + 3));
	return (((sign) ? '' : '-') + num + '.' + cents);
}

function isCardNo(card) {
	// 身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
	var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
	if (reg.test(card)) {
		return true;
	}
	return false;
}

function isIdCardNo(num) {
	if (isNaN(num)) {
		alert("输入的不是数字！");
		return false;
	}
	var len = num.length, re;
	if (len == 15)
		re = new RegExp(/^(\d{6})()?(\d{2})(\d{2})(\d{2})(\d{3})$/);
	else if (len == 18)
		re = new RegExp(/^(\d{6})()?(\d{4})(\d{2})(\d{2})(\d{3})(\d)$/);
	else {
		alert("您输入的身份证号码格式有误，号码位数不对！");
		return false;
	}
	var a = num.match(re);
	if (a != null) {
		if (len == 15) {
			var D = new Date("19" + a[3] + "/" + a[4] + "/" + a[5]);
			var B = D.getYear() == a[3] && (D.getMonth() + 1) == a[4]
					&& D.getDate() == a[5];
		} else {
			var D = new Date(a[3] + "/" + a[4] + "/" + a[5]);
			var B = D.getFullYear() == a[3] && (D.getMonth() + 1) == a[4]
					&& D.getDate() == a[5];
		}
		if (!B) {
			alert("您输入的身份证号码格式有误,出生日期不对！");
			return false;
		}
	}
	return true;
}

function checkName(str) {
	var reg = /((^[\u4E00-\u9FA5]{2,30}$))/;
	str = str.replace(/^\s+/g, "");
	str = str.replace(/\s+$/g, "");
	if (str.length != 0) {
		if (reg.test(str))
			return true;
	}
	return false;
}

function checkCompName(str) {
	var reg = /((^[\u4E00-\u9FA5a-zA-Z\(\)\（\）]{2,50}$)|(^[a-zA-Z]+[\s\.]?([a-zA-Z]+[\s\.]?){2,50}[a-zA-Z]$))/;
	str = str.replace(/^\s+/g, "");
	str = str.replace(/\s+$/g, "");
	if (str.length != 0) {
		if (reg.test(str))
			return true;
	}
	return false;
}

function checkWebUrl(radioVal) {
	$("#url").removeAttr("readonly");
	$("#url").removeAttr("style");
	if (radioVal == 1) {
		$("#url").val("http://");
	} else if (radioVal == 2) {
		$("#url").val("http://onwap.mobi");
		$("#url").attr("style", "background-color: transparent; border:0;");
		$("#url").attr("readonly", true);
	}
}

function getOs() {
	var OsObject = "";
	if (navigator.userAgent.indexOf("MSIE") > 0) {
		return "MSIE";
	}
	if (isFirefox = navigator.userAgent.indexOf("Firefox") > 0) {
		return "Firefox";
	}
	if (isSafari = navigator.userAgent.indexOf("Safari") > 0) {
		return "Safari";
	}
	if (isCamino = navigator.userAgent.indexOf("Camino") > 0) {
		return "Camino";
	}
	if (isMozilla = navigator.userAgent.indexOf("Gecko/") > 0) {
		return "Gecko";
	}
}

function checkPasswordComplexity(v) {
	var numasc = 0;
	var charasc = 0;
	var otherasc = 0;
	var errorMsg = "密码安全级别低，请使用字母加数字或符号的组合密码！";
	if (0 == v.length) {
		return errorMsg;
	} else if (v.length < 6 || v.length > 16) {
		return errorMsg;
	} else {
		for ( var i = 0; i < v.length; i++) {
			var asciiNumber = v.substr(i, 1).charCodeAt();
			if (asciiNumber >= 48 && asciiNumber <= 57) {
				numasc += 1;
			}
			if ((asciiNumber >= 65 && asciiNumber <= 90)
					|| (asciiNumber >= 97 && asciiNumber <= 122)) {
				charasc += 1;
			}
			if ((asciiNumber >= 33 && asciiNumber <= 47)
					|| (asciiNumber >= 58 && asciiNumber <= 64)
					|| (asciiNumber >= 91 && asciiNumber <= 96)
					|| (asciiNumber >= 123 && asciiNumber <= 126)) {
				otherasc += 1;
			}
		}

		if (!getErrorPwdComposit(charasc, numasc, otherasc)) {
			return errorMsg;
		}
		return true;
		/*
		 * if(0==numasc) { return "密码必须含有数字"; }else if(0==charasc){ return
		 * "密码必须含有字母"; }else if(0==otherasc){ return "密码必须含有特殊字符"; }else{ return
		 * true; }
		 */
	}
}

function getErrorPwdComposit(charasc, numasc, otherasc) {
	var res = true;
	if (0 == charasc && 0 == numasc && 0 == otherasc) {
		res = false;
	} else if (0 == charasc && 0 == numasc) {
		res = false;
	} else if (0 == charasc && 0 == otherasc) {
		res = false;
	} else if (0 == numasc && 0 == otherasc) {
		res = false;
	}
	return res;
}
