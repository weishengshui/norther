/**
 * 后台菜单树
 */
var ConsoleMenuTree = (function() {

	var obj = new Object();

	obj.init = function(treeId, type, menus) {
		var _setting = {
			treeId : treeId,
			async : {
				enable : true,
				type : 'post',
				dataType: 'json',
				url : "menu/getMenusByParentId.do",
				otherParam : {
					type : type,
					id: 0
				}
			},
			data : {
				key : {
					name : 'menuName',
					url : '',
					children: 'children'
				},
				simpleData : {
					idKey : 'id'
				}
			},
			callback : {
				onClick : onNodeClick,
				onAsyncSuccess : zTreeOnAsyncSuccess
			}
		};

		var zNodes = [ {
			id : "0",
			menuName : type=='1' ? "网站后台" : '网站前台',
			parentId : '-1',
			isParent : true,
			open : true
		} ];

		obj.zTreeObj = $.fn.zTree.init($('#' + treeId), _setting, zNodes);

		// 立即异步加载数据
		// true不展开(由zTreeOnAsyncSuccess展开)，false只展开一级
		obj.zTreeObj.reAsyncChildNodes(obj.zTreeObj.getNodes()[0], "refresh",
				true);
	};

	/**
	 * 点击节点，初始化表单
	 */
	var onNodeClick = function(event, treeId, treeNode) {
		var parent = treeNode.getParentNode();
		$("#updateSendButton").attr("disabled", true);
		// 模块添加
		$('#parentModuleName').val(treeNode.menuName);
		$('#parentId').val(treeNode.id);
		$("#addbutton").attr("disabled", false);
		if (parent) { // 如果点击的是根节点就不进入下面的代码快
			var parentMenuName = parent.menuName;

			// 模块修改
			$('#edit_moduleName').val(treeNode.menuName);
			$('#edit_url').val(treeNode.url);
			$('#edit_sort').val(treeNode.sort);
			$('#edit_parentModuleName').val(parentMenuName);
			$('#edit_ModuleId').val(treeNode.id);

			$("#delbutton").attr("disabled", false);
			$("#relButton").attr("disabled", false);
		} else {
			clearModuleForm();
		}
	};
	
	/**
	 * 清空表单
	 */
	var clearModuleForm = function() {
		// 模块修改
		$('#edit_moduleName').val('');
		$('#edit_url').val('');
		$('#edit_sort').val('');
		$('#edit_parentModuleName').val('');
		$('#edit_ModuleId').val('');

		$("#delbutton").attr("disabled", true);
		$("#relButton").attr("disabled", true);
	};

	/**
	 * 异步加载完数据，展开树的所有节点
	 */
	var zTreeOnAsyncSuccess = function(event, treeId, treeNode, msg) {
		obj.zTreeObj.expandAll(true);
	};

	return obj;
})();



/**
 * 角色、权限菜单树 
 */
var RoleMenuTree = (function() {

	var obj = new Object();
	var roleId = null;

	obj.init = function(treeId, type, _roleId) {
		roleId = _roleId;
		var _setting = {
			treeId : treeId,
			async : {
				enable : true,
				type : 'post',
				url : "menu/getMenusByParentId.do",
				autoParam : [ "id" ],
				otherParam : {
					type : type
				}
			},
			data : {
				key : {
					name : 'menuName',
					url : ''
				},
				simpleData : {
					idKey : 'id'
				}
			},
			callback : {
				onAsyncSuccess : zTreeOnAsyncSuccess
			},
			check : {
				enable: true
			}
		};

		var zNodes = [ {
			id : "0",
			menuName : type=='1' ? "网站后台" : '网站前台',
			parentId : '-1',
			isParent : true,
			open : true
		} ];

		obj.zTreeObj = $.fn.zTree.init($('#' + treeId), _setting, zNodes);

		// 立即异步加载数据
		obj.zTreeObj.reAsyncChildNodes(obj.zTreeObj.getNodes()[0], "refresh",
				true);
	};
	
	obj.getAllCheckedNodes = function(){
		var nodes = obj.zTreeObj.getCheckedNodes(true);
		var checkedNodes = [];
		if(nodes && nodes.length > 0){
			for(var i = 1, length = nodes.length; i < length; i++){
				checkedNodes.push(nodes[i]);
			}
		}
		
		return checkedNodes;
	};
	
	/**
	 * 自动勾选角色的菜单
	 */
	var checkRoleMenuNodes = function(){
		
		if('' != roleId){
			$.ajax({
				url: 'admin/role/getMenusByRoleId.do',
				type: 'post',
				data: {roleId: roleId},
				contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
				traditional: true,
				dataType: 'json',
				success: function(nodes){
					if(nodes && nodes.length > 0) {
						for(var i = 0, length = nodes.length; i < length; i++){
							var node = obj.zTreeObj.getNodeByParam("id", nodes[i].id, null);
							obj.zTreeObj.checkNode(node, true, false, false);
						}
						obj.zTreeObj.checkNode(obj.zTreeObj.getNodes()[0], true, false, false);
					}
				}
			});
		}
	};

	var zTreeOnAsyncSuccess = function(event, treeId, treeNode, msg) {
		obj.zTreeObj.expandAll(true);
		checkRoleMenuNodes();
	};

	return obj;
})();