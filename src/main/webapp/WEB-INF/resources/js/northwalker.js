
// 检查文件是否为图片
function checkFilenameImg(filePathName){
	var array = filePathName.split('.');
	var ext = array[array.length - 1].toLowerCase();
	var typeArray = new Array("jpg","jpeg","gif","png","bmp");
	for(var i=0; i<typeArray.length; i++){
		if(ext == typeArray[i]) {
			return true;
		}
	}
	return false;
}

//检查文件是否为视频文件
function checkVideoFile(filePathName){
	var array = filePathName.split('.');
	var ext = array[array.length - 1].toLowerCase();
	var typeArray = new Array("mp4");
	// var typeArray = new Array("avi","mp4","flv","rm","3gp", "swf", "rmvb");
	for(var i=0; i<typeArray.length; i++){
		if(ext == typeArray[i]) {
			return true;
		}
	}
	return false;
}

function previewImage(rootPath, imagePathId, title) {
	var imagePath = $('#'+imagePathId).val();
	if(isEmpty(imagePath)) {
		alert('图片不存在！');
		return;
	}
	if(imagePath.indexOf("?") == -1){
		imagePath += "?1=1";
	}
	var url = rootPath + imagePath;
	if(getOs() == 'MSIE'){
		var newWindow = window.open('about:blank');
		var img = newWindow.document.createElement("img");
		img.setAttribute("src", url+"&type=show");
		newWindow.document.body.appendChild(img);
	}else{
		window.open(url+"&type=show", title, 'height=100,width=400,top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
	}
}