<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<script type="text/javascript">
	//本地条件分页查询
	function pageJump(page) {
		PPage("page_select", page, totalPageCount, "pageJump", true);
		$("#currentPage").val(page);
		$('#chaxun').click();
	}

	$(document).ready(function() {
		$('#chaxun').click(function() {
			$('#listForm').submit();
		});
	});

	// 改变每页的记录数
	function changePageSize(obj) {
		$("#currentPage").val(1);
		$("#pageSize").val(obj.value);
		$('#chaxun').click();
	}

	function checkAllCheckBox(el) {
		$("input[name='videoCheckboxes']").each(function() {
			if (el.checked)
				$(this).attr("checked", true);
			else
				$(this).attr("checked", false);
		});
	}

	// 编辑视频
	function editVideo() {
		var count = 0;
		var id = 0;
		$('input[name="videoCheckboxes"]:checked').each(function() {
			id = $(this).val();
			count++;
		});
		if (count == 0) {
			alert("请选择一行记录！");
		} else if (count > 1) {
			alert("一次只能编辑一行记录！");
		} else {
			var title = $('#video_title_' + id).val();
			var path = $('#video_path_' + id).val();
			var link = $('#video_link_' + id).val();
			var sort = $('#video_sort_' + id).val();
			$('#title').val(title);
			$('#path').val(path);
			$('#link').val(link);
			$('#sort').val(sort);
			$('#id').val(id);
			showAddVideoDialog('edit');
		}
	}

	// 删除视频
	function deleteVideos() {
		var count = 0;
		$('input[name="videoCheckboxes"]:checked').each(function() {
			count++;
		});
		if (count == 0) {
			alert("至少选择一条记录！");
			return;
		}
		if (confirm("确认删除？")) {
			$('#deleteVideosForm').submit();
		}
	}

	// 提交视频表单
	function submitCreateOrUpdateVideoForm() {
		var title = $("#title").val();
		var path = $("#path").val();
		var sort = $('#sort').val();
		if (isEmpty(title)) {
			alert("视频标题不能为空！");
			$("#title").focus();
			return;
		}
		if (isEmpty(path)) {
			alert("请上传视频文件文件！");
			$('#path').focus();
			return;
		}
		if (isEmpty(sort)) {
			alert("排序不能为空！");
			$('#sort').focus();
			return;
		} else if (!check_number(sort)) {
			alert("请输入正整数排序值！");
			$('#sort').focus();
			return;
		}
		$('#videoForm').attr("action", "admin/video/createOrUpdate.do");
		$('#videoForm').attr("target", "_self");
		$('#videoForm').submit();
	}

	// 打开video编辑窗口
	function showAddVideoDialog(type) {
		var title = '视频添加';
		if (type == 'add') { // 复位
			$('#title').val('');
			$('#path').val('');
			$('#sort').val('');
			$('#id').val('');
		} else if (type == 'edit') {// 
			title = "视频修改";
		}
		openDialog('edit_video_dialog_div', title, 470, 200);
	}

	// 上传视频文件
	function uploadVideoFile() {
		var file = $('#file').val();
		if (isEmpty(file)) {
			alert("请先添加视频文件！");
			return;
		} else if (!checkVideoFile(file)) {
			$('#file').val('');
			alert("视频文件格式暂只支持mp4！");
			return;
		}
		$('#backGroundDiv').show();
		$('#videoForm').attr("action", "file/upload.do");
		$('#videoForm').attr("target", "file_upload_frame");
		$('#videoForm').submit();
	}
</script>

<div class="crmTitle">
	<table width="100%">
		<tr>
			<td>视频</td>
		</tr>
	</table>
</div>

<div class="crmList line">
	<!-- 查询条件 -->
	<form method="post" action="admin/video/list.do" id="listForm">
		<div class="jbcx">
			<table width="100%" class="custab3">
				<tbody>
					<tr>
						<td width="100px" class="nameType">发布时间：</td>
						<td width="200px"><input type="text" 
							style="width: 83px;" name="publishDate1" class="Wdate"
							value='<fmt:formatDate value="${dto.publishDate1 }" type="both" pattern="yyyy-MM-dd" />'
							onFocus="WdatePicker({readOnly:true})" />&nbsp;至&nbsp; <input
							type="text" style="width: 83px;"
							name="publishDate2" class="Wdate"
							value='<fmt:formatDate value="${dto.publishDate2 }" type="both" pattern="yyyy-MM-dd" />'
							onFocus="WdatePicker({readOnly:true})" /></td>
						<td width="100px" class="nameType">视频标题：</td>
						<td width="200px"><input type="text" name="title"
							class="int2" value='<c:out value="${dto.title }" />' /></td>
						<td>&nbsp;</td>
					</tr>
				</tbody>
			</table>
		</div>
		<table width="100%" class="custab3">
			<tr>
				<td width="2%">&nbsp;</td>
				<td width="7%"><input type="hidden" id="pageSize"
					name="pageSize" value="${page.pageSize}" /> <input type="hidden"
					id="currentPage" name="currentPage" value="${page.currentPage}" />
					<input type="button" id="chaxun" class="sureBtn" value="" /></td>
				<td width="91%">&nbsp;</td>
			</tr>
		</table>
	</form>

	<!-- 列表 -->
	<form action="admin/video/deletes.do" id="deleteVideosForm" method="post">
		<div class="crmList">
			<!-- 工具栏 -->
			<table width="100%" class="bigTable">
				<tr>
					<td width="2%">&nbsp;</td>
					<td width="71%">
						<table>
							<tbody>
								<tr>
									<td width="45px">
										<div class="add" title="添加">
											<a href="javascript:showAddVideoDialog('add')"
												onFocus="this.blur()"> <span>&nbsp;&nbsp;&nbsp;</span>
											</a>
										</div>
									</td>
									<td width="45px">
										<div class="editor" title="编辑">
											<a href="javascript:editVideo()" onFocus="this.blur()"> <span>&nbsp;&nbsp;&nbsp;</span>
											</a>
										</div>
									</td>
									<td width="45px">
										<div class="delete" title="删除">
											<a href="javascript:deleteVideos()" onFocus="this.blur()">
												<span>&nbsp;&nbsp;&nbsp;</span>
											</a>
										</div>
									</td>
									<td>&nbsp;</td>
								</tr>
							</tbody>
						</table>
					</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<!-- 列表数据 -->
			<table width="100%" class="bigTable">
				<tr>
					<td>
						<table width="100%" class="editTable" cellpadding="1">
							<tbody>
								<tr>
									<th width="3%"><input type="checkbox" class="check"
										onClick="checkAllCheckBox(this)" /></th>
									<th width="10%">排序</th>
									<th width="30%">视频标题</th>
									<th width="40%">视频地址</th>
									<th width="10%">操作</th>
								</tr>
							<tbody>
								<c:forEach items="${page.data}" var="video" varStatus="vs">
									<tr>
										<td><input type="checkbox" name="videoCheckboxes"
											value="${video.id}" class="check" /> <input type="hidden"
											id="video_title_${video.id }"
											value='<c:out value="${video.title }"></c:out>' /> <input
											type="hidden" id="video_path_${video.id }"
											value='<c:out value="${video.path }"></c:out>' /> <input
											type="hidden" id="video_sort_${video.id }"
											value='<c:out value="${video.sort }"></c:out>' /></td>
										<td><c:out value="${video.sort }"></c:out></td>
										<td><c:out value="${video.title }"></c:out></td>
										<td><c:out value="${video.path }"></c:out></td>
										<td><a
											href="videoList.do?menuIndex=menuIndex6&id=${video.id }" target="_blank">预览</a></td>
									</tr>
								</c:forEach>
								<c:if test="${empty page.data}">
									<tr>
										<td colspan="4"><b><font color="#FF0000">无相关数据！</font></b></td>
									</tr>
								</c:if>
							</tbody>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
			      <td>
			        <table width="100%" class="titlePage">
			          <tbody>
			           <tr>
			             <td width="14%">共查询到<span class="red">${page.totalCount}</span>条记录</td>
			             <td width="53%" class="paging">
			              	<div id="page_select" class="page"></div>
								<script type="text/javascript">
									var page = "${page.currentPage}";
									var totalCount = "${page.totalCount}";
									if(totalCount > 0 )
										PPage("page_select",page,"${page.pageCount}","pageJump",true);
								</script>
			             </td>
			             <td width="18%">
							 <c:if test="${page.totalCount > 0}">
								每页条数：
							   <select id="setPageSize" onChange="changePageSize(this)">
								  <option value="20" <c:if test="${page.pageSize==20}">selected="selected"</c:if>>20</option>
								 <option value="50" <c:if test="${page.pageSize==50}">selected="selected"</c:if>>50</option>
								 <option value="100" <c:if test="${page.pageSize==100}">selected="selected"</c:if>>100</option>
							   </select>
						   </c:if>
			            </td>  
			           </tr>
			         </tbody>
			        </table>
			      </td>
			    </tr>
			</table>
		</div>
	</form>
</div>
<div id="edit_video_dialog_div" style="display: none;">
	<div class="crmList line">
		<form id="videoForm" name="videoForm" method="post"
			action="admin/video/createOrUpdate.do" enctype="multipart/form-data"
			target="file_upload_frame">
			<input type="hidden" name="id" value='' id="id"> <input
				type="hidden" name="forward" value="video/upload" /> <input
				type="hidden" name="type" value="show">
			<div class="jbcx">
				<table width="100%" class="custab4">
					<tbody>
						<tr>
							<td width="80px" class="nameType"><span style="color: red;">
									* </span>视频标题：</td>
							<td><input type="text" name="title" id="title" class="int2" />
							</td>
						</tr>
						<tr>
							<td class="nameType"><span style="color: red;"> * </span>视频文件：</td>
							<td valign="middle"><input type="hidden" readonly="readonly"
								name="path" id="path" class="int2" /><input type="file"
								name="file" id="file" class="int2" onchange="uploadVideoFile()">&nbsp;视频格式为mp4</td>
						</tr>
						<tr>
							<td class="nameType"><span style="color: red;"> * </span>排序：</td>
							<td><input type="text" name="sort" id="sort" class="int2">&nbsp;
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<table width="100%" class="custab5">
									<tbody>
										<tr>
											<td width="10%" align="right"><input type="hidden"
												name="id" value="" id="id" /> <input type="button"
												value="保存" id="save" style="margin: 3px 20px 6px 115px"
												onclick="submitCreateOrUpdateVideoForm()" class="saveBtn" />
											</td>
											<td width="45%" align="left"><input type="button"
												class="cancelBtn" value="取消"
												style="margin: 3px 20px 6px 0px"
												onClick="closeDialog('edit_video_dialog_div')" /></td>
											<td width="5%">&nbsp;</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</form>
	</div>
</div>
