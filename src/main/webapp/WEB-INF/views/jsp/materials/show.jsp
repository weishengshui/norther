<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
$(document).ready(function(){
	var ue = UE.getEditor('container');
	ue.addListener( 'ready', function( editor ) {
		ue.setContent(unescape("${materials.content}"));
	 } );
	
	$('#brief').val(unescape("${materials.brief }"));
	
	$('#chaxun').click(function(){
		var content = ue.getContent();
		$('#content').val(escape(content));
		
		var title = $('#title').val();
		var thumbPath = $('#thumbPath').val();
		var brief = $('#brief').val();
		if(isEmpty(title)){
			alert("资料标题不能为空！");
			$('title').focus();
			return;
		}
		if(isEmpty(thumbPath)){
			alert('请上传缩略图！');
			$('#thumbPath').focus();
			return;
		}
		if(isEmpty(brief)){
			alert("资料摘要不能为空！");
			$('#brief').focus();
			return;
		}
		$('#brief_').val(escape(brief));
		
		$('#materialsForm').attr("action", "admin/materials/createOrUpdate.do");
		$('#materialsForm').attr("target", "_self");
		$('#materialsForm').submit();
	});
});

// 上传资料缩略图
function uploadMaterialsImage(){
	var file = $('#file').val();
	if(isEmpty(file)) {
		alert("请先添加图片！");
		return;
	} else if(!checkFilenameImg(file)){
		$('#file').val('');
		alert("图片格式不正确！");
		return;
	}
	$('#backGroundDiv').show();
	$('#materialsForm').attr("action", "file/upload.do");
	$('#materialsForm').attr("target", "file_upload_frame");
	$('#materialsForm').submit();
}
</script>

<div class="crmTitle">
  <table width="100%">
    <tr>
      <td>
      	<c:choose>
      		<c:when test="${!empty materials.id }">资料编辑</c:when>
      		<c:otherwise>资料添加</c:otherwise>
      	</c:choose>
      </td>
    </tr> 
  </table>
</div>
<br/>
 <form id="materialsForm" name="materialsForm" method="post" action="admin/materials/createOrUpdate.do" enctype="multipart/form-data">
 	<input type="hidden" name="id" value="<c:out value="${materials.id }"></c:out>">
     <table width="100%"  class="custab2" style="height:100px; border:0px;">
       <tbody>
       	<tr>
       		<td width="8%" class="nameType"><span style="color: red;"> * </span>资料标题：</td>
       		<td colspan="2">
       			<input type="text" name="title" id="title" value='<c:out value="${materials.title }"></c:out>' class="int2" />
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType"><span style="color: red;"> * </span>缩略图：</td>
       		<td valign="middle" width="330px">	
       			<input type="hidden" name="thumbPath" id="thumbPath" value='<c:out value="${materials.thumbPath }"></c:out>'>
       			<input type="hidden" name="forward" value="materials/upload" />
 				<input type="hidden" name="type" value="show">
       			<input type="file" name="file" id="file" onchange="uploadMaterialsImage()" style="width: 250px;"/>&nbsp;&nbsp;
       		</td>
       		<td>
       			<img alt="图片不存在" src="${cPath }${materials.thumbPath }" id="previewImg" width="50px" height="50px" ${(empty materials.thumbPath) ? "style='display:none;'" : "" }>
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType" valign="top"><span style="color: red;"> * </span>资料摘要：</td>
       		<td colspan="2">	
       			<input type="hidden" name="brief" value="${materials.brief }" id="brief_">
       			<textarea rows="5" cols="100" id="brief">
       				<c:out value="${materials.brief }" />
       			</textarea>
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType" valign="top">正文：</td>
       		<td colspan="2">
       			<script id="container" type="text/plain">
				</script>
				<input type="hidden" id="content" name="content" value='<c:out value="${materials.content }"></c:out>'>
       		</td>
       	</tr>
    </tbody>
</table>

<div class="jbcx">
    <table width="100%"  class="custab5">
	    <tbody>
	      <tr>
	         <td width="11%">
	         	<input type="button" id="chaxun" name="chaxun" class="saveBtn" value="保存"/>
	         </td>
	         <td>&nbsp;</td>
	      </tr>
	    </tbody>
    </table>
</div>
</form>
