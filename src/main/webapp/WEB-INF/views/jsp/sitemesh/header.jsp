<%@ page language="java" pageEncoding="UTF-8"%>
<script type="text/javascript">
function logOut(){
	if(confirm("确认退出?")){
		window.location.href = "${cPath}/admin/logout.do";
	}
}

function changePwd(){
	window.location.href = "${cPath}/admin/changePwd.do";
}

</script>
<!--系统头部--------开始-->
<div id="contact_page"></div>
<div id="divTop">
    <div class="logo"></div>
    <div class="dateTime"><span>今天是</span><script language="javascript" type="text/javascript">CAL();</script></div>
    <div class="rightMenu">
        <span class="welcome_icon"></span>
        <span class="welcomeWords">欢迎您！${sessionScope.user.username}</span>
        <a href="javascript:void(0);" onclick="changePwd();" onFocus="this.blur()">修改密码</a> | <a href="javascript:void(0)"
                                                                                             onclick="logOut()"
                                                                                             onFocus="this.blur()">退出 </a>
    </div>
</div>
<!--系统头部--------结束-->


