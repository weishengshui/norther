<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page"%>
<%
	String path = request.getScheme() + "://" + request.getServerName() + ":" 
		+ request.getServerPort() + request.getContextPath()
		+ "/";
%>
<!DOCTYPE html>
<html>
<head>
<title><decorator:title default="深圳市诺石行者网络科技有限公司"  /></title>
<base href="<%=path %>" />

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="front/css/global.css" rel="stylesheet" type="text/css" />
<link href="front/css/css.css" rel="stylesheet" type="text/css" />

<script src="front/js/jquery-1.9.1.min.js"  type="text/javascript"></script>
<script type="text/javascript" src="js/common.js"></script>
<script src="front/js/jquery.SuperSlide.js"  type="text/javascript"></script>
<script src="front/js/index.js"  type="text/javascript"></script>
<script src="front/js/jquery.provincesCity.js"  type="text/javascript"></script>
<script src="front/js/provincesdata.js" type="text/javascript"></script>
<script src="front/js/Validform_v5.3.2_min.js"  type="text/javascript"></script>
<script src="front/js/zxsq.js"  type="text/javascript"></script>
<script src="front/js/xzyb-1.js" type="text/javascript"></script>
<decorator:head />
</head>
<body ${bodyClass }>
	<!-- 导航 -->
	<page:applyDecorator page="/getHeander.do" name="officialHeader" />
	
	<!-- 内容主体 -->
	<decorator:body />
	
	<!-- 底部信息 -->
	<page:applyDecorator page="/getFooter.do" name="officialFooter" />
</body>
</html>

