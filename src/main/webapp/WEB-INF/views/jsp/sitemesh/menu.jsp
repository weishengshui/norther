<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<!--左侧功能菜单--------开始-->
<div id="firstpane" class="menu_list">
	<c:forEach items="${consoleMenus }" var="first">
		<c:if test="${first.isLeaf == false }">
			<p class="menu_head">${first.menuName }</p>
			<div class="menu_body"
				<c:if test="${currentMenu.parent.menuName == first.menuName }">style="display: block;"</c:if>>
				<c:forEach items="${first.children }" var="second">
					<a href="${second.url}" onFocus="this.blur()"
						<c:if test="${currentMenu.menuName == second.menuName }">class="cur"</c:if>>${second.menuName}</a>
				</c:forEach>
			</div>
		</c:if>
	</c:forEach>
</div>

<script type="text/javascript">
	$(function() {
		$("#firstpane p.menu_head").click(
				function() {
					$(this).next("div.menu_body").slideToggle(100).siblings(
							"div.menu_body").slideUp("slow");
				});
		$("#firstpane p.menu_head").mousemove(function() {
			$(this).css({
				background : "url(images/bg1.png) no-repeat left top"
			});
			$(this).css({
				color : "#ffffff"
			});
		});
		$("#firstpane p.menu_head").mouseout(function() {
			$(this).css({
				background : "url(images/bg1.png) no-repeat left top"
			});
			$(this).css({
				color : "#ffffff"
			});
		});
	});
</script>

<!--左侧功能菜单--------结束-->
