<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page"%>
<%
	String path = request.getScheme() + "://" + request.getServerName() + ":" 
		+ request.getServerPort() + request.getContextPath()
		+ "/";
%>
<!DOCTYPE HTML>
<html>
<head>
<title>诺石官网后台</title>

<base href="<%=path %>" />
<link type="text/css" rel="stylesheet" href="css/common.css">
<link type="text/css" rel="stylesheet" href="css/customerList.css">
<link type="text/css" rel="stylesheet" href="css1/jquery-ui-1.10.3.custom.css" />
<link type="text/css" rel="stylesheet" href="css1/combobox.css">

<script src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="<%=path %>js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="js/data.js"></script>
<script type="text/javascript" src="js/page.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/norther.js"></script>
<script type="text/javascript" src="js/combobox.js"></script>
<!-- ueditor -->
<script type="text/javascript" src="<%=path %>js/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="<%=path %>js/ueditor/ueditor.all.js"></script>
<!-- zTree -->
<script type="text/javascript" src="js/jquery.ztree.core-3.5.js"></script>
<script type="text/javascript" src="js/jquery.ztree.excheck-3.5.js"></script>
<script type="text/javascript" src="js/jquery.ztree.exedit-3.5.js"></script>
<script type="text/javascript" src="js/menuTree.js"></script>
<link rel="stylesheet" href="css1/zTreeStyle/zTreeStyle.css" type="text/css">

<decorator:head />
</head>

<body>
	<page:applyDecorator page="/sitemesh/header.jsp" name="header" />
	
	
	
<div id="divGlobal" style="min-height:500px">

<!--左侧功能菜单--> 
  <div id="divMainLeft">  
     <page:applyDecorator page="/sitemesh/menu.jsp" name="menuer" />
  </div>

<!--右块内容区--> 
  <div id="divMainRight">
   <decorator:body />   
  </div>
</div>
<page:applyDecorator page="/sitemesh/footer.jsp" name="footer" />
</body>
</html>

