<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<script type="text/javascript">
	
	function checkAllCheckBox(el) {
		$("input[name='roleCheckboxes']").each(function () {
			if (el.checked)
				$(this).attr("checked", true);
			else
				$(this).attr("checked", false);
		});
	}
	
	// 编辑角色
	function editRoles() {
		var count = 0;
		var id = 0;
		$('input[name="roleCheckboxes"]').each(function(){
			if($(this).attr('checked') == 'checked') {
				id = $(this).val();
				count++;
			}
		});
		if(count == 0) {
			alert("请选择一行记录！");
		} else if(count > 1) {
			alert("一次只能编辑一行记录！");
		} else {
			var roleName = $('#role_name_'+id).val();
			$('#roleName').val(roleName);
			$('#id').val(id);
			showAddRoleDialog('edit');
			//window.location.href="role/show.do?menuPath=role/list.do&id="+id;
		}
	}
	
	// 删除角色
	function deleteRoles() {
		var count = 0;
		$('input[name="roleCheckboxes"]:checked').each(function(){
			count++;
		});
		if(count == 0) {
			alert("至少选择一条记录");
			return;
		}
		if(confirm("确认删除？")){
			$('#deleteRolesForm').submit();
		}
	}
	
	// 打开添加角色窗口
	function showAddRoleDialog(type) {
		var title = '角色添加';
		if(type == 'add') {
			$('#id').val('');
			$('#roleName').val('');
		} else if(type == 'edit') {
			title = "角色修改";
		}
		openDialog('edit_role_dialog_div',title,422,180);
		var roleId = $('#id').val();
		RoleMenuTree.init('treeDemo', 1, roleId);
	}
	
	function submitCreateOrUpdateRoleForm() {
		var roleName = $('#roleName').val();
		if(isEmpty(roleName)) {
			alert("角色名称不能为空！");
			$('#roleName').val(trim(roleName));
			$('#roleName').focus();
			return false;
		}
		var nodes = RoleMenuTree.getAllCheckedNodes();
		$('#hidden_menus_ids_div').empty();
		if(nodes.length > 0){
			for(var i = 0, length = nodes.length; i < length; i++){
				var menuIdHtml = "<input type='hidden' name='menuId' value='" + nodes[i].id + "' />";
				$('#hidden_menus_ids_div').append(menuIdHtml);		
			}
		}
		
		// checkRoleNameExists
		var exists = false;
		// 校验角色名称是否已经存在
		var param = {};
		param.roleName = roleName;
		param.id = $('#id').val();
		$.ajax({
			url: 'admin/role/checkRoleNameExists.do',
			type: 'post',
			data: param,
			dataType: 'json',
			async: false,
			success: function(data) {
				exists = data.success == false;
				if(exists == false){
					alert(data.msg);
				}
			}
		});
		return exists;
	}
	
	function showRoleMenuTree(roleId) {
		openDialog('edit_role_menu_div','编辑权限',422,400);
	}
</script>

<div class="crmTitle">
  <table width="100%">
    <tr>
      <td>角色权限</td>
    </tr> 
  </table>
</div>

<div class="crmList line">
 <form action="admin/role/deletes.do" id="deleteRolesForm" method="post">
 <div class="crmList">
 <!-- 工具栏 -->
 <table width="100%" class="bigTable">
	<tr>
	  <td width="2%">&nbsp;</td>
	  <td width="71%">      
		<table>
		   <tbody>
				<tr>
					<td width="45px">
						<div class="add" title="添加">
							<a href="javascript:showAddRoleDialog('add');" onFocus="this.blur()">
								<span>&nbsp;&nbsp;&nbsp;</span>
							</a>
						</div>
					</td>
					<td width="45px">
						<div class="editor" title="修改">
							<a href="javascript:editRoles()" onFocus="this.blur()">
							<span>&nbsp;&nbsp;&nbsp;</span>
							</a>
						</div>
					</td>
					<td width="45px">
						<div class="delete" title="删除">
							<a href="javascript:deleteRoles()" onFocus="this.blur()">
								<span>&nbsp;&nbsp;&nbsp;</span>
							</a>
						</div>
					</td>
					<td>&nbsp;</td>
				</tr>
			 </tbody>
		</table>       
		</td>
		<td>&nbsp;</td>
	</tr>
</table>
<!-- 列表数据 -->
  <table width="100%" class="bigTable">                                
    <tr>
      <td>          
       <table width="100%" class="editTable" cellpadding="1">
        <tbody>
			 <tr>
			  <th width="3%"><input type="checkbox" class="check" onClick="checkAllCheckBox(this)" /></th>
			  <th width="10%">序号</th>
			  <th width="50%">角色名称</th>
			 </tr>
			 <tbody id="trContent">
				   <c:forEach items="${roles}" var="role" varStatus="s">
						   <tr>
								<td>
								<c:choose>
									<c:when test="${role.admin == 1 }">&nbsp;</c:when>
									<c:otherwise>
										<input type="checkbox" name="roleCheckboxes" value="${role.id}"
										 class="check"/>
									</c:otherwise>
								</c:choose>
								 </td>
								<td>
									${s.count }
								</td>
								<td>
									<input type="hidden" id="role_name_${role.id }" value="${role.roleName }"/>
									<c:out value="${role.roleName }"></c:out>
								</td>
						   </tr>
				   </c:forEach>
				   <c:if test="${empty roles}">
				   		<tr>
							<td colspan="4"><b><font color="#FF0000">无相关数据！</font></b></td>
						</tr>
				   </c:if>
			 </tbody>
        </tbody>
       </table>      
      </td>        
    </tr>             
    
    <tr>
      <td>
        <table width="100%" class="titlePage">
          <tbody>
           <tr>
             <td width="14%">共查询到<span class="red">${fn:length(roles)}</span>条记录</td>
             <td width="53%" class="paging">
             &nbsp;
             </td>
             <td width="18%">
             &nbsp;
            </td>  
           </tr>
         </tbody>
        </table>
      </td>
    </tr>
  </table>
</div>
</form>
</div>
<div id="edit_role_dialog_div" style="display: none;">
	<div class="crmList line">
 <form id="create_update_role_form" action="admin/role/createOrUpdate.do" method="post" onsubmit="return submitCreateOrUpdateRoleForm();">
	<div style="display: none;" id="hidden_menus_ids_div"></div>
   <div class="jbcx">
     <table width="100%" class="custab4">
       <tbody>            
         <tr>
           <td width="20%" class="nameType"><span class="red"> * </span>角色名称：</td>
           <td width="30%">
		   		<input type="text" name="roleName" id="roleName" class="int2" />
		   </td>
         </tr>
         <tr>
           <td width="20%" class="nameType"><span class="red"> * </span>权&nbsp;&nbsp;&nbsp;&nbsp;限：</td>
           <td width="30%">
		   		<a href="javascript:showRoleMenuTree();" >编辑权限</a>
		   </td>
         </tr>
		 <tr>
			 <td colspan="2">
			   <table width="100%" class="custab5">
				 <tbody>
				   <tr>
					 <td width="10%" align="right">
						<input type="hidden" name="id" value="" id="id"/>
						<input type="submit" value="保存" id="save" style="margin: 3px 20px 6px 85px" class="saveBtn" />
					 </td>
					 <td width="45%" align="left">
						<input  type="button" class="cancelBtn" 
						style="margin: 3px 20px 6px 0px"
						value="取消" onClick="closeDialog('edit_role_dialog_div')"/>
					 </td>
					 <td width="5%">&nbsp;</td>
				   </tr>
				 </tbody>
			   </table>
			 </td>
		   </tr>
       </tbody>
     </table>
    </div>
 </form>
</div>
</div>
<!-- 编辑角色的权限 -->
<div id="edit_role_menu_div" style="display: none;">
	<div style="height: 290px; overflow-y: scroll;">
		<ul id="treeDemo" class="ztree"><li>数据加载中...</li></ul>
	</div>
     <table width="100%" class="custab4">
       <tbody>            
		 <tr>
			 <td>
			   <table width="100%" class="custab5">
				   <tr>
					 <td>
						<input  type="button" style="margin: 3px 20px 6px 140px" class="cancelBtn" value="确定" onClick="closeDialog('edit_role_menu_div')"/>
					 </td>
				   </tr>
			   </table>
			  </td>
			 </tr>
       </tbody>
     </table>
</div>