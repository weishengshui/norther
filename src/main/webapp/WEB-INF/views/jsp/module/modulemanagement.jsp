<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html;charset=UTF-8" language="java"
	pageEncoding="UTF-8"%>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="css1/purview.css">
		<script type="text/javascript">
		$(document).ready(function(){
			ConsoleMenuTree.init('treeDemo', ${type});
		});
		
		function operateModule(type){
			if($("#parentId").val() == "" || $("#parentId").val() == null){
				alert("请选择一个模块!");
				return false;
			}
			var moduleName = $("#menuName").val();
			var url = $("#url").val();
			var sortval = $("#sort").val();
			var parentId = $("#parentId").val();
			
			if(type == "del"){
				if(confirm("确认删除?")){
					blockButton1();
					document.forms['moduleForm'].action = 'menu/delete.do';
					document.forms['moduleForm'].submit();
				}
				return;
			}
			
			if(moduleName == "" || moduleName == null){
				alert("模块名有误!");
				return false;
			}else if(url == "" || url == null){
				alert("url有误!");
				return false;
			}else if(parentId == "" || parentId == null){
				alert("请选择一个模块!");
				return false;
			}else if(!check_number(sortval) || sortval>100){
				alert("排序有误!内容必须为小于100的数字");
				$("#sort").focus();
				return false;
			}
			if(type == "add"){
				if(confirm("确认添加?")){
					blockButton1();
					document.forms['moduleForm'].action = 'menu/createOrUpdate.do';
					document.forms['moduleForm'].submit();
				}
			}
		}
		
		function blockButton1(){
			$("#addbutton").attr("disabled",true);
			$("#delbutton").attr("disabled",true);
			$("#relButton").attr("disabled",true);
		}
		
		function releaseButton(){
			$("#edit_moduleName").removeAttr("readonly");
			$("#edit_url").removeAttr("readonly");
			$("#edit_sort").removeAttr("readonly");
			$("#updateSendButton").attr("disabled",false);
			$("#relButton").attr("disabled",false);
		}
		
		function editModule(){
			if($("#edit_ModuleId").val() == "" || $("#edit_ModuleId").val() == null){
				alert("请选择一个模块!");
				return false;
			}
			var moduleName = $("#edit_moduleName").val();
			var url = $("#edit_url").val();
			var sortval = $("#edit_sort").val();
			
			if(!check_number(sortval) || sortval>100){
				alert("排序内容必须为小于100的数字");
				$("#edit_sort").focus();
				return false;
			}
			
			if(moduleName == null || moduleName == ""){
				alert("模块名有误!");
				return false;
			}
			if(confirm("确认修改?")){
				document.forms['editModuleForm'].submit();
			}
		}
   		</script>
	</head>
	<body>
	<DIV CLASS="main">
		<div id="sidebar" style="min-height: 300px;">
			<ul id="treeDemo" class="ztree"><li>数据加载中...</li></ul>
		</div>
		<div class='menu'>
			<font class="f_12">模块添加</font>
		</div>
		<div id="addUserTable">
			<form id="moduleForm" method="post" action="menu/createOrUpdate.do" >
				<input type="hidden" name="type" value="${type }" />
				<table class="editTable">
					<tr>
						<td>模块名称：</td>
						<td><input type="text" id="menuName" name="menuName" /></td>
					</tr>
					<tr>
						<td>模块URL：</td>
						<td><input type="text" id="url" name="url" /></td>
					</tr>
					<tr>
						<td>排序：</td>
						<td><input type="text" id="sort" name="sort" /></td>
					</tr>
					<tr>
						<td>父模块名：</td>
						<td><input type="text" id="parentModuleName" readonly="readonly"/></td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="hidden" name="parent.id" id="parentId"  value="" />
							<input type="button" id="addbutton" onClick="operateModule('add')"  value="添 加" disabled />&nbsp;
							<input type="button" id="delbutton" onClick="operateModule('del')"  value="删 除" disabled />&nbsp;
							<b><font color="red">${result}</font></b>
						</td>
					</tr>
				</table>
			</form>
		</div>
		<div class='menu'>
			<font class="f_12">模块修改</font>
		</div>
		<div class="treeRightDiv">
			<form id="editModuleForm" method="post" action="menu/createOrUpdate.do">
				<input type="hidden" name="type" value="${type }" />
				<table class="editTable">
					<tr>
						<td>模块名称：</td>
						<td><input type="text" id="edit_moduleName" name="menuName" readonly /></td>
					</tr>
					<tr>
						<td>模块URL：</td>
						<td><input type="text" id="edit_url" name="url" readonly /></td>
					</tr>
					<tr>
						<td>排序：</td>
						<td><input type="text" id="edit_sort" name="sort" readonly /></td>
					</tr>
					<tr>
						<td>所属模块：</td>
						<td id="_tdModuleSelect">
							<input type="text" id="edit_parentModuleName" readonly="readonly"/>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="hidden" name="id" id="edit_ModuleId"  value="" />
							<input type="button" id="relButton" onClick="releaseButton()" value="编辑所选" disabled />&nbsp;
							<input type="button" id="updateSendButton" onClick="editModule()" value="提交修改" disabled />&nbsp;
							<b><font color="red">${editResult}</font></b>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</DIV>
	</body>
</html>
