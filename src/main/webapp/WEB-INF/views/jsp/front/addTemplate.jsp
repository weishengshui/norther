<%
// 选择样板
%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<title>选择样板-深圳市诺石行者网络科技有限公司</title>
</head>
<body>

<!--搜索条件-->
<div class="xzyb-1 margin-t-30 w1000">
	<ul class="sstj">
    	<li class="bgfcfcfc frist">
 			<div class="fl"><span>名称：</span><p><input type="text" class="input-list"></p></div>
 			<div class="fl"><span>编号：</span><p><input type="text" class="input-list"></p></div>
    	</li>
    	<li class="bgf7f7f7">
 			<div class="fl"><span>分类：</span><p><a href="#">女戒</a><a href="#">吊坠</a></p></div>
 			<div class="fl"><span>系列：</span><p><a href="#">铂金对戒</a><a href="#">素金</a><a href="#">彩宝</a></p></div>
    	</li>
    	<li class="bgfcfcfc">
 			<div class="fl"><span>款式：</span><p><a href="#">简单</a><a href="#">豪华</a><a href="#">经典</a><a href="#">时尚百变</a></p></div>
 			<div class="fl"><span>金料：</span><p><a href="#">铂金</a><a href="#">18K金</a></p></div>
    	</li>
    	<li class="bgf7f7f7">
 			<div><span>主石重量区间：</span><p><a href="#">0~0.3ct</a><a href="#">0.3~0.5ct</a><a href="#">0.5~0.7ct</a><a href="#">0.7~1ct</a><a href="#">1~2ct</a><a href="#">2~5ct</a></p></div>
    	</li>
    	<li class="bgfcfcfc">
 			<div><span>价格区间：</span><p><a href="#">0~1000元</a><a href="#">1000~2000元</a><a href="#">2000~4000元</a><a href="#">4000~6000元</a><a href="#">6000~20000元</a></p></div>
    	</li>
    	<li class="bgf7f7f7 sl">
 			<div><span>石料：</span><i class="more" id="sl-more"><em class="sq">更多∨</em><em class="zk">更多∧</em></i><p><a href="#">钻石</a><a href="#" class="on">玉石</a><a href="#"  class="on">碧玺</a><a href="#">草莓晶</a><a href="#">发晶</a><a href="#">海蓝宝</a><a href="#">玛瑙</a><a href="#">欧泊</a><a href="#">玉髓</a><a href="#">黑尖晶</a><a href="#">钻石</a><a href="#" >玉石</a><a href="#"  >碧玺</a><a href="#">草莓晶</a><a href="#">发晶</a><a href="#">海蓝宝</a><a href="#">玛瑙</a><a href="#">欧泊</a><a href="#">玉髓</a><a href="#">黑尖晶</a><a href="#">欧泊</a><a href="#">玉髓</a><a href="#">黑尖晶</a><a href="#">钻石</a><a href="#" >玉石</a><a href="#"  >碧玺</a><a href="#">草莓晶</a><a href="#">发晶</a><a href="#">海蓝宝</a><a href="#">玛瑙</a><a href="#">欧泊</a><a href="#">玉髓</a><a href="#">黑尖晶</a><a href="#">欧泊</a><a href="#">玉髓</a><a href="#">黑尖晶</a><a href="#">钻石</a><a href="#" >玉石</a><a href="#"  >碧玺</a><a href="#">草莓晶</a><a href="#">发晶</a><a href="#">海蓝宝</a><a href="#">玛瑙</a><a href="#">欧泊</a><a href="#">玉髓</a><a href="#">黑尖晶</a></p></div>
    	</li>
    	<li class="bgfcfcfc syfw">
 			<div><span>适用范围：</span><i class="more"  id="syfw-more"><em class="sq">更多∨</em><em class="zk">更多∧</em></i><p><a href="#">男士</a><a href="#">女士</a><a href="#">情人节</a><a href="#">中秋节</a><a href="#">圣诞节</a><a href="#">春节</a><a href="#">结婚纪念日</a><a href="#">生日</a><a href="#">婚礼</a><a href="#">婚礼</a><a href="#">婚礼</a><a href="#">婚礼</a><a href="#">婚礼</a><a href="#">婚礼</a></p></div>
                                                                                     
    	</li>
        
        <li class="gkss" id="gkss">
        <div class="zk">展开高级搜索<i>∨</i></div>
        <div class="sq">收起高级搜索<i>∧</i></div>
        
        </li>
    </ul>
</div>
<!--搜索条件End-->

<!--产品列表-->

<ul class="cplb margin-t-30 w1000">
	<li>
    	<img src="images/gwc.jpg" width="180" height="180">
        <div class="cs">
            <h3>甄选钻石女戒1A001P Pt950钻石女戒</h3>
            <p class="fl">编号：1A0432P</p>
            <p class="fl">分类：女戒</p>
            <p class="fl">系列：铂金对戒</p>
            <p class="fl">款式：经典</p>
            <p>金料：铂金</p>
            <p class="bqj">标签价：<span>￥2000</span></p>
            <p class="pfj">批发价：<span>￥1200</span></p>
            <span class="gwc-an"><em></em></span>
        </div>
    </li>
	<li>
    	<img src="images/gwc.jpg" width="180" height="180">
        <div class="cs">
            <h3>甄选钻石女戒1A001P Pt950钻石女戒</h3>
            <p class="fl">编号：1A0432P</p>
            <p class="fl">分类：女戒</p>
            <p class="fl">系列：铂金对戒</p>
            <p class="fl">款式：经典</p>
            <p>金料：铂金</p>
            <p class="bqj">标签价：<span>￥2000</span></p>
            <p class="pfj">批发价：<span>￥1200</span></p>
            <span class="gwc-an"><em></em></span>
        </div>
    </li>
	<li>
    	<img src="images/gwc.jpg" width="180" height="180">
        <div class="cs">
            <h3>甄选钻石女戒1A001P Pt950钻石女戒</h3>
            <p class="fl">编号：1A0432P</p>
            <p class="fl">分类：女戒</p>
            <p class="fl">系列：铂金对戒</p>
            <p class="fl">款式：经典</p>
            <p>金料：铂金</p>
            <p class="bqj">标签价：<span>￥2000</span></p>
            <p class="pfj">批发价：<span>￥1200</span></p>
            <span class="gwc-an"><em></em></span>
        </div>
    </li>
	<li>
    	<img src="images/gwc.jpg" width="180" height="180">
        <div class="cs">
            <h3>甄选钻石女戒1A001P Pt950钻石女戒</h3>
            <p class="fl">编号：1A0432P</p>
            <p class="fl">分类：女戒</p>
            <p class="fl">系列：铂金对戒</p>
            <p class="fl">款式：经典</p>
            <p>金料：铂金</p>
            <p class="bqj">标签价：<span>￥2000</span></p>
            <p class="pfj">批发价：<span>￥1200</span></p>
            <span class="gwc-an"><em></em></span>
        </div>
    </li>
	<li>
    	<img src="images/gwc.jpg" width="180" height="180">
        <div class="cs">
            <h3>甄选钻石女戒1A001P Pt950钻石女戒</h3>
            <p class="fl">编号：1A0432P</p>
            <p class="fl">分类：女戒</p>
            <p class="fl">系列：铂金对戒</p>
            <p class="fl">款式：经典</p>
            <p>金料：铂金</p>
            <p class="bqj">标签价：<span>￥2000</span></p>
            <p class="pfj">批发价：<span>￥1200</span></p>
            <span class="gwc-an"><em></em></span>
        </div>
    </li>
	<li>
    	<img src="images/gwc.jpg" width="180" height="180">
        <div class="cs">
            <h3>甄选钻石女戒1A001P Pt950钻石女戒</h3>
            <p class="fl">编号：1A0432P</p>
            <p class="fl">分类：女戒</p>
            <p class="fl">系列：铂金对戒</p>
            <p class="fl">款式：经典</p>
            <p>金料：铂金</p>
            <p class="bqj">标签价：<span>￥2000</span></p>
            <p class="pfj">批发价：<span>￥1200</span></p>
            <span class="gwc-an"><em></em></span>
        </div>
    </li>
    <div class="blank"></div>

</ul>

<!--产品列表End-->
	<div class="w1000">
    	<ul class="page margin-t-30">
        	<li><a href="#" class="frist">共3页/32条</a></li>
        	<li><a href="#" class="nolink">首页</a></li>
        	<li><a href="#" class="nolink">上一页</a></li>
        	<li><a href="#" class="">下一页</a></li>
        	<li><a href="#" class="">末页</a></li>
        	<li><a href="#" class="frist">1/3</a></li>
        	<li><a href="javascript:void('0')" class="form">
            	<form>转到：
                	<input type="text" class="ys">页
                	<input type="submit" value="Go" class="go-btn">
                </form>
            
            </a></li>
    	</ul>
        <div class="blank"></div>
	</div>
   
<!--右边购物车-->
	<div class="ybgwc">
    	<a href="gwc.html" target="_blank">
    	<em></em>
    	<span>购物车</span>
    	<span><i id='gwcnum'>0</i>件</span>
        </a>
    </div>
<!--右边购物车End-->

</body>
</html>