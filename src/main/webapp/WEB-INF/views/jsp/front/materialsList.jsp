<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<title>资料列表-深圳市诺石行者网络科技有限公司</title>
<script type="text/javascript">
	function checkMaterialsListForm() {
		var currentPage = $('#currentPage').val();
		if(isEmpty(currentPage)){
			alert("请输入页码！");
			$('#currentPage').focus();
			return false;
		}
		if(!check_number(currentPage)) {
			alert("请输入一个数字页码！");
			$('#currentPage').focus();
			return false;
		}
		currentPage = parseInt(currentPage);
		var pageCount = parseInt("${page.pageCount}");
		currentPage = (currentPage < 1) ? 1 : currentPage;
		currentPage = (currentPage > pageCount) ? pageCount : currentPage;
		
		$('#currentPage').val(currentPage);
		
		return true;
	}
</script>
</head>
<body>
<!--资料列表-->
<div class="xwdt w1000 margin-t-30">
	<ul class="new-list">
		<c:forEach items="${page.data }" var="m">
	    	<li>
	        	<div class="fl"><a href="materialsShow.do?menuIndex=menuIndex6&id=${m.id }" target="_self"><img src="${cPath }${m.thumbPath}" width="80" height="80"></a></div>
	            <div class="fr">
	                <h3><a href="materialsShow.do?menuIndex=menuIndex6&id=${m.id }" target="_self"><c:out value="${m.title }"></c:out></a></h3>
	                <span><fmt:formatDate value="${m.publishDate }" type="both" pattern="yyyy-MM-dd" /></span>
	                <p><a href="materialsShow.do?menuIndex=menuIndex6&id=${m.id }" target="_self">
	                	<script type="text/javascript">
	                		var m_brief = unescape("${m.brief}");
	                		if(m_brief.length > 50) {
	                			m_brief = m_brief.substring(0, 50) + "......";
	                		}
	                		document.write(m_brief);
	                	</script>
	                </a></p>
	            </div>
	        </li>
		</c:forEach>
		<c:if test="${empty page.data }">
			<li style="color: red;">没有资料</li>
		</c:if>
    </ul>
    <c:if test="${page.totalCount > 0 }">
    	<ul class="page margin-t-30">
        	<li><a href="javascript:void(0);" class="frist">共${page.pageCount }页/${page.totalCount }条</a></li>
        	<li><a href="materialsList.do?menuIndex=menuIndex6&currentPage=1" class="">首页</a></li>
        	<li><a href="materialsList.do?menuIndex=menuIndex6&currentPage=${(page.currentPage == 1) ? 1 : (page.currentPage - 1) }" class="">上一页</a></li>
        	<li><a href="materialsList.do?menuIndex=menuIndex6&currentPage=${(page.currentPage == page.pageCount) ? page.pageCount : (page.currentPage + 1)}" class="">下一页</a></li>
        	<li><a href="materialsList.do?menuIndex=menuIndex6&currentPage=${page.pageCount }" class="">末页</a></li>
        	<li><a href="javascript:void(0);" class="frist">${page.currentPage}/${page.pageCount }</a></li>
        	<li><a href="javascript:void(0)" class="form">
            	<form action="materialsList.do?menuIndex=menuIndex6" method="post" onsubmit="return checkMaterialsListForm();">转到：
            		<input type="hidden" id="pageSize" name="pageSize" value = "${page.pageSize}" />
                	<input type="text" class="ys" name="currentPage" id="currentPage" value="${page.currentPage }">页
                	<input type="submit" value="Go" class="go-btn">
                </form>
            </a></li>
    	</ul>
    </c:if>
   <div class="blank"></div>

</div>
<!--资料列表End-->
</body>
</html>