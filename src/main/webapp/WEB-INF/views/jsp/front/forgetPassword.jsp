<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<title>忘记密码-深圳市诺石行者网络科技有限公司</title>
<script type="text/javascript">
	$(document).ready(function(){
		var type = "${param.type}";
		var error = "${error}";
		if(type == '' && error == '') {
			alert("申请成功，请注意查收邮件！");
		}
	});
	function checkEmailRandomCode() {
		var email = $('#email').val();
		var verification = $('#verification').val();
		if (email == '') {
			$('#error_span').text("邮箱不能为空！");
			return false;
		}
		if (!isEmail(email)) {
			$('#error_span').text("邮箱格式不正确！");
			return false;
		}
		if (verification == '') {
			$('#error_span').text("验证码不能为空！");
			return false;
		}
		return true;
	}
	function reloadImage() {
		var timenow = new Date().getTime();
		document.getElementById('randomImage').src = 'verificationCode?time='
				+ timenow;
	}
</script>
</head>
<body>

	<!--忘记密码-->
	<div class="xzyb margin-t-30 w1000 wjmm">
		<div class="title-1">重置密码</div>
		<div class="xzyb-con">
			<div class="fl zb">
				<h5>忘记密码？</h5>
				<p>您可以通过您的邮箱重新获取密码</p>
			</div>
			<div class="fl yb">
				<span style="color: red; margin: 0 0 0 150px;" id="error_span">${error}</span>
				<form action="forgetPassword.do?menuIndex=menuIndex0" method="post"
					onsubmit="return checkEmailRandomCode()">
					<div class="itm1">
						<label>邮箱：</label><input type="text" name="email" id="email" value="${email }">
					</div>
					<div class="itm1">
						<label>验证码：</label><input type="text" class="yzm"
							name="verification" id="verification"><a
							href="javascript:reloadImage();"><img name="randomImage"
							id="randomImage"
							src="verificationCode?time=<%=new Date().getTime()%>" width="57"
							height="25"></a>
					</div>
					<div class="itm1">
						<input type="submit" class="btn" value="提交">
					</div>

				</form>
			</div>
		</div>
	</div>
	<!--忘记密码End-->

</body>
</html>