<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<title>诺石伙伴-深圳市诺石行者网络科技有限公司</title>
<script type="text/javascript">
	function checkPartnerListForm() {
		var currentPage = $('#currentPage').val();
		if (isEmpty(currentPage)) {
			alert("请输入页码！");
			$('#currentPage').focus();
			return false;
		}
		if (!check_number(currentPage)) {
			alert("请输入一个数字页码！");
			$('#currentPage').focus();
			return false;
		}
		currentPage = parseInt(currentPage);
		var pageCount = parseInt("${page.pageCount}");
		currentPage = (currentPage < 1) ? 1 : currentPage;
		currentPage = (currentPage > pageCount) ? pageCount : currentPage;

		$('#currentPage').val(currentPage);

		return true;
	}
</script>
</head>
<body>
	<!-- 诺石伙伴 -->
	<div class="nshb margin-t-30 w1000 ">
		<ul class="hb-list">
			<c:forEach items="${page.data }" var="p">
			<li>
				<img src="${cPath }${p.thumbPath}" width="180" height="180">
	            <h3 title="${p.companyName }" style="word-break:break-all;"><c:out value="${p.companyName }"/></h3>
	            <p><span>地址：</span><c:out value="${p.address }" /></p>
	            <p><span>电话：</span><c:out value="${p.phone }" /></p>
			</li>
			</c:forEach>
			<c:if test="${empty page.data }">
<!-- 				<li style="color: red;">没有诺石伙伴</li> -->
				<span style="color: red;">没有诺石伙伴</span>
			</c:if>
		</ul>
		<c:if test="${page.totalCount > 0 }">
			<ul class="page margin-t-30">
				<li><a href="javascript:void(0);" class="frist">共${page.pageCount
						}页/${page.totalCount }条</a></li>
				<li><a href="partnerList.do?menuIndex=menuIndex5&currentPage=1" class="">首页</a></li>
				<li><a
					href="partnerList.do?menuIndex=menuIndex5&currentPage=${(page.currentPage == 1) ? 1 : (page.currentPage - 1) }"
					class="">上一页</a></li>
				<li><a
					href="partnerList.do?menuIndex=menuIndex5&currentPage=${(page.currentPage == page.pageCount) ? page.pageCount : (page.currentPage + 1)}"
					class="">下一页</a></li>
				<li><a href="partnerList.do?menuIndex=menuIndex5&currentPage=${page.pageCount }"
					class="">末页</a></li>
				<li><a href="javascript:void(0);" class="frist">${page.currentPage}/${page.pageCount
						}</a></li>
				<li><a href="javascript:void(0)" class="form">
						<form action="partnerList.do?menuIndex=menuIndex5" method="post"
							onsubmit="return checkPartnerListForm();">
							转到： <input type="hidden" id="pageSize" name="pageSize"
								value="${page.pageSize}" /> <input type="text" class="ys"
								name="currentPage" id="currentPage" value="${page.currentPage }">页
							<input type="submit" value="Go" class="go-btn">
						</form>
				</a></li>
			</ul>
		</c:if>
		<div class="blank"></div>
	</div>
	<!-- 诺石伙伴end -->
</body>
</html>