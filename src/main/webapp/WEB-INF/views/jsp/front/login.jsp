<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<title>登录-深圳市诺石行者网络科技有限公司</title>
<script type="text/javascript">
function checkUsernamePassowrd() {
	var username = $('#loginName').val();
	var password = $('#loginPass').val();
	if(username == '') {
		$('#error_span').text("用户名不能为空！");
		return false;
	}
	if(password == '') {
		$('#error_span').text("密码不能为空！");
		return false;
	}
	return true;
}
</script>
</head>
<body>

<!--前台登录-->
<div class="xzyb margin-t-30 w1000 dl">
    <div class="title-1">请登录</div>
    <div class="xzyb-con">
    	<div class="fl yb">
    		<span style="color: red; margin:0 0 0 150px;" id="error_span">${error}</span>
        	<form action="login.do?menuIndex=menuIndex0" method="post" onsubmit="return checkUsernamePassowrd();">
        		<input type="hidden" name="from" value="${param.from }" />
          		<div class="itm1"><label>账号：</label><input name="loginName" id="loginName" type="text"></div>
          		<div class="itm1"><label>密码：</label><input name="loginPass" id="loginPass" type="password"></div>
          		<div class="itm1"><a target="_self" href="forgetPassword.do?type=enter&menuIndex=menuIndex0">忘记密码</a></div>
          		<div class="itm1"><input type="submit" class="btn" value="登录"></div>
            </form>
    	</div>
    	<div class="fl zb">
        	<h5>欢迎成为诺石合作伙伴</h5>
            <p>您可以免费注册一个分销商账号，随后我们的客服会与您联系</p>
            <a href="agentApply.do?menuIndex=menuIndex3&type=enter" target="_self" class="btn btn-5">立即注册</a>
    	</div>
    </div>
</div>
<!--前台登录End-->

</body>
</html>