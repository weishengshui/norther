<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<title>新闻详情-深圳市诺石行者网络科技有限公司</title>
</head>
<body>
<!-- 新闻详情 -->
<div class="xzyb margin-t-30 w1000 gsjj">
    <div class="title-1"><c:out value="${news.title }"></c:out> <span class="title">发布日期：<fmt:formatDate value="${news.publishdate }" type="both" pattern="yyyy-MM-dd"/></span></div>
    <div class="gsjj-con">
    	<script type="text/javascript">
      		var n_content = "${news.content}";
      		n_content = unescape(n_content);
      		document.write(n_content);
      	</script>
    </div>
</div>
<!-- 新闻详情 End -->
</body>
</html>