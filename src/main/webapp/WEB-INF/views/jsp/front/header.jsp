<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<div class="nav-main w1000">
  <div class="zc"> <a href="login.do?type=enter&menuIndex=menuIndex0" target="_self">登录</a>| <a href="agentApply.do?menuIndex=menuIndex3&type=enter" target="_self" class="zc-btn">注册</a> </div>
  <a href="index.html"><div class="logo">
    <h3>深圳市诺石行者网络科技有限公司</h3>
  </div>
  </a>
  <ul class="nav">
  	<c:forEach items="${applicationScope.menus }" var="first" varStatus="s">
  		<c:choose>
  			<c:when test="${empty first.children }">
  				<c:choose>
  					<c:when test="${fn:contains(first.url, 'index.do')}">
  					<c:choose>
  						<c:when test="${param.menuIndex == null }">
	  						<li><a href="${first.url }" target="_self" class="nava ${param.menuIndex == null ? 'on' : '' }">${first.menuName }</a></li>
  						</c:when>
  						<c:otherwise>
  							<li><a href="${first.url }" target="_self" class="nava ${fn:contains(first.url, param.menuIndex==null ? 'xxxxx' : param.menuIndex) ? 'on' : ''}">${first.menuName }</a></li>
  						</c:otherwise>
  						</c:choose>
  					</c:when>
  					<c:otherwise>
  						<li><a href="${first.url }" target="_self" class="nava ${fn:contains(first.url, param.menuIndex==null ? 'xxxxx' : param.menuIndex) ? 'on' : ''}">${first.menuName }</a></li>
  					</c:otherwise>
  				</c:choose>
  			</c:when>
  			<c:otherwise>
  				<li><a href="javascript:void(0);" target="_self" class="nava ${fn:contains(first.url, param.menuIndex==null ? 'xxxxx' : param.menuIndex) ? 'on' : '' }">${first.menuName }</a>
		        	<ul style="margin-left: ${s.index * 125 -1}px;">
		        		<c:forEach items="${first.children }" var="second">
			            	<li><a href="${second.url }" target="_self">${second.menuName }</a></li>
		        		</c:forEach>
		        	</ul>
		        </li>	
  			</c:otherwise>
  		</c:choose>
  	</c:forEach>
  </ul>
</div>
