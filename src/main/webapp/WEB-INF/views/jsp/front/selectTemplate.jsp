<%
// 选择样板
%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<title>选择样板-深圳市诺石行者网络科技有限公司</title>
<script type="text/javascript">
	function submitAddTemplateForm() {
		var storeCode = $("#code").val();
		if(!storeCode || storeCode == '') {
			alert("请选择一个店面！");
			return false;
		}
		var storeName = $("#code").children("option[selected='selected']").text();
		$("#name").val(storeName);
		return true;
	}
</script>
</head>
<body>

<!--在线申请-->
<div class="xzyb margin-t-30 w1000">
    <div class="title-1">选择样板</div>
    <div class="xzyb-con">
    	<div class="fl zb">
        	<h5>尊敬的分销商</h5>
            <p>您需要在右侧选择一家店面进行选择样板，订单提交后我们客服会与您及时联系！</p>
            <span><i>温馨提示：</i>每笔订单至少30个样板，才可以下单</span>
    	</div>
    	<div class="fl yb">
        	<form action="template/addTemplate.do" method="post" onsubmit="return submitAddTemplateForm();">
            <div class="itm">
            <label>店面：</label>
			<select id="code" name="code">
				<c:forEach items="${stores }" var="store">
					<option value="${store.code }"><c:out value="${store.name }" /></option>
				</c:forEach>
            </select>
            <input type="hidden" name="name" id="name" />
            </div>
            	<input type="submit" value="开始选择样板" class="btn btn-4">
            </form>
    	</div>
    </div>
</div>
<!--在线申请End-->

</body>
</html>