<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<title>视频列表-深圳市诺石行者网络科技有限公司</title>
<!-- Chang URLs to wherever Video.js files will be hosted -->
<link href="js/video-js/video-js.css" rel="stylesheet" type="text/css">
<!-- video.js must be in the <head> for older IEs to work. -->
<script src="js/video-js/video.js"></script>

<!-- Unless using the CDN hosted version, update the URL to the Flash SWF -->
<script>
  videojs.options.flash.swf = "js/video-js/video-js.swf";
</script>
<style type="text/css">
  .vjs-default-skin .vjs-control-bar,
  .vjs-default-skin .vjs-big-play-button { background: rgba(33,33,33,1) }
  .vjs-default-skin .vjs-slider { background: rgba(33,33,33,1) }
</style>
</head>
<body>

<!--视频-->
<div class="sp w1000 margin-t-30">
	<div class="sp-swf">
		<c:choose>
			<c:when test="${fn:endsWith(fn:toLowerCase(video.path), '.swf') }">
				<object type="application/x-shockwave-flash" data="${cPath }${video.path}" 
					width="627" height="200" id="movie_player">
					<param name="allowFullScreen" value="true">
			        <param value="high" name="quality">
			        <param value="always" name="allowScriptAccess">
			        <param value="internal" name="allowNetworking">
			        <param value="transparent" name="wmode">
	            	<param name="flashvars" value="VideoIDS=XNjYxMTAzMzUy&amp;ShowId=0&amp;category=87&amp;Cp=0&amp;Light=on&amp;THX=off&amp;unCookie=0&amp;frame=0&amp;pvid=1409491579049o1o&amp;uepflag=0&amp;Tid=0&amp;isAutoPlay=true&amp;Version=/v1.0.0979&amp;show_ce=0&amp;winType=interior&amp;embedid=AjE2NTI3NTgzOAJ2LmJhaWR1LmNvbQIvbGluaw==&amp;vext=bc%3D%26pid%3D1409491579049o1o%26unCookie%3D0%26frame%3D0%26type%3D0%26svt%3D0%26emb%3DAjE2NTI3NTgzOAJ2LmJhaWR1LmNvbQIvbGluaw%3D%3D%26dn%3D%E7%BD%91%E9%A1%B5%26hwc%3D1%26mtype%3Doth">
	            	<param name="movie" value="${cPath }${video.path}">
	            	<div class="player_html5">
	            	<div class="picture" style="height:100%">
	            	<div style="line-height: 400px;"><span style="font-size:18px">您还没有安装flash播放器,请点击<a href="http://www.adobe.com/go/getflash" target="_blank">这里</a>安装</span>
		            </div>
		            </div>
	            </div>
	            </object>
			</c:when>
			<c:otherwise>
<!-- 			      poster="js/video-js/video-poster.jpg" -->
				<video id="example_video_1" class="video-js vjs-default-skin" autoplay controls preload="none" width="627" height="413"
			      data-setup="{}">
			    <source src="${cPath }${video.path}" type='video/mp4' />
			    <source src="${cPath }${video.path}" type='video/webm' />
			    <source src="${cPath }${video.path}" type='video/ogg' />
			    <p class="vjs-no-js">您的浏览器版本太低！ <a href="http://videojs.com/html5-video-support/" target="_blank">支持的浏览器</a></p>
			  </video>
			</c:otherwise>
		</c:choose>
    </div>
    
    <div class="sp-list">
    	<div class="sp-title">视频列表</div>
        <ul id="sp-ul">
        	<c:forEach items="${videos }" var="v">
        		<li><a href="videoList.do?menuIndex=menuIndex6&id=${v.id }" target="_self"><c:out value="${v.title }"></c:out></a></li>
        	</c:forEach>
        	<c:if test="${empty videos }">
        		<li style="color: red;">没有视频</li>
        	</c:if>
        </ul>
	</div>
</div>
<script>
	$(function(){
		$('#sp-ul li:odd').addClass('a1')
	
	});
</script>
<!--视频End-->
</body>
</html>