<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<title>在线申请-深圳市诺石行者网络科技有限公司</title>
</head>
<body>

<!--在线申请-->
<div class="zxsq margin-t-30 w1000 gwcbg">
	<ul class="zxsq-title">
    	<li><i>1</i>填写申请</li>
    	<li><i>2</i>添加店面</li>
    	<li class="on"><i>3</i>等待审核</li>
    </ul>
    
    <div class="zxsq-bottom zxsq-bottom2">
    	<c:choose>
    		<c:when test="${fn:length(error) == 0 }">
	    	<i></i>
	    	<p>您的申请已提交成功，请等待审核！<br />审核通知会发送到您的手机号码，请注意查收！</p>
    		</c:when>
    		<c:otherwise>保存失败，失败原因：<c:out value="${error }"/></c:otherwise>
    	</c:choose>
   	
    </div>
</div>
<!--在线申请End-->

</body>
</html>