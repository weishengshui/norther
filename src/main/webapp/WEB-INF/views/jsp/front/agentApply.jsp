<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<title>在线申请-深圳市诺石行者网络科技有限公司</title>
</head>
<body>

<!--在线申请-->
<div class="zxsq margin-t-30 w1000">
	<ul class="zxsq-title">
    	<li class="on"><i>1</i>填写申请</li>
    	<li><i>2</i>添加店面</li>
    	<li><i>3</i>等待审核</li>
    </ul>
    <form action="agentApply.do?menuIndex=menuIndex3" method="post" class="form">
    <ul class="zxsq-cont">
    	<li>
        	<label><i>*</i>分销商名称：</label>
            <div class="itms">
            	<input type="text" name="name" class="input1"  datatype="*1-18"  errormsg="请输入正确的公司名称！至少1个字符,最多18个字">
                <p class="Validform_checktip">请填写您的公司名称</p>
            </div>
        </li>
    	<li class="li2">
        	<label><i>*</i>法人代表：</label>
            <div class="itms">
            	<input type="text" name="name" class="input1" datatype="*2-18"  errormsg="请输入正确的法人代表！至少2个字符,最多18个字">
                <p  class="Validform_checktip">请与您的营业执照一致</p>
            </div>
        </li>
    	<li >
        	<label><i>*</i>详细地址：</label>
            <div class="itms" id="city">           
           			<select id="province" name="province"  class="input2" datatype="*1-2" nullmsg="所在省不能为空！" errormsg="请正确填写您所在地区！">         
                    </select>
                    <select id="city1" name="city"  class="input2"    datatype="*"   nullmsg="所在市1不能为空！" errormsg="请输入详细地址">
                    </select>
                    <select id="city2" name="area" class="input2"   datatype="*"   nullmsg="所在县区不能为空！" errormsg="请输入详细地址">
                    </select>
            		<input type="text" name="address" class="input3"  datatype="*1-18"   nullmsg="请输入详细地址!" errormsg="请输入详细地址,至少1个字符,最多18个字">
                <p  class="Validform_checktip">请输入您的详细地址</p>
            </div>
        </li>
    	<li class="li2">
        	<label><i>*</i>联系人：</label>
            <div class="itms">
            	<input type="text" name="header" class="input1" datatype="*2-18"  errormsg="请输入正确的联系人！至少2个字符,最多18个字">
                <p  class="Validform_checktip">方便我们联系您</p>
            </div>
        </li>
    	<li>
        	<label><i>*</i>移动电话：</label>
            <div class="itms">
            	<input type="text" name="telphone" class="input1"  datatype="m" nullmsg="手机号码不能为空！" errormsg="请正确填写您的手机号码！">
                <p class="Validform_checktip">请填写正确的手机号码，稍后我们会将注册信息发送到您的手机号码</p>
            </div>
        </li>
    	<li class="li2">
        	<label><i>*</i>固定电话：</label>
            <div class="itms">
            	<input type="text" name="fixedTelephone" class="input1"  datatype="n6-16" nullmsg="固定电话不能为空！" errormsg="请正确填写您的固定电话！" >
                <p class="Validform_checktip">联系人的座机号码</p>
            </div>
        </li>
    	<li>
        	<label><i>*</i>邮箱：</label>
            <div class="itms">
            	<input type="text" name="email" class="input1"  datatype="e" nullmsg="邮箱不能为空！" errormsg="请正确填写您的邮箱！">
                <p class="Validform_checktip">联系人的邮箱</p>
            </div>
        </li>
    	<li class="li2">
        	<label>QQ：</label>
            <div class="itms">
            	<input type="text" name="qq" class="input1">
                <p>选填内容</p>
            </div>
        </li>
    	<li class="li9">
        	<label>传真：</label>
            <div class="itms">
            	<input type="text" name="fax" class="input1">
                <p>选填内容</p>
            </div>
        </li>
    
    </ul>
    <div class="zxsq-bottom">
    	<input type="submit" value="下一步" class="btn btn-1">
    </div>
    </form>
</div>
<!--在线申请End-->

</body>
</html>