<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<title>在线申请-深圳市诺石行者网络科技有限公司</title>
</head>
<body>

<!--在线申请-->
<div class="zxsq margin-t-30 w1000 gwcbg">
	<ul class="zxsq-title">
    	<li><i>1</i>填写申请</li>
    	<li class="on"><i>2</i>添加店面</li>
    	<li><i>3</i>等待审核</li>
    </ul>
    <div class="tjdm-gwc">
           <p class="p1">店面名称</p>
           <p class="p2">详细地址</p>
           <p class="p4">区域</p>
           <p class="p5">电话</p>
           <p class="p6">邮编</p>
           <p class="p7">说明</p>
 	 </div>
     <ul class="gwcbg-1">
     	<c:forEach items="${stores }" var="store">
     		<li>
     			<p class="p1"><c:out value="${store.name }" /></p>
     			<p class="p2"><c:out value="${store.province }${store.city }${store.area }${store.address }" /></p>
     			<p class="p4"><c:out value="${store.region }"/></p>
     			<p class="p5"><c:out value="${store.telphone }"/></p>
     			<p class="p6"><c:out value="${store.zipCode }"/></p>
     			<p class="p7" title="<c:out value='${store.introduction }'/>"><c:choose>
		                	<c:when test="${fn:length(store.introduction) > 6 }">
		                		<c:out value="${fn:substring(store.introduction, 0, 6) }"/>..
		                	</c:when>
		                	<c:otherwise>
		                		<c:out value="${store.introduction }"/>
		                	</c:otherwise>
		                </c:choose></p>
     		</li>
     	</c:forEach>
     </ul>
    <div class="zxsq-bottom zxsq-bottom1">
   		 <p class="ts">每家店面都将生成一个店主账号，用于在网站中选择样板。如果您有多家店面，请继续添加，否则请提交审核</p>
    	<a href="addStore.do?menuIndex=menuIndex3&type=enter" target="_self" class="btn btn-2">继续添加店面</a>
    	<a href="agentWaitAudit.do?menuIndex=menuIndex3&time=<%=new Date().getTime() %>" target="_self" class="btn btn-3">提交审核</a>
    </div>
</div>
<!--在线申请End-->

</body>
</html>