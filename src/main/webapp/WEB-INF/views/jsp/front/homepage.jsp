<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<title>首页-深圳市诺石行者网络科技有限公司</title>
</head>
<body class="index-bg">

<!--banner-->
<div class="index-banner w1000 margin-t-30">
	<ul class="bd">
		<c:if test="${!empty banners }">
			<c:forEach items="${banners }" var="banner">
				<li style=" background:url(${cPath}${banner.path }) bottom center no-repeat;"><a href="${(empty banner.link) ? 'javascript:void(0);' : banner.link }" target="_self" title="${banner.title }"></a></li>
			</c:forEach>
		</c:if>
    </ul>
	<div class="hd">
    	<ul class="hd1">
			<c:if test="${!empty banners }">
				<c:forEach items="${banners }" var="banner" varStatus="s">
					<c:choose>
						<c:when test="${!empty param.index }">
							<li ${(s.count == param.index) ? "class='on'" : "" }></li>
						</c:when>
						<c:otherwise>
							<li ${(s.index == 0) ? "class='on'" : "" }></li>	
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</c:if>
        </ul>
    </div>
</div>
<!--bannerEnd-->

<!--最新动态-->
<div class="zxdt">
	<div class="w1000">
    	<div class="title"><h3>最新动态</h3><i>Dynamic</i><p><a href="newsList.do?menuIndex=menuIndex4">More</a></p></div>
        <ul>
       		<c:if test="${!empty news }">
       			<c:forEach items="${news }" var="newRecord">
       				<li class="li1">
		            	<a class="a1" target="_self" href="newsShow.do?id=${newRecord.id }&menuIndex=menuIndex4"><img src="${cPath }${newRecord.thumbPath}"><i></i></a>
		                <a class="a2" target="_self" href="newsShow.do?id=${newRecord.id }&menuIndex=menuIndex4"><h3><c:choose>
		                	<c:when test="${fn:length(newRecord.title) > 15 }">
		                		${fn:substring(newRecord.title, 0, 15) }...
		                	</c:when>
		                	<c:otherwise>
		                		${newRecord.title }
		                	</c:otherwise>
		                </c:choose></h3></a>
		                <p class="a3"><fmt:formatDate value="${newRecord.publishdate }" type="both" pattern="yyyy-MM-dd" /></p>
		            </li>
       			</c:forEach>
       		</c:if>
        </ul>
    </div>

</div>
<!--最新动态End-->
</body>
</html>