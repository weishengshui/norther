<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<title>新闻列表-深圳市诺石行者网络科技有限公司</title>
<script type="text/javascript">
	function checkNewsListForm() {
		var currentPage = $('#currentPage').val();
		if(isEmpty(currentPage)){
			alert("请输入页码！");
			$('#currentPage').focus();
			return false;
		}
		if(!check_number(currentPage)) {
			alert("请输入一个数字页码！");
			$('#currentPage').focus();
			return false;
		}
		currentPage = parseInt(currentPage);
		var pageCount = parseInt("${page.pageCount}");
		currentPage = (currentPage < 1) ? 1 : currentPage;
		currentPage = (currentPage > pageCount) ? pageCount : currentPage;
		
		$('#currentPage').val(currentPage);
		
		return true;
	}
</script>
</head>
<body>
<!--新闻动态-->
<div class="xwdt w1000 margin-t-30">
	<ul class="new-list">
		<c:forEach items="${page.data }" var="n">
	    	<li>
	        	<div class="fl"><a href="newsShow.do?menuIndex=menuIndex4&id=${n.id }" target="_self"><img src="${cPath }${n.thumbPath}" width="80" height="80"></a></div>
	            <div class="fr">
	                <h3><a href="newsShow.do?menuIndex=menuIndex4&id=${n.id }" target="_self"><c:out value="${n.title }"></c:out></a></h3>
	                <span><fmt:formatDate value="${n.publishdate }" type="both" pattern="yyyy-MM-dd" /></span>
	                <p><a href="newsShow.do?menuIndex=menuIndex4&id=${n.id }" target="_self">
	                	<script type="text/javascript">
	                		var n_brief = unescape("${n.brief}");
	                		if(n_brief.length > 50) {
	                			n_brief = n_brief.substring(0, 50) + "......";
	                		}
	                		document.write(n_brief);
	                	</script>
	                </a></p>
	            </div>
	        </li>
		</c:forEach>
		<c:if test="${empty page.data }">
			<li style="color: red;">没有新闻</li>
		</c:if>
    </ul>
    <c:if test="${page.totalCount > 0 }">
    	<ul class="page margin-t-30">
        	<li><a href="javascript:void(0);" class="frist">共${page.pageCount }页/${page.totalCount }条</a></li>
        	<li><a href="newsList.do?menuIndex=menuIndex4&currentPage=1" class="">首页</a></li>
        	<!-- nolink -->
        	<li><a href="newsList.do?menuIndex=menuIndex4&currentPage=${(page.currentPage == 1) ? 1 : (page.currentPage - 1) }" class="">上一页</a></li>
        	<li><a href="newsList.do?menuIndex=menuIndex4&currentPage=${(page.currentPage == page.pageCount) ? page.pageCount : (page.currentPage + 1)}" class="">下一页</a></li>
        	<li><a href="newsList.do?menuIndex=menuIndex4&currentPage=${page.pageCount }" class="">末页</a></li>
        	<li><a href="javascript:void(0);" class="frist">${page.currentPage}/${page.pageCount }</a></li>
        	<li><a href="javascript:void(0)" class="form">
            	<form action="newsList.do?menuIndex=menuIndex4" method="post" onsubmit="return checkNewsListForm();">转到：
            		<input type="hidden" id="pageSize" name="pageSize" value = "${page.pageSize}" />
                	<input type="text" class="ys" name="currentPage" id="currentPage" value="${page.currentPage }">页
                	<input type="submit" value="Go" class="go-btn">
                </form>
            </a></li>
    	</ul>
    </c:if>
    <div class="blank"></div>

</div>
<!--新闻动态End-->
</body>
</html>