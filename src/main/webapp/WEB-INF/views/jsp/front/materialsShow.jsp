<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<title>资料详情-深圳市诺石行者网络科技有限公司</title>
</head>
<body>
<!-- 资料详情 -->
<div class="xzyb margin-t-30 w1000 gsjj">
    <div class="title-1">
    	<c:out value="${materials.title }" />
    	<span class="title">发布日期：<fmt:formatDate value="${materials.publishDate }" type="both" pattern="yyyy-MM-dd"/> </span>
    </div>
    <div class="gsjj-con">
    	<script type="text/javascript">
      		var m_content = "${materials.content}";
      		m_content = unescape(m_content);
      		document.write(m_content);
      	</script>
    </div>
</div>
<!-- 资料详情End -->
</body>
</html>