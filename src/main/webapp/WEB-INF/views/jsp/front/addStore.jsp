<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<title>在线申请-深圳市诺石行者网络科技有限公司</title>
<script type="text/javascript">
	$(document).ready(function(){
		$('#region').empty();
		var regions = reciveRegions();
		$(regions).each(function(index, item){
			var option = "<option value='"+item.sequence+"'>"+item.name+"</option>";
			$('#region').append(option);
		});
	});
</script>
</head>
<body>

<!--在线申请-->
<div class="zxsq margin-t-30 w1000">
	<ul class="zxsq-title">
    	<li class="on"><i>1</i>填写申请</li>
    	<li><i>2</i>添加店面</li>
    	<li><i>3</i>等待审核</li>
    </ul>
    <form action="addStore.do?menuIndex=menuIndex3" method="post" class="form">
    <ul class="zxsq-cont">
    	<li>
        	<label><i>*</i>店面名称：</label>
            <div class="itms">
            	<input type="text" class="input1" name="name" datatype="*2-18"  errormsg="请输入正确的店面名称！至少2个字符,最多18个字">
                <p class="Validform_checktip">请填写您的店面名称</p>
            </div>
        </li>
    	<li class="li2">
        	<label><i>*</i>详细地址：</label>
            <div class="itms" id="city">           
           			<select id="province" name="province" class="input2" datatype="*1-2" nullmsg="所在省不能为空！" errormsg="请正确填写您所在地区！">         
                    </select>
                    <select id="city1" name="city" class="input2" datatype="*" nullmsg="所在市1不能为空！" errormsg="请输入详细地址">
                    </select>
                    <select id="city2" name="area" class="input2" datatype="*" nullmsg="所在县区不能为空！" errormsg="请输入详细地址">
                    </select>
            		<input type="text" name="address" class="input3"  datatype="*1-18"   nullmsg="请输入详细地址!" errormsg="请输入详细地址,至少1个字符,最多18个字">
                <p  class="Validform_checktip">请输入您的详细地址</p>
            </div>
        </li>
    	<li class="li2">
        	<label>区域：</label>
            <div class="itms">
            	<select name="region" id="region" class="input1"></select>
                <p  class="Validform_checktip">请填写您所在的区域</p>
            </div>
        </li>
    	<li>
        	<label><i>*</i>电话：</label>
            <div class="itms">
            	<input type="text" name="telphone" class="input1"  datatype="n" nullmsg="手机号码不能为空！" errormsg="请正确填写您的手机号码！只能为数字">
                <p class="Validform_checktip">联系人的电话号码</p>
            </div>
        </li>
    	<li class="li2">
        	<label>邮编：</label>
            <div class="itms">
            	<input type="text" name="zipCode" class="input1"  >
                <p class="Validform_checktip">所在地区的邮编</p>
            </div>
        </li>
    	<li class="li10">
        	<label>说明：</label>
            <div class="itms">
           	<textarea name="introduction" class="input1"></textarea>
            </div>
        </li>
    	
    
    </ul>
    <div class="zxsq-bottom">
    	<input type="submit" value="下一步" class="btn btn-1">
    </div>
    </form>
</div>
<!--在线申请End-->

</body>
</html>