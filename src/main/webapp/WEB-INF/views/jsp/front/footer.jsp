<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="footer margin-t-30">
    	<c:if test="${!empty footerInfo }">
    		<div class="top-footer w1000">
    	<div class="footer-logo"><a href="index.do?menuIndex=menuIndex1" target="_self"><img src="front/images/footer-logo.jpg"></a></div>
		        <div class="footer-lx">
		            <div class="footer-title"><h3>联系我们</h3><span>Contact</span></div>
		            <ul>
		                <li><span>联系电话：</span>${footerInfo.phone }     ${footerInfo.mobile }</li>
		                <li class="fr"><span>QQ：</span>${footerInfo.qq } </li>
		                <li><span>公司地址：</span>${footerInfo.companyAddress }</li>
		                <li class="fr"><span>邮箱：</span>${footerInfo.email }</li>
		                <li><span>新浪微博：</span>${footerInfo.sinaWeibo }</li>
		                <li class="fr"><span>邮编：</span>${footerInfo.zipcode }</li>
		            </ul>
		        </div>
		        <div class="footer-wx">
		            <div class="footer-title"><h3>微信公众号</h3><span>WeiXin</span></div>
		            <img src="${cPath }${footerInfo.twoDimensionalCode }" width="80" height="80">
		            <p>微信公众号<span>${footerInfo.weixin }</span>扫描有惊喜</p>
		        </div>
    </div>
	<div class="bottom-footer w1000">
	备案编号：${footerInfo.recordNo }    ${footerInfo.copyright }
    </div>
   	</c:if>
   	<!-- 统计代码 -->
   	${footerInfo.statisticsCode }
</div>
