<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
$(document).ready(function(){
	$('#chaxun').click(function(){
		var title = $("#title").val();
		var path = $("#path").val();
		var sort = $('#sort').val();
		if(isEmpty(title)){
			alert("图片名称不能为空！");
			$("#title").focus();
			return;
		}
		if(isEmpty(path)){
			alert("请上传图片！");
			$('#path').focus();
			return;
		} 
		if(isEmpty(sort)){
			alert("排序不能为空！");
			$('#sort').focus();
			return;
		} else if(!check_number(sort)){
			alert("请输入正整数排序值！");
			$('#sort').focus();
			return;
		}
		$('#bannerForm').attr("action", "banner/createOrUpdate.do");
		$('#bannerForm').attr("target", "_self");
		$('#bannerForm').submit();
	});
	
});

function uploadBannerImage(){
	var file = $('#file').val();
	if(isEmpty(file)) {
		alert("请先添加图片！");
		return;
	} else if(!checkFilenameImg(file)){
		alert("图片格式不正确！");
		return;
	}
	$('#backGroundDiv').show();
	$('#bannerForm').attr("action", "file/upload.do");
	$('#bannerForm').attr("target", "file_upload_frame");
	$('#bannerForm').submit();
}
</script>


<div class="crmTitle">
  <table width="100%">
    <tr>
      <td width="10%">
      	<div class="defaultab currentTab">
	      	<c:choose>
	      		<c:when test="${empty banner.id }">图片添加</c:when>
	      		<c:otherwise>图片编辑</c:otherwise>
	      	</c:choose>
	      </div>
      </td>
      <td width="*%">&nbsp;</td>
    </tr> 
  </table>
</div>
<br/>
 <form id="bannerForm" name="bannerForm" method="post" action="banner/createOrUpdate.do" 
 enctype="multipart/form-data" target="file_upload_frame">
 	<input type="hidden" name="id" value='<c:out value="${banner.id }"></c:out>'>
 	<input type="hidden" name="forward" value="banner/upload" />
 	<input type="hidden" name="type" value="show">
     <table width="100%"  class="custab2" style="height:100px; border:0px;">
       <tbody>
       	<tr>
       		<td width="8%" class="nameType"><span style="color: red;"> * </span>图片名称：</td>
       		<td width="200px">
       			<input type="text" name="title" id="title" value='<c:out value="${banner.title }"></c:out>' class="int2" />
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType"><span style="color: red;"> * </span>图片地址：</td>
       		<td width="200px" valign="middle">
       			<input type="text" readonly="readonly" name="path" id="path" value='<c:out value="${banner.path }"></c:out>' class="int2" />&nbsp;
       			<input type="file" name="file" id="file" class="int2" >&nbsp;
       			<input type="button" value="上传" onclick="uploadBannerImage()" />&nbsp;
       			<img src="images/htt.gif" onClick="previewImage('<%=request.getContextPath() %>', 'path', '${banner.title }')" title="预览" />
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType">链接地址：</td>
       		<td width="200px">
       			<input type="text" name="link" id="link" value='<c:out value="${banner.link }"></c:out>' class="int2" >
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType"><span style="color: red;"> * </span>排序：</td>
       		<td width="200px">
       			<input type="text" name="sort" id="sort" value='<c:out value="${banner.sort }"></c:out>' class="int2" >&nbsp;值越大排在越前面
       		</td>
       	</tr>
    </tbody>
</table>

<div class="jbcx">
    <table width="100%"  class="custab5">
	    <tbody>
	      <tr>
	         <td width="11%">
	         	<input type="button" id="chaxun" name="chaxun" class="saveBtn" value="保存"/>
	         </td>
	         <td>&nbsp;</td>
	      </tr>
	    </tbody>
    </table>
</div>
</form>
