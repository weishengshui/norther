<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<script type="text/javascript">
	function checkAllCheckBox(el) {
		$("input[name='bannerCheckboxes']").each(function () {
			if (el.checked)
				$(this).attr("checked", true);
			else
				$(this).attr("checked", false);
		});
	}
	
	// 编辑轮换图片
	function editBanner() {
		var count = 0;
		var id = 0;
		$('input[name="bannerCheckboxes"]:checked').each(function(){
			id = $(this).val();
			count++;
		});
		if(count == 0) {
			alert("请选择一行记录！");
		} else if(count > 1) {
			alert("一次只能编辑一行记录！");
		} else {
			var title = $('#banner_title_'+id).val();
			var path = $('#banner_path_'+id).val();
			var link = $('#banner_link_'+id).val();
			var sort = $('#banner_sort_'+id).val();
			$('#title').val(title);
			$('#path').val(path);
			$('#link').val(link);
			$('#sort').val(sort);
			$('#id').val(id);
			showAddBannerDialog('edit');
		}
	}
	
	function addBanner() {
		var bannerCount = $('input[name="bannerCheckboxes"]').length;
		if(bannerCount >= 5){
			alert("最多只能添加5张图片！");
			return;
		}
		showAddBannerDialog('add')
	}
	
	// 删除轮换图片
	function deleteBanners() {
		var count = 0;
		$('input[name="bannerCheckboxes"]:checked').each(function(){
			count++;
		});
		if(count == 0) {
			alert("至少选择一条记录");
			return;
		}
		if(confirm("确认删除？")){
			$('#deleteBannersForm').submit();
		}
	}
	
	// 校验提交banner表单
	function submitCreateOrUpdateBannerForm() {
		var path = $("#path").val();
		var sort = $('#sort').val();
		if(isEmpty(path)){
			alert("请上传图片！");
			$('#path').focus();
			return;
		} 
		if(isEmpty(sort)){
			alert("排序不能为空！");
			$('#sort').focus();
			return;
		} else if(!check_number(sort)){
			alert("请输入正整数排序值！");
			$('#sort').focus();
			return;
		}
		$('#bannerForm').attr("action", "admin/banner/createOrUpdate.do");
		$('#bannerForm').attr("target", "_self");
		$('#bannerForm').submit();
	}
	
	// 打开banner编辑窗口
	function showAddBannerDialog(type) {
		var title = '图片添加';
		if(type == 'add') { // 复位
			$('#title').val('');
			$('#path').val('');
			$('#link').val('');
			$('#sort').val('');
			$('#id').val('');
		} else if(type == 'edit') {// 
			title = "图片修改";
		}
		openDialog('edit_banner_dialog_div',title,450, 230);
	}
	
	var previewBannerImage = function() {
		var basePath = '<%=request.getContextPath()%>';
		previewImage(basePath, 'path', $('#title').val());
	};
	
	// 上传banner图片
	function uploadBannerImage(){
		var file = $('#file').val();
		if(isEmpty(file)) {
			alert("请先添加图片！");
			return;
		} else if(!checkFilenameImg(file)){
			$('#file').val('');
			alert("图片格式不正确！");
			return;
		}
		$('#backGroundDiv').show();
		$('#bannerForm').attr("action", "file/upload.do");
		$('#bannerForm').attr("target", "file_upload_frame");
		$('#bannerForm').submit();
	}
</script>

<div class="crmTitle">
  <table width="100%">
    <tr>
      <td>首页</td>
    </tr> 
  </table>
</div>

<div class="crmList line">
 <form action="admin/banner/deletes.do" id="deleteBannersForm" method="post">
 <div class="crmList">
 <!-- 工具栏 -->
 <table width="100%" class="bigTable">
	<tr>
	  <td width="2%">&nbsp;</td>
	  <td width="71%">      
		<table>
		   <tbody>
				<tr>
					<td width="45px">
						<div class="add" title="添加">
							<a href="javascript:addBanner()" onFocus="this.blur()">
								<span>&nbsp;&nbsp;&nbsp;</span>
							</a>
						</div>
					</td>
					<td width="45px">
						<div class="editor" title="编辑">
							<a href="javascript:editBanner()" onFocus="this.blur()">
							<span>&nbsp;&nbsp;&nbsp;</span>
							</a>
						</div>
					</td>
					<td width="45px">
						<div class="delete" title="删除">
							<a href="javascript:deleteBanners()" onFocus="this.blur()">
								<span>&nbsp;&nbsp;&nbsp;</span>
							</a>
						</div>
					</td>
					<td>&nbsp;</td>
				</tr>
			 </tbody>
		</table>       
		</td>
		<td>&nbsp;</td>
	</tr>
</table>
<!-- 列表数据 -->
  <table width="100%" class="bigTable">                                
    <tr>
      <td>          
       <table width="100%" class="editTable" cellpadding="1">
        <tbody>
			 <tr>
			  <th width="3%"><input type="checkbox" class="check" onClick="checkAllCheckBox(this)" /></th>
			  <th width="10%">序号</th>
			  <th width="10%">图片名称</th>
			  <th width="20%">链接地址</th>
			  <th width="37%">图片地址</th>
			  <th width="10%">操作</th>
			 </tr>
			 <tbody>
				   <c:forEach items="${page.data}" var="banner" varStatus="vs">
						   <tr>
								<td>
									<input type="checkbox" name="bannerCheckboxes" value="${banner.id}" class="check"/>
									<input type="hidden" id="banner_title_${banner.id }" value='<c:out value="${banner.title }"></c:out>' />
									<input type="hidden" id="banner_path_${banner.id }" value='<c:out value="${banner.path }"></c:out>' />
									<input type="hidden" id="banner_link_${banner.id }" value='<c:out value="${banner.link }"></c:out>' />
									<input type="hidden" id="banner_sort_${banner.id }" value='<c:out value="${banner.sort }"></c:out>' />	
								</td>
								<td>
									<c:out value="${banner.sort }"></c:out>
								</td>
								<td>
									<c:out value="${banner.title }"></c:out>
								</td>
								<td>
									<c:out value="${banner.link }"></c:out>
								</td>
								<td>
									<c:out value="${banner.path }"></c:out>
								</td>
								<td>
									<input type="hidden" id="imagepath${vs.count }" value='<c:out value="${banner.path }"></c:out>'>
									<a href="index.do?menuIndex=menuIndex1&index=${vs.count }" target="_blank">预览</a>
								</td>
						   </tr>
				   </c:forEach>
				   <c:if test="${empty page.data}">
				   		<tr>
							<td colspan="4"><b><font color="#FF0000">无相关数据！</font></b></td>
						</tr>
				   </c:if>
			 </tbody>
        </tbody>
       </table>      
      </td>        
    </tr>             
  </table>
</div>
</form>
</div>
<div id="edit_banner_dialog_div" style="display: none;">
	<div class="crmList line">
	 <form id="bannerForm" name="bannerForm" method="post" action="admin/banner/createOrUpdate.do" 
 		enctype="multipart/form-data" target="file_upload_frame">
 		<input type="hidden" name="id" value='' id="id">
 		<input type="hidden" name="forward" value="banner/upload" />
 		<input type="hidden" name="type" value="show">
	   <div class="jbcx">
	     <table width="100%" class="custab4">
	       <tbody>            
	         <tr>
       		 	<td width="80px" class="nameType">图片名称：</td>
       		 	<td>
       				<input type="text" name="title" id="title" class="int2" />
       			</td>
	       	 </tr>
	       	 <tr>
	       		<td class="nameType"><span style="color: red;"> * </span>图片地址：</td>
	       		<td>
	       			<input type="hidden" readonly="readonly" name="path" id="path" class="int2" />
	       			<input type="file" name="file" id="file" onchange="uploadBannerImage()" class="int2" >&nbsp;
	       			<a href="javascript:void(0);" onclick="previewBannerImage()" title="预览" >预览</a>
	       		</td>
	       	</tr>
	       	<tr>
	       		<td class="nameType">链接地址：</td>
	       		<td>
	       			<input type="text" name="link" id="link" class="int2" >
	       		</td>
	       	</tr>
	       	<tr>
	       		<td class="nameType"><span style="color: red;"> * </span>排序：</td>
	       		<td>
	       			<input type="text" name="sort" id="sort" class="int2" >
	       		</td>
	       	</tr>
			 <tr>
				 <td colspan="2">
				   <table width="100%" class="custab5">
					 <tbody>
					   <tr>
						 <td width="10%" align="right">
							<input type="hidden" name="id" value="" id="id"/>
							<input type="button" value="保存" id="save"
							style="margin: 3px 20px 6px 105px" onclick="submitCreateOrUpdateBannerForm()"
							 class="saveBtn" />
						 </td>
						 <td width="45%" align="left">
							<input  type="button" class="cancelBtn" value="取消" 
							style="margin: 3px 20px 6px 0px"
							onClick="closeDialog('edit_banner_dialog_div')"/>
						 </td>
						 <td width="5%">&nbsp;</td>
					   </tr>
					 </tbody>
				   </table>
				 </td>
			   </tr>
	       </tbody>
	     </table>
	    </div>
	 </form>
	</div>
</div>
