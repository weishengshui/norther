<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE script PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title></title>
<script src="<%=path %>/js/jquery-1.8.0.min.js"></script>
<script type="text/javascript">
	var result = ${result};
	if(!result || result.success != true) {
		alert("上传图片失败！");
	} else {
		$(parent.document.getElementById("path")).val(result.data[0]);
	}
	$(parent.document.getElementById("backGroundDiv")).hide();
</script>
</head>
<body>

</body>
</html>
