<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
$(document).ready(function(){
	$('#chaxun').click(function(){
		var phone = $('#phone').val();
		var mobile = $('#mobile').val();
		var companyAddress = $('#companyAddress').val();
		var zipcode = $('#zipcode').val();
		var qq = $('#qq').val();
		var email = $('#email').val();
		var weixin = $('#weixin').val();
		var twoDimensionalCode = $('#twoDimensionalCode').val();
		if(isEmpty(phone)){
			alert("联系电话不能为空！");
			$('#phone').focus();
			return;
		} else if(!checkTelPhone(phone)) {
			alert("联系电话格式不正确！");
			$('#phone').focus();
			return;
		}
		if(isEmpty(mobile)){
			alert("手机号码不能为空！");
			$('#mobile').focus();
			return;
		} else if(!checkMobile(mobile)) {
			alert("手机号码格式不正确！");
			$('#mobile').focus();
			return;
		}
		if(isEmpty(companyAddress)){
			alert("公司地址不能为空！");
			$('#companyAddress').focus();
			return;
		}
		if(isEmpty(zipcode)){
			alert("邮编不能为空！");
			$('#zipcode').focus();
			return;
		}
		if(isEmpty(qq)){
			alert("QQ不能为空！");
			$('#qq').focus();
			return;
		} 
		if(isEmpty(email)){
			alert("邮箱不能为空！");
			$('#email').focus();
			return;
		} else if(!isEmail(email)) {
			alert("邮箱格式不正确！");
			$('#email').focus();
			return;
		}
		if(isEmpty(weixin)){
			alert("微信公众号不能为空！");
			$('#weixin').focus();
			return;
		}
		if(isEmpty(twoDimensionalCode)){
			alert("请上传二维码图片！");
			$('#twoDimensionalCode').focus();
			return;
		}
		
		$('#ComInfoForm').attr("action", "companyInfo/createOrUpdateComInfo.do");
		$('#ComInfoForm').attr("target", "_self");
		$('#ComInfoForm').submit();
	});
});

//上传二维码图片
function uploadCompanyInfoImage(){
	var file = $('#file').val();
	if(isEmpty(file)) {
		alert("请先添加图片！");
		return;
	} else if(!checkFilenameImg(file)){
		$('#file').val('');
		alert("图片格式不正确！");
		return;
	}
	$('#backGroundDiv').show();
	$('#ComInfoForm').attr("action", "file/upload.do");
	$('#ComInfoForm').attr("target", "file_upload_frame");
	$('#ComInfoForm').submit();
}
</script>

<div class="crmTitle">
  <table width="100%">
    <tr>
      <td>
      	<c:choose>
	      		<c:when test="${comInfoType eq 'TS_INTRO' }">TS介绍</c:when>
	      		<c:when test="${comInfoType eq 'COMPANY_INTRO' }">公司简介</c:when>
	      		<c:when test="${comInfoType eq 'CONTACT_US' }">联系我们</c:when>
	      		<c:when test="${comInfoType eq 'FOOTER_INFO' }">底部信息</c:when>
      	</c:choose>
      </td>
    </tr> 
  </table>
</div>
<br/>
 <form id="ComInfoForm" name="ComInfoForm" method="post" action="companyInfo/createOrUpdateComInfo.do"
 	enctype="multipart/form-data">
 	<input type="hidden" name="id" value="<c:out value="${info.id }"></c:out>">
 	<input type="hidden" name="comInfoType" value='<c:out value="${comInfoType }"></c:out>'>
 	<input type="hidden" name="forward" value="footer/upload" />
 	<input type="hidden" name="type" value="show">
     <table width="100%"  class="custab2" style="height:100px; border:0px;">
       <tbody>
       <tr>
       		<td width="8%" class="nameType">版权信息：</td>
       		<td colspan="2">
       			<input type="text" name="copyright" id="copyright" value='<c:out value="${info.copyright }"></c:out>' class="int2" />
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType">备案号：</td>
       		<td colspan="2">
       			<input type="text" name="recordNo" id="recordNo" value='<c:out value="${info.recordNo }"></c:out>' class="int2" />
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType"><span style="color: red;"> * </span>联系电话：</td>
       		<td colspan="2">
       			<input type="text" name="phone" id="phone" value='<c:out value="${info.phone }"></c:out>' class="int2" />
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType"><span style="color: red;"> * </span>手机号码：</td>
       		<td colspan="2">
       			<input type="text" name="mobile" id="mobile" value='<c:out value="${info.mobile }"></c:out>' class="int2" />
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType"><span style="color: red;"> * </span>公司地址：</td>
       		<td colspan="2">
       			<input type="text" name="companyAddress" id="companyAddress" value='<c:out value="${info.companyAddress }"></c:out>' class="int2" />
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType"><span style="color: red;"> * </span>邮编：</td>
       		<td colspan="2">
       			<input type="text" name="zipcode" id="zipcode" value='<c:out value="${info.zipcode }"></c:out>' class="int2" />
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType"><span style="color: red;"> * </span>QQ：</td>
       		<td colspan="2">
       			<input type="text" name="qq" id="qq" value='<c:out value="${info.qq }"></c:out>' class="int2" />
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType"><span style="color: red;"> * </span>邮箱：</td>
       		<td colspan="2">
       			<input type="text" name="email" id="email" value='<c:out value="${info.email }"></c:out>' class="int2" />
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType">新浪微博：</td>
       		<td colspan="2">
       			<input type="text" name="sinaWeibo" id="sinaWeibo" value='<c:out value="${info.sinaWeibo }"></c:out>' class="int2" />
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType"><span style="color: red;"> * </span>微信公众号：</td>
       		<td colspan="2"> 
       			<input type="text" name="weixin" id="weixin" value='<c:out value="${info.weixin }"></c:out>' class="int2" />
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType"><span style="color: red;"> * </span>二维码：</td>
       		<td width="300px">
       			<input type="hidden" name="twoDimensionalCode" id="twoDimensionalCode" value='<c:out value="${info.twoDimensionalCode }"></c:out>' />
       			<input type="file" name="file" id="file" class="int2" onchange="uploadCompanyInfoImage()" />
       			&nbsp;&nbsp;
       		</td>
       		<td>
       			<img alt="图片不存在" src="${cPath }${info.twoDimensionalCode }" id="previewImg" width="50px" height="50px" ${(empty info.twoDimensionalCode) ? "style='display:none;'" : "" }>
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType" valign="top">统计代码：</td>
       		<td colspan="2">
       			<textarea rows="10" cols="80" name="statisticsCode" id="statisticsCode">
       			<c:out value="${info.statisticsCode }"></c:out>
       			</textarea>
       		</td>
       	</tr>
    </tbody>
</table>

<div class="jbcx">
    <table width="100%"  class="custab5">
	    <tbody>
	      <tr>
	         <td width="11%">
	         	<input type="button" id="chaxun" name="chaxun" class="saveBtn" value="保存"/>
	         </td>
	         <td>&nbsp;</td>
	      </tr>
	    </tbody>
    </table>
</div>
</form>
