<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
$(document).ready(function(){
	
	$('#chaxun').click(function(){
		var companyName = $('#companyName').val();
		var thumbPath = $('#thumbPath').val();
		var address = $('#address').val();
		var phone = $('#phone').val();
		if(isEmpty(companyName)){
			alert("公司名称不能为空！");
			$('#companyName').focus();
			return;
		}
		if(isEmpty(thumbPath)){
			alert('请上传缩略图！');
			$('#thumbPath').focus();
			return;
		}
		if(isEmpty(address)){
			alert("地址不能为空！");
			$('#address').focus();
			return;
		}
		if(isEmpty(phone)){
			alert("电话不能为空！");
			$('#phone').focus();
			return;
		}
		if(!(checkTelPhone(phone) || checkMobile(phone))){
			alert("电话格式不正确！");
			$('#phone').focus();
			return;
		}
		
		$('#partnerForm').attr("action", "admin/partner/createOrUpdate.do");
		$('#partnerForm').attr("target", "_self");
		$('#partnerForm').submit();
	});
});

// 上传资料缩略图
function uploadPartnerImage(){
	var file = $('#file').val();
	if(isEmpty(file)) {
		alert("请先添加图片！");
		return;
	} else if(!checkFilenameImg(file)){
		$('#file').val('');
		alert("图片格式不正确！");
		return;
	}
	$('#backGroundDiv').show();
	$('#partnerForm').attr("action", "file/upload.do");
	$('#partnerForm').attr("target", "file_upload_frame");
	$('#partnerForm').submit();
}
</script>


<div class="crmTitle">
  <table width="100%">
    <tr>
      <td>
      	<c:choose>
      		<c:when test="${!empty partner.id }">诺石伙伴编辑</c:when>
      		<c:otherwise>诺石伙伴添加</c:otherwise>
      	</c:choose>
      </td>
    </tr> 
  </table>
</div>
<br/>
 <form id="partnerForm" name="partnerForm" method="post" action="admin/partner/createOrUpdate.do" enctype="multipart/form-data">
 	<input type="hidden" name="id" value="<c:out value="${partner.id }"></c:out>">
     <table width="100%"  class="custab2" style="height:100px; border:0px;">
       <tbody>
       	<tr>
       		<td width="8%" class="nameType"><span style="color: red;"> * </span>公司名称：</td>
       		<td colspan="2">
       			<input type="text" name="companyName" id="companyName" value='<c:out value="${partner.companyName }"></c:out>' class="int2" />
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType"><span style="color: red;"> * </span>缩略图：</td>
       		<td valign="middle" width="330px">	
       			<input type="hidden" name="thumbPath" id="thumbPath" value='<c:out value="${partner.thumbPath }"></c:out>'>
       			<input type="hidden" name="forward" value="partner/upload" />
 				<input type="hidden" name="type" value="show">
       			<input type="file" name="file" id="file" onchange="uploadPartnerImage()" style="width: 250px;"/>&nbsp;&nbsp;
       		</td>
       		<td>
       			<img alt="图片不存在" src="${cPath }${partner.thumbPath }" id="previewImg" width="50px" height="50px" ${(empty partner.thumbPath) ? "style='display:none;'" : "" }>
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType" valign="top"><span style="color: red;"> * </span>地址：</td>
       		<td colspan="2">	
				<input type="text" name="address" id="address" value='<c:out value="${partner.address }"></c:out>' class="int2" />
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType" valign="top"><span style="color: red;"> * </span>电话：</td>
       		<td colspan="2">	
				<input type="text" name="phone" id="phone" value='<c:out value="${partner.phone }"></c:out>' class="int2" />
       		</td>
       	</tr>
    </tbody>
</table>

<div class="jbcx">
    <table width="100%"  class="custab5">
	    <tbody>
	      <tr>
	         <td width="11%">
	         	<input type="button" id="chaxun" name="chaxun" class="saveBtn" value="保存"/>
	         </td>
	         <td>&nbsp;</td>
	      </tr>
	    </tbody>
    </table>
</div>
</form>
