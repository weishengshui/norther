<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<link type="text/css" rel="stylesheet" href="css/common.css">
<link type="text/css" rel="stylesheet" href="css/customerList.css">
<link type="text/css" rel="stylesheet" href="css/jquery.ui.tooltip.css">
<script type="text/javascript" src="js/md5.js"></script>
<script type="text/javascript">
    $(function () {
    	
    	result = "${result}";
    	if(result && result != '') {
    		alert(result);
    	}
    	
		$(".tab").tooltip({
			position: {
				my: "left+5 center",
				at: "right center"
			}
		});
		
    });
    function checkSubmit() {
        var oldName = "${user.username}";
        var oldPwd = $("#oldPwd").val();
        var newPwd = $("#newPwd").val();
        var newPwdAgain = $("#newPwdAgain").val();
        if (oldName.length < 1) {
            alert("登录超时,请重新登录后再修改!");
            return ;
        } else if (hex_md5(oldPwd).toUpperCase() != "${user.password}") {
            alert("原始密码输入不正确，请重新输入!");
            $("#oldPwd").focus();
            return false;
        } else if(newPwd == ''){
        	alert("新密码不能为空！");
        	$("#newPwd").focus();
        	return false;
        } else if (newPwd != newPwdAgain) {
            alert("新密码两次输入不一致!");
            $("#newPwd").focus();
            return false;
        } else if (oldPwd == newPwd) {
            alert("密码未做改变!");
            $("#oldPwd").focus();
            return false;
        }
        $('#updatePwd').submit();
    }
</script>
<div class="crmTitle">
    <table width="100%">
        <tr>
            <td width="94%">修改密码</td>
            <td width="6%">
                <div class="back" title="返回">
                    <a href="javascript:void(0);"
                       onClick="javascript:window.history.go(-1);"
                       onFocus="this.blur()"><span>返回</span></a></div>
            </td>
        </tr>
    </table>
</div>
<div class="add_box_content">
    <form id="updatePwd" name="updatePwd" method="post" action="admin/updatePwd.do">
        <table width="100%" class="tab">
            <tr>
                <th width="26%"><span class="c_red"> * </span>原始密码：</th>
                <td colspan="2">
                    <input type="password" id="oldPwd" name="oldPwd" class="int2"/>
                </td>
            </tr>
            <tr>
                <th class="nameType"><span class="c_red"> * </span>新密码：</th>
                <td colspan="2">
                    <input type="password" id="newPwd" name="newPwd" class="int2"/>
                </td>
            </tr>
            <tr>
                <th class="nameType"><span class="c_red"> * </span>确认密码：</th>
                <td colspan="2">
                    <input type="password" id="newPwdAgain" name="password" class="int2"/>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td colspan="2" align="center" class="btn_td">
                    <input type="button" id="chaxun" name="chaxun" class="saveBtn2" value="保存" onClick="checkSubmit()"/>
                    <input type="button" id="quxiao" name="quxiao" class="closeBtn2" value="取消"
                           onClick="javascript:window.history.go(-1);"/>
                </td>
            </tr>
        </table>
    </form>
</div>
