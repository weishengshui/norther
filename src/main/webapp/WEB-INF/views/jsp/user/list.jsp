<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<script type="text/javascript">
	var totalPageCount = "${page.pageCount}";//总页数

	//本地条件分页查询
	function pageJump(page) {
		PPage("page_select", page, totalPageCount, "pageJump", true);
		$("#currentPage").val(page);
		$('#chaxun').click();
	}

	$(document).ready(function() {
		$('#chaxun').click(function() {
			$('#listForm').submit();
		});
	});

	// 改变每页的记录数
	function changePageSize(obj) {
		$("#currentPage").val(1);
		$("#pageSize").val(obj.value);
		$('#chaxun').click();
	}

	function checkAllCheckBox(el) {
		$("input[name='userCheckboxes']").each(function() {
			if (el.checked)
				$(this).attr("checked", true);
			else
				$(this).attr("checked", false);
		});
	}

	// 编辑新闻
	function editUser() {
		var count = 0;
		var id = 0;
		$('input[name="userCheckboxes"]').each(function(){
			if($(this).attr('checked') == 'checked') {
				id = $(this).val();
				count++;
			}
		});
		if(count == 0) {
			alert("请选择一行记录！");
		} else if(count > 1) {
			alert("一次只能编辑一行记录！");
		} else {
			var username = $('#user_name_'+id).val();
			var account = $('#user_account_'+id).val();
			var roleId = $('#user_roleId_'+id).val();
			$('#username').val(username);
			$('#account').val(account);
			$('#password').val('');
			$('#northRoleId').val(roleId);
			$('#id').val(id);
			showAddUserDialog('edit');
		}
	}

	// 删除用户
	function deleteUsers() {
		var count = 0;
		$('input[name="userCheckboxes"]:checked').each(function() {
			count++;
		});
		if (count == 0) {
			alert("至少选择一条记录！");
			return;
		}
		if (confirm("确认删除？")) {
			$('#deleteUsersForm').submit();
		}

	}
	
	// 打开编辑用户窗口
	function showAddUserDialog(type) {
		var title = '用户添加';
		if(type == 'add') { // 复位
			$('#northRoleId option:first').attr("selected", "selected");
			$('#id').val('');
			$('#account').val('');
			$('#username').val('');
			$('#password').val('${initialPassword}');
		} else if(type == 'edit') {// 修改的时候，只有主动重置密码才修改密码
			title = "用户修改";
		}
		openDialog('edit_user_dialog_div',title,600,240);
	}
	
	// 改变用用户状态
	function changeUserStatus(id, status) {
		var msg = status == 1 ? "确定停用该用户吗？" : "确定启用该用户吗？";
		if(confirm(msg)){
			location.href = "${cPath}/admin/user/changeUserStatus.do?id="+id+"&status="+status;
		}
	}
	
	// 提交更新用户表单
	function submitCreateOrUpdateUserForm() {
		var account = $('#account').val();
		var username = $('#username').val();
		var password = $('#password').val();
		var id = $('#id').val();
		if(isEmpty(account)) {
			alert("账号不能为空！");
			$('#account').val(trim(account));
			$('#account').focus();
			return false;
		} else if(isEmpty(username)) {
			alert("姓名不能为空！");
			$('#username').val(trim(username));
			$('#username').focus();
			return false;
		} else if(isEmpty(password) && id == '') {
			alert("密码不能为空！");
			$('#password').val(trim(password));
			$('#password').focus();
			return false;
		} else if((password.length < 6 && id == '') || (password.length > 0 && password.length < 6 && id != '')) {
			alert("密码的长度不能小于6！");
			$('#password').val(trim(password));
			$('#password').focus();
			return false;
		} 
		
		var result = false;
		// 校验账号是否已经存在
		var param = {};
		param.account = account;
		param.id = $('#id').val();
		$.ajax({
			url: 'admin/user/checkAccountExists.do',
			type: 'post',
			data: param,
			dataType: 'json',
			async: false,
			success: function(data) {
				result = data.success == false;
				if(result == false){
					alert(data.msg);
				}
			}
		});
		return result;
	}
</script>

<div class="crmTitle">
	<table width="100%">
		<tr>
			<td>用户管理</td>
		</tr>
	</table>
</div>

<div class="crmList line">
	<form method="post" action="admin/user/list.do" id="listForm">
		<div class="jbcx">
			<table width="100%" class="custab3">
				<tbody>
					<tr>
						<td width="100px" class="nameType">账号：</td>
						<td width="200px"><input type="text" name="account"
							class="int2" value='<c:out value="${dto.account }" />' /></td>
						<td width="100px" class="nameType">姓名：</td>
						<td width="200px">
						<input type="text" name="username" 
							class="int2" value='<c:out value="${dto.username }" />' />
						</td>
						<td width="100px" class="nameType">状态：</td>
						<td width="19%">
						<select name="userStatus" style="width: 200px;">
							<option value="" ${(empty dto.userStatus) ? "selected" : "" }>全部</option>
							<option value="0" ${(dto.userStatus == 0) ? "selected" : "" }>启用</option>
							<option value="1" ${(dto.userStatus == 1) ? "selected" : "" }>停用</option>
						</select>
						</td>
						<td>&nbsp;</td>
					</tr>
				</tbody>
			</table>
		</div>

		<table width="100%" class="custab3">
			<tr>
				<td width="2%">&nbsp;</td>
				<td width="7%"><input type="hidden" id="pageSize"
					name="pageSize" value="${page.pageSize}" /> <input type="hidden"
					id="currentPage" name="currentPage" value="${page.currentPage}" />
					<input type="button" id="chaxun" class="sureBtn" value="" /></td>
				<td width="91%">&nbsp;</td>
			</tr>
		</table>
	</form>

	<form action="admin/user/deletes.do" id="deleteUsersForm" method="post">
		<div class="crmList">
			<!-- 工具栏 -->
			<table width="100%" class="bigTable">
				<tr>
					<td width="2%">&nbsp;</td>
					<td width="71%">
						<table>
							<tbody>
								<tr>
									<td width="45px">
										<div class="add" title="添加">
											<a href="javascript:showAddUserDialog('add')"
												onFocus="this.blur()"> <span>&nbsp;&nbsp;&nbsp;</span>
											</a>
										</div>
									</td>
									<td width="45px">
										<div class="editor" title="修改">
											<a href="javascript:editUser()" onFocus="this.blur()"> <span>&nbsp;&nbsp;&nbsp;</span>
											</a>
										</div>
									</td>
									<td width="45px">
										<div class="delete" title="删除">
											<a href="javascript:deleteUsers()" onFocus="this.blur()">
												<span>&nbsp;&nbsp;&nbsp;</span>
											</a>
										</div>
									</td>
									<td>&nbsp;</td>
								</tr>
							</tbody>
						</table>
					</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<!-- 列表数据 -->
			<table width="100%" class="bigTable">
				<tr>
					<td>
						<table width="100%" class="editTable" cellpadding="1">
							<tbody>
								<tr>
									<th width="3%"><input type="checkbox" class="check"
										onClick="checkAllCheckBox(this)" /></th>
									<th width="40%">账号</th>
									<th width="30%">姓名</th>
									<th width="10%">所属角色</th>
									<th width="10%">状态</th>
									<th width="10%">操作</th>
								</tr>
							<tbody id="trContent">
								<c:forEach items="${page.data}" var="user">
									<tr>
										<td>
										<input type="hidden" id="user_name_${user.id }" value="${user.username }" />
										<input type="hidden" id="user_account_${user.id }" value="${user.account }" />
										<input type="hidden" id="user_roleId_${user.id }" value="${user.northRole.id }" />
										<input type="hidden" id="user_password_${user.id }" value="${user.password }" />
										<input type="checkbox" name="userCheckboxes"
											value="${user.id}" class="check" /></td>
										<td><c:out value="${user.account }"></c:out></td>
										<td><c:out value="${user.username }"></c:out></td>
										<td><c:out value="${user.northRole.roleName }"></c:out></td>
										<td>
											${user.status == 0 ? "启用" : "停用" }
										</td>
										<td>
											<c:choose>
												<c:when test="${user.status == 0 }">
													<a href="javascript:changeUserStatus(${user.id }, 1)">停用</a>
												</c:when>
												<c:otherwise>
													<a href="javascript:changeUserStatus(${user.id }, 0)">启用</a>
												</c:otherwise>
											</c:choose>
										</td>
									</tr>
								</c:forEach>
								<c:if test="${empty page.data}">
									<tr>
										<td colspan="4"><b><font color="#FF0000">无相关数据！</font></b></td>
									</tr>
								</c:if>
							</tbody>
							</tbody>
						</table>
					</td>
				</tr>

				<tr>
					<td>
						<table width="100%" class="titlePage">
							<tbody>
								<tr>
									<td width="14%">共查询到<span class="red">${(empty page.totalCount) ? 0 : page.totalCount}</span>条记录
									</td>
									<td width="53%" class="paging">
										<div id="page_select" class="page"></div> <script
											type="text/javascript">
											var page = "${page.currentPage}";
											var totalCount = "${page.totalCount}";
											if (totalCount > 0)
												PPage("page_select", page,
														"${page.pageCount}",
														"pageJump", true);
										</script>
									</td>
									<td width="18%"><c:if test="${page.totalCount > 0}">
					每页条数：
				   <select id="setPageSize" onChange="changePageSize(this)">
												<option value="20"
													<c:if test="${page.pageSize==20}">selected="selected"</c:if>>20</option>
												<option value="50"
													<c:if test="${page.pageSize==50}">selected="selected"</c:if>>50</option>
												<option value="100"
													<c:if test="${page.pageSize==100}">selected="selected"</c:if>>100</option>
											</select>
										</c:if></td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</form>
</div>
<div id="edit_user_dialog_div" style="display: none;">
	<div class="crmList line">
	 <form id="create_update_user_form" action="admin/user/createOrUpdate.do" method="post" onsubmit="return submitCreateOrUpdateUserForm();">
	   <input type="hidden" name="type" value="1">
	   <div class="jbcx">
	     <table width="100%" class="custab4">
	       <tbody>            
	         <tr>
	           <td width="70px" class="nameType"><span class="red"> * </span>账号：</td>
	           <td>
			   		<input type="text" name="account" id="account" class="int2" />
			   </td>
	         </tr>
	         <tr>
	           <td class="nameType"><span class="red"> * </span>姓名：</td>
	           <td>
			   		<input type="text" name="username" id="username" class="int2" />
			   </td>
	         </tr>
	         <tr>
	           <td class="nameType"><span class="red"> * </span>密码：</td>
	           <td>
			   		<input type="password" name="password" id="password" class="int2" value="${initialPassword }" />&nbsp;该密码可以强制修改用户密码，为空则不修改
			   </td>
	         </tr>
	         <tr>
	           <td class="nameType"><span class="red"> * </span>角色：</td>
	           <td>
	           	<select name="northRole.id" id="northRoleId" style="width: 200px;">
					<c:forEach items="${roles }" var="role">
						<option value="${role.id }">${role.roleName }</option>
					</c:forEach>
	           	</select>
			   </td>
	         </tr>
			 <tr>
				 <td colspan="2">
				   <table width="100%" class="custab5">
					 <tbody>
					   <tr>
						 <td width="10%" align="right">
							<input type="hidden" name="id" value="" id="id"/>
							<input type="submit" value="保存" id="save"
							style="margin: 3px 20px 6px 169px"
							 class="saveBtn" />
						 </td>
						 <td width="45%" align="left">
							<input  type="button" class="cancelBtn" value="取消" 
							style="margin: 3px 20px 6px 0px"
							onClick="closeDialog('edit_user_dialog_div')"/>
						 </td>
						 <td width="5%">&nbsp;</td>
					   </tr>
					 </tbody>
				   </table>
				 </td>
			   </tr>
	       </tbody>
	     </table>
	    </div>
	 </form>
	</div>
</div>
