<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
$(document).ready(function(){
	var ue = UE.getEditor('container');
	ue.addListener( 'ready', function( editor ) {
		ue.setContent(unescape("${news.content}"));
	 } );
	$('#chaxun').click(function(){
		var content = ue.getContent();
		$('#content').val(escape(content));
		$('#newsForm').submit();
	});
});
</script>


<div class="crmTitle">
  <table width="100%">
    <tr>
      <td width="10%">
	      <div class="defaultab currentTab">
	      	<c:choose>
	      		<c:when test="${!empty news.id }">编辑新闻</c:when>
	      		<c:otherwise>添加新闻</c:otherwise>
	      	</c:choose>
	      </div>
	  </td>
      <td width="*%">&nbsp;</td>
    </tr> 
  </table>
</div>
<br/>
 <form id="newsForm" name="newsForm" method="post" action="news/createOrUpdate.do">
 	<input type="hidden" name="id" value="<c:out value="${news.id }"></c:out>">
 	<input type="hidden" name="createTime" value="<fmt:formatDate value="${news.createTime }" type="both" pattern="yyyy-MM-dd"/>">
     <table width="100%"  class="custab2" style="height:100px; border:0px;">
       <tbody>
       	<tr>
       		<td width="8%" class="nameType">标题：</td>
       		<td>
       			<input type="text" name="title" id="title" value='<c:out value="${news.title }"></c:out>' class="int2" />
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType">记者：</td>
       		<td>
       			<input type="text" name="reporter" id="reporter" value='<c:out value="${news.reporter }"></c:out>' class="int2"/>
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType">发布日期：</td>
       		<td>
       			<input type="text" id="publishdate" name="publishdate" onClick="WdatePicker({isShowClear:false,readOnly:true})" 
       			value='<fmt:formatDate value="${news.publishdate }" type="both" pattern="yyyy-MM-dd"/>' class="Wdate"  style="width:86px"/>
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType" valign="top">正文：</td>
       		<td>
       			<script id="container" type="text/plain">
				</script>
				<input type="hidden" id="content" name="content" value='<c:out value="${news.content }"></c:out>'>
       		</td>
       	</tr>
    </tbody>
</table>

<div class="jbcx">
    <table width="100%"  class="custab5">
	    <tbody>
	      <tr>
	         <td width="11%">
	         	<input type="button" id="chaxun" name="chaxun" class="saveBtn" value="保存"/>
	         </td>
	         <td>&nbsp;</td>
	      </tr>
	    </tbody>
    </table>
</div>
</form>
