<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<script type="text/javascript">
	var totalPageCount = "${page.pageCount}";//总页数
	
	//本地条件分页查询
	function pageJump(page) {
		PPage("page_select", page, totalPageCount, "pageJump", true);
		$("#currentPage").val(page);
		$('#chaxun').click();
	}
	
	$(document).ready(function(){
		$('#frontShow').val("${dto.frontShow }");
		$('#chaxun').click(function(){
			$('#listForm').submit();
		});
	});
	
	// 改变每页的记录数
	function changePageSize(obj){
		$("#currentPage").val(1);
		$("#pageSize").val(obj.value);
		$('#chaxun').click();
	}
	
	function checkAllCheckBox(el) {
		$("input[name='newsCheckboxes']").each(function () {
			if (el.checked)
				$(this).attr("checked", true);
			else
				$(this).attr("checked", false);
		});
	}
	
	// 编辑企业动态
	function editNews() {
		var count = 0;
		var id = 0;
		$('input[name="newsCheckboxes"]:checked').each(function(){
			id = $(this).val();
			count++;
		});
		if(count > 1) {
			alert("一次只能编辑一行记录！");
		} else {
			location.href="${cPath}/admin/company/show.do?menuPath=admin/company/list.do&id="+id;
		}
	}
	
	// 删除企业动态
	function deleteNews() {
		var count = 0;
		$('input[name="newsCheckboxes"]:checked').each(function(){
			count++;
		});
		if(count == 0) {
			alert("至少选择一条记录");
			return;
		}
		if(confirm("确认删除？")){
			$('#deleteNewsForm').submit();
		}
		
	}
</script>

<div class="crmTitle">
  <table width="100%">
    <tr>
      <td>新闻动态</td>
    </tr> 
  </table>
</div>

<div class="crmList line">
 <form method="post" action="admin/company/list.do" id="listForm">
   <div class="jbcx">
     <table width="100%" class="custab3">
       <tbody>            
         <tr>
           <td width="100px" class="nameType">发布日期：</td>
           <td width="200px">
		   		<input type="text" style="width:83px;" name="publishdate1" class="Wdate"
		   		value='<fmt:formatDate value="${dto.publishdate1 }" type="both" pattern="yyyy-MM-dd" />' 
		   		onFocus="WdatePicker({readOnly:true})" />&nbsp;至&nbsp;
		   		<input type="text" style="width:83px;" name="publishdate2" class="Wdate" 
		   		value='<fmt:formatDate value="${dto.publishdate2 }" type="both" pattern="yyyy-MM-dd" />' onFocus="WdatePicker({readOnly:true})" />
		   </td>
           <td width="100px" class="nameType">新闻标题：</td>
           <td width="200px">
           	<input type="text" name="title" class="int2" value='<c:out value="${dto.title }" />' />
		   </td>
		   <td width="100px" class="nameType">首页显示：</td>
           <td width="19%">
           	<select name="frontShow" style="width: 200px;">
           		<option value="">请选择</option>
           		<option value="1" ${dto.frontShow==1 ? 'selected': '' }>显示</option>
           		<option value="0" ${dto.frontShow==0 ? 'selected': '' }>不显示</option>
           	</select>
		   </td>
		   <td>&nbsp;</td>
		  </tr>
       </tbody>
     </table>
    </div>
	
	<table width="100%" class="custab3">
		<tr>
           <td width="2%">&nbsp;</td>        
           <td width="7%">
				<input type="hidden" id="pageSize" name="pageSize" value = "${page.pageSize}" />
				<input type="hidden" id="currentPage" name="currentPage" value="${page.currentPage}" />
				<input type="button" id="chaxun" class="sureBtn" value=""/>
           </td> 
           <td width="91%">&nbsp;</td>
         </tr>
	</table>
 </form>
 
 <form action="admin/company/deletes.do" id="deleteNewsForm" method="post">
 <div class="crmList">
 <!-- 工具栏 -->
 <table width="100%" class="bigTable">
	<tr>
	  <td width="2%">&nbsp;</td>
	  <td width="71%">      
		<table>
		   <tbody>
				<tr>
					<td width="45px">
						<div class="add" title="添加">
							<a href="admin/company/show.do?menuPath=admin/company/list.do" onFocus="this.blur()">
								<span>&nbsp;&nbsp;&nbsp;</span>
							</a>
						</div>
					</td>
					<td width="45px">
						<div class="editor" title="修改">
							<a href="javascript:editNews()" onFocus="this.blur()">
							<span>&nbsp;&nbsp;&nbsp;</span>
							</a>
						</div>
					</td>
					<td width="45px">
						<div class="delete" title="删除">
							<a href="javascript:deleteNews()" onFocus="this.blur()">
								<span>&nbsp;&nbsp;&nbsp;</span>
							</a>
						</div>
					</td>
					<td>&nbsp;</td>
				</tr>
			 </tbody>
		</table>       
		</td>
		<td>&nbsp;</td>
	</tr>
</table>
<!-- 列表数据 -->
  <table width="100%" class="bigTable">                                
    <tr>
      <td>          
       <table width="100%" class="editTable" cellpadding="1">
        <tbody>
			 <tr>
			  <th width="3%"><input type="checkbox" class="check" onClick="checkAllCheckBox(this)" /></th>
			  <th width="30%">新闻标题</th>
			  <th width="5%">首页显示</th>
			  <th width="10%">发布时间</th>
			  <th width="10%">更新时间</th>
			  <th width="10%">发布人</th>
			  <th width="10%">操作</th>
			 </tr>
			 <tbody id="trContent">
				   <c:forEach items="${page.data}" var="news">
						   <tr>
								<td><input type="checkbox" name="newsCheckboxes" value="${news.id}" class="check"/></td>
								<td>
									<c:out value="${news.title }"></c:out>
								</td>
								<td>
									<c:out value="${(news.showFront eq 0) ? '否' : '是' }"></c:out>
								</td>
								<td>
									<fmt:formatDate value="${news.publishdate }" type="both" pattern="yyyy-MM-dd HH:mm:ss"/>
								</td>
								<td>
									<fmt:formatDate value="${news.updateTime }" type="both" pattern="yyyy-MM-dd HH:mm:ss"/>
								</td>
								<td>
									<c:out value="${news.user.username }"></c:out>
								</td>
								<td>
									<a href="newsShow.do?menuIndex=menuIndex4&id=${news.id }" target="_blank">预览</a>
								</td>
						   </tr>
				   </c:forEach>
				   <c:if test="${empty page.data}">
				   		<tr>
							<td colspan="7"><b><font color="#FF0000">无相关数据！</font></b></td>
						</tr>
				   </c:if>
			 </tbody>
        </tbody>
       </table>      
      </td>        
    </tr>             
    
    <tr>
      <td>
        <table width="100%" class="titlePage">
          <tbody>
           <tr>
             <td width="14%">共查询到<span class="red">${page.totalCount}</span>条记录</td>
             <td width="53%" class="paging">
              	<div id="page_select" class="page"></div>
					<script type="text/javascript">
						var page = "${page.currentPage}";
						var totalCount = "${page.totalCount}";
						if(totalCount > 0 )
							PPage("page_select",page,"${page.pageCount}","pageJump",true);
					</script>
             </td>
             <td width="18%">
				 <c:if test="${page.totalCount > 0}">
					每页条数：
				   <select id="setPageSize" onChange="changePageSize(this)">
					  <option value="20" <c:if test="${page.pageSize==20}">selected="selected"</c:if>>20</option>
					 <option value="50" <c:if test="${page.pageSize==50}">selected="selected"</c:if>>50</option>
					 <option value="100" <c:if test="${page.pageSize==100}">selected="selected"</c:if>>100</option>
				   </select>
			   </c:if>
            </td>  
           </tr>
         </tbody>
        </table>
      </td>
    </tr>
  </table>
</div>
</form>
</div>
