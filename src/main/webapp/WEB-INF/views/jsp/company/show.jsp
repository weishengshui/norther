<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
$(document).ready(function(){
	var ue = UE.getEditor('container');
	ue.addListener( 'ready', function( editor ) {
		ue.setContent(unescape("${news.content}"));
	 } );
	
	$('#brief').val(unescape("${news.brief }"));
	
	$('#chaxun').click(function(){
		var content = ue.getContent();
		$('#content').val(escape(content));
		
		var title = $('#title').val();
		var thumbPath = $('#thumbPath').val();
		var brief = $('#brief').val();
		if(isEmpty(title)){
			alert("新闻标题不能为空！");
			$('title').focus();
			return;
		}
		if(isEmpty(thumbPath)){
			alert('请上传缩略图！');
			$('#thumbPath').focus();
			return;
		}
		if(isEmpty(brief)){
			alert("新闻摘要不能为空！");
			$('#brief').focus();
			return;
		}
		$('#brief_').val(escape(brief));
		
		$('#newsForm').attr("action", "admin/company/createOrUpdate.do");
		$('#newsForm').attr("target", "_self");
		$('#newsForm').submit();
	});
});

// 上传新闻缩略图
function uploadNewsImage(){
	var file = $('#file').val();
	if(isEmpty(file)) {
		alert("请先添加图片！");
		return;
	} else if(!checkFilenameImg(file)){
		$('#file').val('');
		alert("图片格式不正确！");
		return;
	}
	$('#backGroundDiv').show();
	$('#newsForm').attr("action", "file/upload.do");
	$('#newsForm').attr("target", "file_upload_frame");
	$('#newsForm').submit();
}
</script>


<div class="crmTitle">
  <table width="100%">
    <tr>
      <td>
      	<c:choose>
      		<c:when test="${!empty news.id }">新闻编辑</c:when>
      		<c:otherwise>新闻添加</c:otherwise>
      	</c:choose>
      </td>
    </tr> 
  </table>
</div>
<br/>
 <form id="newsForm" name="newsForm" method="post" action="admin/company/createOrUpdate.do" enctype="multipart/form-data">
 	<input type="hidden" name="id" value="<c:out value="${news.id }"></c:out>">
     <table width="100%"  class="custab2" style="height:100px; border:0px;">
       <tbody>
       	<tr>
       		<td width="8%" class="nameType"><span style="color: red;"> * </span>新闻标题：</td>
       		<td colspan="2">
       			<input type="text" name="title" id="title" value='<c:out value="${news.title }"></c:out>' class="int2" />
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType"><span style="color: red;"> * </span>首页显示：</td>
       		<td colspan="2">
       			<input type="radio" name="showFront" ${(news.showFront eq 1)? "checked" : ""} value="1"/>是&nbsp;&nbsp;&nbsp;&nbsp;
       			<input type="radio" name="showFront" ${((empty news.showFront) || news.showFront eq 0)? "checked" : ""} value="0" />否
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType"><span style="color: red;"> * </span>缩略图：</td>
       		<td valign="middle" width="330px">	
       			<input type="hidden" name="thumbPath" id="thumbPath" value='<c:out value="${news.thumbPath }"></c:out>'>
       			<input type="hidden" name="forward" value="company/upload" />
 				<input type="hidden" name="type" value="show">
       			<input type="file" name="file" id="file" onchange="uploadNewsImage()" style="width: 250px;"/>&nbsp;&nbsp;
       		</td>
       		<td>
       			<img alt="图片不存在" src="${cPath }${news.thumbPath }" id="previewImg" width="50px" height="50px" ${(empty news.thumbPath) ? "style='display:none;'" : "" }>
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType" valign="top"><span style="color: red;"> * </span>新闻摘要：</td>
       		<td colspan="2">	
       			<input type="hidden" name="brief" value="${news.brief }" id="brief_">
       			<textarea rows="5" cols="100" id="brief">
       			</textarea>
       		</td>
       	</tr>
       	<tr>
       		<td width="8%" class="nameType" valign="top">正文：</td>
       		<td colspan="2">
       			<script id="container" type="text/plain">
				</script>
				<input type="hidden" id="content" name="content" value='<c:out value="${news.content }"></c:out>'>
       		</td>
       	</tr>
    </tbody>
</table>

<div class="jbcx">
    <table width="100%"  class="custab5">
	    <tbody>
	      <tr>
	         <td width="11%">
	         	<input type="button" id="chaxun" name="chaxun" class="saveBtn" value="保存"/>
	         </td>
	         <td>&nbsp;</td>
	      </tr>
	    </tbody>
    </table>
</div>
</form>
