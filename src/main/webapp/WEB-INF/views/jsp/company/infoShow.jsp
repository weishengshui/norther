<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script type="text/javascript">
$(document).ready(function(){
	var ue = UE.getEditor('container');
	ue.addListener( 'ready', function( editor ) {
		ue.setContent(unescape("${info.content}"));
	 } );
	$('#chaxun').click(function(){
		if($('#title').length > 0) {
			var title = $('#title').val();
			if(isEmpty(title)){
				alert("标题不能为空！");
				$('#title').focus();
				return;
			}
		}
		
		var content = ue.getContent();
		$('#content').val(escape(content));
		$('#ComInfoForm').submit();
	});
	
	var success = "${param.success}";
	var comInfoType = "${comInfoType}";
	if(success == 'true'){
		alert("操作成功！");
		if(comInfoType == 'TS_INTRO'){ // TS介绍
			location.href = 'admin/company/showComInfo.do?comInfoType=TS_INTRO';
		} else if(comInfoType == 'COMPANY_INTRO'){ // 公司简介
			location.href = 'admin/company/showComInfo.do?comInfoType=COMPANY_INTRO';
		} else if(comInfoType == 'CONTACT_US'){ // 联系我们
			location.href = 'admin/company/showComInfo.do?comInfoType=CONTACT_US';
		}
	}
});
</script>

<div class="crmTitle">
  <table width="100%">
    <tr>
      <td>
	  	<c:choose>
	      		<c:when test="${comInfoType eq 'TS_INTRO' }">TS介绍</c:when>
	      		<c:when test="${comInfoType eq 'COMPANY_INTRO' }">公司简介</c:when>
	      		<c:when test="${comInfoType eq 'CONTACT_US' }">联系我们</c:when>
	      	</c:choose>
	  </td>
    </tr> 
  </table>
</div>

 <form id="ComInfoForm" name="ComInfoForm" method="post" action="admin/company/createOrUpdateComInfo.do">
 	<input type="hidden" name="id" value="<c:out value="${info.id }"></c:out>">
 	<input type="hidden" name="comInfoType" value='<c:out value="${comInfoType }"></c:out>'>
     <table width="100%"  class="custab2" style="height:100px; border:0px;">
       <tbody>
       <c:if test="${comInfoType != 'CONTACT_US' }">
	       <tr>
	       		<td width="8%" class="nameType"><span style="color: red;"> * </span>标题：</td>
	       		<td>
	       			<input type="text" name="title" id="title" value='<c:out value="${info.title }"></c:out>' class="int2" />
	       		</td>
	       	</tr>
       </c:if>
       	<tr>
       	<c:choose>
       		<c:when test="${comInfoType != 'CONTACT_US' }">
       			<td valign="top" class="nameType">正文：</td>
       			<td>
	       			<script id="container" type="text/plain">
				</script>
					<input type="hidden" id="content" name="content" value='<c:out value="${info.content }"></c:out>'>
	       		</td>
       		</c:when>
       		<c:otherwise>
	       		<td colspan="2">
	       			<script id="container" type="text/plain">
				</script>
					<input type="hidden" id="content" name="content" value='<c:out value="${info.content }"></c:out>'>
	       		</td>
       		</c:otherwise>
       	</c:choose>
       	</tr>
    </tbody>
</table>

<div class="jbcx">
    <table width="100%"  class="custab5">
	    <tbody>
	      <tr>
	         <td width="11%">
	         	<input type="button" id="chaxun" name="chaxun" class="saveBtn" value="保存"/>
	         </td>
	         <td>&nbsp;</td>
	      </tr>
	    </tbody>
    </table>
</div>
</form>
