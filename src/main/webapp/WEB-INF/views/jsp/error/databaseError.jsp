<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isErrorPage="true"%>
<%@page import="java.io.PrintWriter"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>数据库异常</title>
<link type="text/css" rel="stylesheet" href="<%=request.getContextPath() %>/css/common.css">
<style type="text/css">
.error_cont{ width:810px; height:430px; overflow:hidden; margin:0 auto; margin-top:8%;}
.depression{ float:left; width:422px; height:261px; margin-right:20px; _margin-right:10px;}

.error_benkui{ float:left; width:367px;}
.error_benkui img{ cursor:auto; margin-bottom:10px;}
.error_benkui p{ display:block; width:137px; color:#006faf; font-size:18px; height:31px; line-height:31px; margin:0 0 15px 90px;}
.error_benkui p a{ display:block; color:#006faf; font-size:15px; font-weight:bold;}
.error_benkui p a span{ padding-left:38px;}
.error_benkui p span.previousPage{ background:url(<%=request.getContextPath() %>/images/error_return.gif) no-repeat 12px 0;}
.error_benkui p span.home{ background:url(<%=request.getContextPath() %>/images/error_home.gif) no-repeat 12px -1px;}
.error_benkui p a.cur,.error_benkui p a:hover{ background:url(<%=request.getContextPath() %>/images/error_btnBg.png) no-repeat left;}

.error_benkui .jishu{ color:#636b6f; font-size:14px; font-weight:bold; margin-left:50px; line-height:26px; letter-spacing:1px;}
.error_benkui .jishu a{ color:#636b6f; font-weight:normal; letter-spacing:1px; word-break:keep-all;}
.error_benkui .jishu a:hover{ text-decoration:underline;}
</style>
</head>

<body>

<!--主体 -->
<div class="error_cont">
 <div class="depression"><img src="<%=request.getContextPath() %>/images/depression.jpg" width="422" height="261"/></div>
 <div class="error_benkui">
   <img src="<%=request.getContextPath() %>/images/benkui.jpg"  width="367" height="100">
   <p><a href="javascript:void(0);" class="cur" onFocus="this.blur()" onclick="javascript:history.go(-1);"><span class="previousPage">返回上一页</span></a></p>
   <p><a href="<%=request.getContextPath() %>/" onFocus="this.blur()"><span class="home">返回首页</span></a></p>    
   <div class="jishu">
      如有疑问请联系总部技术支持：<br>
      <a href="mailto:657620636@qq.com" onFocus="this.blur()">657620636@qq.com</a>
   </div>
   <div style="color: red;">
   	<%
   		exception.printStackTrace(new PrintWriter(out, true));
   	%>
   </div>
 </div>
</div>

</body>
</html>