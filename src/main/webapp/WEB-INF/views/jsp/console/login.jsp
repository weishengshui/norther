<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.Date"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<link href="front/css/global.css" rel="stylesheet" type="text/css" />
<style type="text/css">

/*后台登陆*/
body	{ background:url(front/images/dlbg.jpg) center no-repeat #448fbf;}
.ht-dl		{ width:500px; height:400px; background:url(front/images/bg1.png); margin:150px auto 0; padding-top:90px;}
.ht-dl	.itms	{ width:100%; height:40px; margin-bottom:20px;}
.ht-dl	.itms-1	{ width:100%; height:40px; margin-bottom:10px;}
.ht-dl	label	{ width:130px; text-align:right; display:block; float:left; color:#808080; line-height:35px; margin-right:5px;}
.ht-dl	input.input-1		{ width:280px; height:35px; border:1px solid #cccccc; padding:0 10px;}
.ht-dl	input.input-2		{ width:160px; height:35px; border:1px solid #cccccc; padding:0 10px;}
.ht-dl	.itms	img			{ float:right; margin-right:100px;}
.ht-dl	.itms	span		{ margin-left:5px; position:relative; top:-2px;}
.ht-dl	span.worrg			{ position:relative; top:-10px; left:130px; color:#ff0000; margin-left:10px;}
.ht-dl	.itms	.dl-sub		{ width:380px; height:40px; margin-left:70px; background:#4c9ec6; color:#fff; border:none; cursor:pointer; font-size:16px; }
.ht-dl	.itms	.dl-sub:hover	{ background:#3a7999;}
</style>
<script type="text/javascript">
$(document).ready(function() {
	if ('${codeFocus}' != '') {
		$("#randomCode").focus();
	} else {
		$("#account").focus();
	}
	if ($.cookie("userAccount") != null) {
		$("#remember").attr("checked", 'checked');
		$("#account").val($.cookie("userAccount"));
	}
});

function login() {
	var name = document.getElementById("account");
	var pwd = document.getElementById("password");
	var randomCode = document.getElementById("randomCode");
	if (isEmpty(name.value)) {
		$("#error_span").text("请您输入用户名！");
		name.focus();
	} else if (isEmpty(pwd.value)) {
		$("#error_span").text("请您输入密码！");
		pwd.focus();
	} else if (isEmpty(randomCode.value)) {
		$("#error_span").text("请您输入验证码！");
		randomCode.focus();
	} else {
		checkRemember();
		document.forms['loginForm'].submit();
	}
}

function checkRemember() {
	if ($("#remember").attr("checked")) {
		$.cookie("userAccount", $("#account").val(), {
			expires : 7,
			path : '/'
		});//存7天
	}
}

function reloadImage() {
	var timenow = new Date().getTime();
	document.getElementById('randomImage').src = 'verificationCode?time='
			+ timenow;
}

function pressEnter(event) {
	if (event.keyCode == 13) {
		login();
		return false;
	}
}
</script>
</head>
<body>
	<div class="ht-dl">
    	<form action="admin/login.do" id="loginForm" method="post" name="loginForm">
        <span class="worrg" id="error_span">${error}</span>  
        	<div class="itms">
        		<label>用户名：</label><input type="text" class="input-1" name="account" id="account" />
            </div>
        	<div class="itms">
        		<label>密码：</label><input type="password" class="input-1" name="password" id="password">
            </div>
        	<div class="itms">
        		<label>验证码：</label><input type="text" class="input-2" onkeypress="pressEnter(event)" id="randomCode" name="verification" />
        		<a href="javascript:reloadImage();"><img 
        		name="randomImage" id="randomImage"
        		src="<%=path%>/verificationCode?time=<%=new Date().getTime() %>" width="72" height="36"></a>
            </div>
        	<div class="itms itms-1">
        	<label>　</label>	<input type="checkbox"  id="remember" onclick="checkRemember()" value="1"><span>记住用户名</span>
            </div>
        	<div class="itms">
            	<input type="button" onclick="login()" class="dl-sub" value="登陆">
            </div>
     	 </form>
    </div>
</body>
</html>