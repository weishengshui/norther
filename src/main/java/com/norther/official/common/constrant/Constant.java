package com.norther.official.common.constrant;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.norther.official.common.util.AppProperties;

/**
 * 常量表
 * 
 * @author sean
 * 
 */
public class Constant {

	/**
	 * 图片轮换 缓存 region
	 */
	public static final String BANNER_CACHE_REGION = "com.norther.official.core.entity.Banner";

	/**
	 * 新闻动态缓存 region
	 */
	public static final String COMPANY_NEWS_REGION = "com.norther.official.core.entity.CompanyNews";

	/**
	 * 用户缓存 region
	 */
	public static final String USER_REGION = "com.norther.official.core.entity.NorthUser";

	/**
	 * 角色缓存 region
	 */
	public static final String ROLE_REGION = "com.norther.official.core.entity.NorthRole";

	/**
	 * 菜单缓存 region
	 */
	public static final String MENU_MODULE_REGION = "com.norther.official.core.entity.MenuModule";

	/**
	 * 视频缓存 region
	 */
	public static final String VIDEO_REGION = "com.norther.official.core.entity.Video";

	/**
	 * 资料缓存 region
	 */
	public static final String MATERIALS_REGION = "com.norther.official.core.entity.Materials";

	/**
	 * 诺石伙伴缓存 region
	 */
	public static final String PARTNER_REGION = "com.norther.official.core.entity.Partner";

	/**
	 * 文件保存的跟路径
	 */
	public final static String FILE_ROOT_PATH = AppProperties
			.get(AppProperties.FILE_PATH);

	/**
	 * 超级用户
	 */
	public final static int ADMIN_USER = 1;

	/**
	 * ueditor json配置文件路径：相对于webRoot
	 */
	public final static String UEDITOR_JSON_PATH = "js/ueditor/jsp";

	/**
	 * MIME类型映射map
	 */
	public final static Map<String, String> MIME_TYPE_MAP = new HashMap<String, String>() {
		private static final long serialVersionUID = 1L;
		{
			put(".gif", "image/gif");
			put(".jpg", "image/jpeg");
			put(".jpeg", "image/jpeg");
			// put(".jpg", "image/jpg");
			put(".png", "image/png");
			put(".bmp", "image/bmp");
			put(".flv", "video/x-flv");
			put(".swf", "application/x-shockwave-flash");
			put(".mp3", "audio/mpeg");
			put(".mp4", "video/mp4");
			put(".mkv", "video/x-matroska");
			put(".avi", "video/x-msvideo");
			put(".rm", "application/vnd.rn-realmedia");
			put(".rmvb", "video/vnd.rn-realvideo");
			put(".mpeg", "video/mpeg");
			put(".mpg", "video/mpeg");
			put(".ogg", "application/ogg");
			put(".ogv", "video/ogg");
			put(".mov", "video/quicktime");
			put(".wmv", "video/x-ms-wmv");
			put(".webm", "video/webm");
			put(".wav", "audio/x-wav");
			put(".mid", "audio/midi");
			put(".rar", "application/x-rar-compressed");
			put(".zip", "audio/application/zip");
			put(".tar", "application/x-tar");
			put(".gz", "application/x-gzip");
			put(".7z", "application/x-7z-compressed");
			put(".bz2", "application/x-bzip2");
			put(".cab", "application/vnd.ms-cab-compressed");
			put(".iso", "application/x-iso9660-image");
			put(".doc", "application/msword");
			put(".docx",
					"application/vnd.openxmlformats-officedocument.wordprocessingml.document");
			put(".xls", "application/vnd.ms-excel");
			put(".xlsx",
					"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			put(".ppt", "audio/application/vnd.ms-powerpoint");
			put(".pptx",
					"application/vnd.openxmlformats-officedocument.presentationml.presentation");
			put(".pdf", "application/pdf");
			put(".txt", "text/plain");
			put(".md", "application/octet-stream"); // 没有找到对应的mime类型：以“二进制”文件类型替代
			put(".xml", "application/xml");
		}
	};

	/**
	 * 基本的初始化
	 */
	static {
		File dir = new File(FILE_ROOT_PATH);
		if (!dir.exists()) {
			dir.mkdirs();
		}
	}

}
