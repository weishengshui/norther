package com.norther.official.common.exception;

/**
 * 诺石接口异常
 * 
 * @author Administrator
 * 
 */
public class NorthWebserviceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NorthWebserviceException() {
		super();
	}

	public NorthWebserviceException(String msg) {
		super(msg);
	}

	public NorthWebserviceException(String msg, Throwable t) {
		super(msg, t);
	}

}
