package com.norther.official.common.exception;

/**
 * 接口参数异常
 * 
 * @author Administrator
 * 
 */
public class ParameterException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ParameterException() {
		super();
	}

	public ParameterException(String msg) {
		super(msg);
	}

	public ParameterException(String msg, Throwable t) {
		super(msg, t);
	}

}
