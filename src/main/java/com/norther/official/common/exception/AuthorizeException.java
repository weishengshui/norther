package com.norther.official.common.exception;

/**
 * 用户名、密码异常
 * 
 * @author Administrator
 * 
 */
public class AuthorizeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AuthorizeException() {
		super();
	}

	public AuthorizeException(String msg) {
		super(msg);
	}

}
