package com.norther.official.common.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AppProperties {

	/**
	 * 初始密码
	 */
	public final static String INITIAL_PASSWORD = "initial_password";

	/*
	 * 是否用的h2数据库
	 */
	public final static String IS_H2 = "is_h2";

	/**
	 * 文件保存路径
	 */
	public final static String FILE_PATH = "filePath";

	private static Properties PRO;

	static {
		InputStream is = AppProperties.class
				.getResourceAsStream("/appConfig.properties");
		PRO = new Properties();
		try {
			PRO.load(is);
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String get(String key) {
		return PRO.getProperty(key);
	}

}
