package com.norther.official.common.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

	public final static SimpleDateFormat YYYYMMDD = new SimpleDateFormat(
			"yyyyMMdd");

	private static final Locale LOCALE = Locale.CHINA;

	public static String getYyyyMmDd(Date date) {

		return YYYYMMDD.format(date);
	}

	/**
	 * 获取当前日期时间
	 * 
	 * @return
	 */
	public static Date getNowDateTime() {

		return getNowCalendar().getTime();
	}

	/**
	 * 获取当前calendar
	 * 
	 * @return
	 */
	public static Calendar getNowCalendar() {

		return Calendar.getInstance(LOCALE);
	}

	/**
	 * 加一天，减一秒
	 * 
	 * @param date
	 * @return
	 */
	public static Date getDateAddOneDayMinusOneSecond(Date date) {

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, 1);
		cal.add(Calendar.SECOND, -1);
		date = cal.getTime();
		return date;
	}

}
