package com.norther.official.config;

import javax.servlet.Filter;

import org.springframework.orm.hibernate4.support.OpenSessionInViewFilter;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.opensymphony.sitemesh.webapp.SiteMeshFilter;

public class Initializer extends
		AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return null;
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[] { WebAppConfig.class };
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

	@Override
	protected Filter[] getServletFilters() {

		OpenSessionInViewFilter openSessionInViewFilter = new OpenSessionInViewFilter();

		CharacterEncodingFilter encodingFilter = new CharacterEncodingFilter();
		encodingFilter.setEncoding("UTF-8");
		encodingFilter.setForceEncoding(true);

		SiteMeshFilter siteMeshFilter = new SiteMeshFilter();

		return new Filter[] { openSessionInViewFilter, encodingFilter,
				siteMeshFilter, new HiddenHttpMethodFilter() };
	}

	// @Override
	// protected Dynamic registerServletFilter(ServletContext servletContext,
	// Filter filter) {
	//
	// ServletRegistration.Dynamic verificationCodeServlet = servletContext
	// .addServlet("VerificationCodeServlet",
	// VerificationCodeServlet.class);
	// verificationCodeServlet.addMapping("/verificationCode");
	//
	// Dynamic registration = super.registerServletFilter(servletContext,
	// filter);
	// return registration;
	// }
}
