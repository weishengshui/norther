package com.norther.official.core.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.norther.official.core.entity.Banner;

/**
 * 图片轮换 service
 * 
 * @author sean
 * 
 */
public interface IBannerService {

	/**
	 * 查询轮换图片
	 * 
	 * @param dto
	 * @param currentPage
	 * @param pageSize
	 * @return
	 */
	Page<Banner> pageQuery(Pageable pageable);

	/**
	 * 根据id查询轮换图片
	 * 
	 * @param id
	 * @return
	 */
	Banner findBannerById(Long id);

	/**
	 * 保存轮换图片
	 * 
	 * @param banner
	 */
	void saveBanner(Banner banner);

	/**
	 * 批量删除轮换图片
	 * 
	 * @param ids
	 * @return
	 */
	int deleteBannersByIds(Long[] ids);

}
