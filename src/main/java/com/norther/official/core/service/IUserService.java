package com.norther.official.core.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.norther.official.core.dto.NorthUserDto;
import com.norther.official.core.entity.NorthUser;

/**
 * 用户管理 service
 * 
 * @author sean
 * 
 */
public interface IUserService {

	/**
	 * 列表查询用户
	 * 
	 * @param dto
	 * @param pageable
	 * @return
	 */
	Page<NorthUser> pageQuery(NorthUserDto dto, Pageable pageable);

	/**
	 * 保存用户
	 * 
	 * @param user
	 * @return
	 */
	NorthUser saveUser(NorthUser user);

	/**
	 * 查询登录用户
	 * 
	 * @param account
	 * @param password
	 * @return
	 */
	NorthUser findUserForLogin(String account, String password);

	/**
	 * 根据id查询用户
	 * 
	 * @param id
	 * @return
	 */
	NorthUser findUserById(Long id);

	/**
	 * 修改用户密码
	 * 
	 * @param id
	 * @param password
	 * @return
	 */
	boolean changeUserPassword(Long id, String password);

	/**
	 * 批量删除用户
	 * 
	 * @param ids
	 * @return
	 */
	int deleteUsers(Long[] ids);
}
