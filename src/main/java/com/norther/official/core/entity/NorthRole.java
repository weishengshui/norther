package com.norther.official.core.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * 角色
 * 
 * @author Administrator
 * 
 */
@Entity
@Table(name = "north_role")
@JsonIgnoreProperties(value = { "northUsers" })
public class NorthRole implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	// 角色名称
	@Column(name = "role_name")
	private String roleName;

	// 用户，一对多
	@OneToMany(mappedBy = "northRole")
	private Set<NorthUser> northUsers;

	// 是否是超级管理员：0不是，1是
	@Column
	private int admin;

	// 角色权限：多对多
	// 主表：维护关系
	@ManyToMany
	@JoinTable(name = "module_role", joinColumns = { @JoinColumn(name = "role_id", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "menu_id", referencedColumnName = "id") })
	private Set<MenuModule> menuModules = new HashSet<MenuModule>();

	// 创建时间
	@Column(name = "create_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTime;

	// 修改时间
	@Column(name = "update_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateTime;

	// 是否删除：0未删除、1已删除
	@Column(name = "is_delete")
	private int isDelete;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Set<NorthUser> getNorthUsers() {
		return northUsers;
	}

	public void setNorthUsers(Set<NorthUser> northUsers) {
		this.northUsers = northUsers;
	}

	public Set<MenuModule> getMenuModules() {
		return menuModules;
	}

	public void setMenuModules(Set<MenuModule> menuModules) {
		this.menuModules = menuModules;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public int getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}

	public int getAdmin() {
		return admin;
	}

	public void setAdmin(int admin) {
		this.admin = admin;
	}

}
