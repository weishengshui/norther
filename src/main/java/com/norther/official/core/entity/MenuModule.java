package com.norther.official.core.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * 菜单
 * 
 * @author Administrator
 * 
 */
@Entity
@Table(name = "menu_module")
@JsonIgnoreProperties(value = { "parent", "northRoles" })
public class MenuModule implements Serializable, Comparable<MenuModule> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	// 菜单名称
	@Column(name = "menu_name")
	private String menuName;

	@Column
	private String url;

	// 排序：值小排在前面
	@Column
	private int sort;

	// 菜单类型：1后台菜单、2前台菜单
	@Column
	private int type;

	// 是否是叶子：0不是、1是
	@Column(name = "is_leaf")
	private boolean isLeaf;

	// 父菜单：一级菜单上面的根是没有的
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "parent_id", referencedColumnName = "id", nullable = true)
	private MenuModule parent;

	@Column(name = "parent_id", insertable = false, updatable = false)
	private Long parentId;

	// 子菜单
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "parent", cascade = { CascadeType.REMOVE })
	@OrderBy("sort asc")
	private Set<MenuModule> children;

	// 角色权限：多对多
	// 从表：不维护关系
	@ManyToMany(mappedBy = "menuModules")
	private Set<NorthRole> northRoles = new HashSet<NorthRole>();

	// 创建时间
	@Column(name = "create_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTime;

	// 修改时间
	@Column(name = "update_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateTime;

	// 是否删除：0未删除、1已删除
	@Column(name = "is_delete")
	private int isDelete;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public MenuModule getParent() {
		return parent;
	}

	public void setParent(MenuModule parent) {
		this.parent = parent;
	}

	public Set<MenuModule> getChildren() {
		return children;
	}

	public void setChildren(Set<MenuModule> children) {
		this.children = children;
	}

	public Set<NorthRole> getNorthRoles() {
		return northRoles;
	}

	public void setNorthRoles(Set<NorthRole> northRoles) {
		this.northRoles = northRoles;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public int getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	@Override
	public int compareTo(MenuModule o) {
		return this.sort - o.sort;
	}

	public boolean getIsLeaf() {
		return isLeaf;
	}

	public void setIsLeaf(boolean isLeaf) {
		this.isLeaf = isLeaf;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof MenuModule) {
			MenuModule role = (MenuModule) obj;
			if (this.id != null && this.id.equals(role.getId())) {
				return true;
			}
		}
		return false;
	}
}
