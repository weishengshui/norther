package com.norther.official.core.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * 城市 entity
 * 
 * @author sean
 * 
 */
@Entity
@Table(name = "s_city")
@JsonIgnoreProperties(value = { "districts" })
public class City implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "CityID")
	private Long id;

	@Column(name = "CityName")
	private String cityName;

	@Column(name = "ZipCode")
	private String zipCode;

	@ManyToOne
	@JoinColumn(name = "ProvinceID", referencedColumnName = "ProvinceID")
	private Province province;

	@OneToMany(mappedBy = "city")
	private Set<District> districts;

	@Column(name = "DateCreated")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTime;

	@Column(name = "DateUpdated")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public Province getProvince() {
		return province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

}
