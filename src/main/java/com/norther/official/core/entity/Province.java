package com.norther.official.core.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * 省份 entity
 * 
 * @author sean
 * 
 */
@Entity
@Table(name = "s_province")
@JsonIgnoreProperties(value = { "cities" })
public class Province implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ProvinceID")
	private Long id;

	@Column(name = "ProvinceName")
	private String provinceName;

	@Column(name = "DateCreated")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTime;

	@Column(name = "DateUpdated")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateTime;

	@OneToMany(mappedBy = "province")
	private Set<City> cities;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Set<City> getCities() {
		return cities;
	}

	public void setCities(Set<City> cities) {
		this.cities = cities;
	}

}
