package com.norther.official.core.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 用户
 * 
 * @author Administrator
 * 
 */
@Entity
@Table(name = "north_user")
public class NorthUser implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	// 姓名
	@Column
	private String username;
	@Column
	private String password;

	// 账号
	@Column(unique = true)
	private String account;

	// 角色，多对一
	@ManyToOne
	@JoinColumn(name = "role_id", referencedColumnName = "id")
	private NorthRole northRole;

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	private Set<CompanyNews> companyNews = new HashSet<CompanyNews>();

	// 创建时间
	@Column(name = "create_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTime;

	// 修改时间
	@Column(name = "update_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateTime;

	// 是否删除：0未删除、1已删除
	@Column(name = "is_delete")
	private int isDelete;

	// 用户状态：0启用、1停用
	@Column
	private int status;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public NorthRole getNorthRole() {
		return northRole;
	}

	public void setNorthRole(NorthRole northRole) {
		this.northRole = northRole;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public int getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Set<CompanyNews> getCompanyNews() {
		return companyNews;
	}

	public void setCompanyNews(Set<CompanyNews> companyNews) {
		this.companyNews = companyNews;
	}

}
