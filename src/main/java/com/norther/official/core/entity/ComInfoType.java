package com.norther.official.core.entity;

/**
 * 企业信息类型
 * 
 * @author sean
 * 
 */
public enum ComInfoType {
	/**
	 * TS 介绍
	 */
	TS_INTRO,
	/**
	 * 公司简介
	 */
	COMPANY_INTRO,
	/**
	 * 联系我们
	 */
	CONTACT_US,

	/**
	 * 底部信息
	 */
	FOOTER_INFO
}
