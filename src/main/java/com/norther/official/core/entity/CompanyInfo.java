package com.norther.official.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 公司信息：TS介绍、公司简介、联系我们
 * 
 * @author sean
 * 
 */
@Entity
@Table(name = "company_info")
public class CompanyInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	// 标题
	@Column
	private String title;

	/**
	 * 信息类型
	 */
	@Column(name = "info_type")
	@Enumerated(EnumType.STRING)
	private ComInfoType comInfoType;

	/**
	 * 内容
	 */
	@Column(columnDefinition = "text")
	private String content;

	// 版权
	@Column
	private String copyright;

	// 备案号
	@Column(name = "record_no")
	private String recordNo;

	// 联系电话
	@Column(name = "phone")
	private String phone;

	// 手机号码
	@Column
	private String mobile;

	// 公司地址
	@Column(name = "company_address")
	private String companyAddress;

	// 邮编
	@Column
	private String zipcode;

	// QQ
	@Column
	private String qq;

	// 邮箱
	@Column
	private String email;

	// 新浪微博
	@Column(name = "sina_weibo")
	private String sinaWeibo;

	// 微信公众号
	@Column(name = "weixin")
	private String weixin;

	// 二维码图片路径
	@Column(name = "two_dimensional_code")
	private String twoDimensionalCode;

	// 统计代码
	@Column(name = "statistics_code", columnDefinition = "text")
	private String statisticsCode;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTime;

	/**
	 * 修改时间
	 */
	@Column(name = "update_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ComInfoType getComInfoType() {
		return comInfoType;
	}

	public void setComInfoType(ComInfoType comInfoType) {
		this.comInfoType = comInfoType;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCopyright() {
		return copyright;
	}

	public void setCopyright(String copyright) {
		this.copyright = copyright;
	}

	public String getRecordNo() {
		return recordNo;
	}

	public void setRecordNo(String recordNo) {
		this.recordNo = recordNo;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSinaWeibo() {
		return sinaWeibo;
	}

	public void setSinaWeibo(String sinaWeibo) {
		this.sinaWeibo = sinaWeibo;
	}

	public String getWeixin() {
		return weixin;
	}

	public void setWeixin(String weixin) {
		this.weixin = weixin;
	}

	public String getTwoDimensionalCode() {
		return twoDimensionalCode;
	}

	public void setTwoDimensionalCode(String twoDimensionalCode) {
		this.twoDimensionalCode = twoDimensionalCode;
	}

	public String getStatisticsCode() {
		return statisticsCode;
	}

	public void setStatisticsCode(String statisticsCode) {
		this.statisticsCode = statisticsCode;
	}

}
