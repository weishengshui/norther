package com.norther.official.core.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 分销商
 * 
 * @author Administrator
 * 
 */
public class AgentDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// 分销商名称
	private String name;

	// 法人
	private String faren;

	// 负责人
	private String header;

	// 省
	private String province;

	// 市
	private String city;

	// 区
	private String area;

	// 详细地址
	private String address;

	// 邮编
	private String zipNumber;

	// 移动电话
	private String telphone;

	// 固定电话
	private String fixedTelephone;

	// 传真
	private String fax;

	// 邮箱
	private String email;

	// QQ
	private String qq;

	// 分销商的店铺列表
	private List<Store> stores;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFaren() {
		return faren;
	}

	public void setFaren(String faren) {
		this.faren = faren;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZipNumber() {
		return zipNumber;
	}

	public void setZipNumber(String zipNumber) {
		this.zipNumber = zipNumber;
	}

	public String getTelphone() {
		return telphone;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	public String getFixedTelephone() {
		return fixedTelephone;
	}

	public void setFixedTelephone(String fixedTelephone) {
		this.fixedTelephone = fixedTelephone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public List<Store> getStores() {
		return stores;
	}

	public void setStores(List<Store> stores) {
		this.stores = stores;
	}

}
