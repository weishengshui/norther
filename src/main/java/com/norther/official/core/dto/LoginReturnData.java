package com.norther.official.core.dto;

import java.io.Serializable;

public class LoginReturnData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Authorize authorize;

	private LoginEntityInfo entityInfo;

	public LoginReturnData() {
	}

	public Authorize getAuthorize() {
		return authorize;
	}

	public void setAuthorize(Authorize authorize) {
		this.authorize = authorize;
	}

	public LoginEntityInfo getEntityInfo() {
		return entityInfo;
	}

	public void setEntityInfo(LoginEntityInfo entityInfo) {
		this.entityInfo = entityInfo;
	}

}
