package com.norther.official.core.dto;

import java.io.Serializable;

public class ProRate implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//
	private String price;

	//
	private String priceType;

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getPriceType() {
		return priceType;
	}

	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}

}
