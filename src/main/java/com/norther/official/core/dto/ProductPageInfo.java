package com.norther.official.core.dto;

import java.io.Serializable;

public class ProductPageInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// 产品类型
	private String productType = "1";

	// 当前页，默认第1页
	private String pageNum = "1";
	// 一页的记录数，默认0
	private String pageSize = "6";

	// 起始价格
	private String beginPrice="0";
	// 截至价格，没有就是0
	private String endPrice = "0";

	// 钻石的起始重量
	private String beginStoneWeight="0";
	// 截至重量，没有限制就是0
	private String endStoneWeight = "0";

	// 是否群镶：1是、0否
	private String goldMount;

	// 钻石形状
	private String goldShape;

	// 分类ID
	private String productCategory;

	// 材质ID
	private String productMaterial;

	public ProductPageInfo() {
	}

	public ProductPageInfo(String productType, String pageNum, String pageSize) {
		super();
		this.productType = productType;
		this.pageNum = pageNum;
		this.pageSize = pageSize;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getPageNum() {
		return pageNum;
	}

	public void setPageNum(String pageNum) {
		this.pageNum = pageNum;
	}

	public String getPageSize() {
		return pageSize;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	public String getBeginPrice() {
		return beginPrice;
	}

	public void setBeginPrice(String beginPrice) {
		this.beginPrice = beginPrice;
	}

	public String getEndPrice() {
		return endPrice;
	}

	public void setEndPrice(String endPrice) {
		this.endPrice = endPrice;
	}

	public String getBeginStoneWeight() {
		return beginStoneWeight;
	}

	public void setBeginStoneWeight(String beginStoneWeight) {
		this.beginStoneWeight = beginStoneWeight;
	}

	public String getEndStoneWeight() {
		return endStoneWeight;
	}

	public void setEndStoneWeight(String endStoneWeight) {
		this.endStoneWeight = endStoneWeight;
	}

	public String getGoldMount() {
		return goldMount;
	}

	public void setGoldMount(String goldMount) {
		this.goldMount = goldMount;
	}

	public String getGoldShape() {
		return goldShape;
	}

	public void setGoldShape(String goldShape) {
		this.goldShape = goldShape;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public String getProductMaterial() {
		return productMaterial;
	}

	public void setProductMaterial(String productMaterial) {
		this.productMaterial = productMaterial;
	}
}
