package com.norther.official.core.dto;

import java.util.Date;

import com.norther.official.core.entity.Materials;

public class MaterialsDto extends Materials {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// 发布日期区间查询条件
	private Date publishDate1;
	private Date publishDate2;

	public Date getPublishDate1() {
		return publishDate1;
	}

	public void setPublishDate1(Date publishDate1) {
		this.publishDate1 = publishDate1;
	}

	public Date getPublishDate2() {
		return publishDate2;
	}

	public void setPublishDate2(Date publishDate2) {
		this.publishDate2 = publishDate2;
	}

}
