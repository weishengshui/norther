package com.norther.official.core.dto;

import java.io.Serializable;

/**
 * http json 响应：是否成功、错误信息
 * 
 * @author Administrator
 * 
 */
public class ResponseResult implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private boolean success;
	private String msg;

	private Long id;

	public ResponseResult() {
	}

	public ResponseResult(boolean success, String msg) {
		super();
		this.success = success;
		this.msg = msg;
	}

	public ResponseResult(boolean success, String msg, Long id) {
		super();
		this.success = success;
		this.msg = msg;
		this.id = id;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
