package com.norther.official.core.dto;

import java.io.Serializable;

public class GoldInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//
	private String colorsNames;

	//
	private String diyTag;

	//
	private Double goldWeight;

	//
	private String goldclassName;

	//
	private String mosaicCount;

	//
	private String mosaicEnd;

	//
	private String mosaicStart;

	//
	private String mountNames;

	//
	private String sequence;

	//
	private String shapeEnNames;

	//
	private String shapeNames;

	//
	private String surfaceCraftNames;

	public String getColorsNames() {
		return colorsNames;
	}

	public void setColorsNames(String colorsNames) {
		this.colorsNames = colorsNames;
	}

	public String getDiyTag() {
		return diyTag;
	}

	public void setDiyTag(String diyTag) {
		this.diyTag = diyTag;
	}

	public Double getGoldWeight() {
		return goldWeight;
	}

	public void setGoldWeight(Double goldWeight) {
		this.goldWeight = goldWeight;
	}

	public String getGoldclassName() {
		return goldclassName;
	}

	public void setGoldclassName(String goldclassName) {
		this.goldclassName = goldclassName;
	}

	public String getMosaicCount() {
		return mosaicCount;
	}

	public void setMosaicCount(String mosaicCount) {
		this.mosaicCount = mosaicCount;
	}

	public String getMosaicEnd() {
		return mosaicEnd;
	}

	public void setMosaicEnd(String mosaicEnd) {
		this.mosaicEnd = mosaicEnd;
	}

	public String getMosaicStart() {
		return mosaicStart;
	}

	public void setMosaicStart(String mosaicStart) {
		this.mosaicStart = mosaicStart;
	}

	public String getMountNames() {
		return mountNames;
	}

	public void setMountNames(String mountNames) {
		this.mountNames = mountNames;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public String getShapeEnNames() {
		return shapeEnNames;
	}

	public void setShapeEnNames(String shapeEnNames) {
		this.shapeEnNames = shapeEnNames;
	}

	public String getShapeNames() {
		return shapeNames;
	}

	public void setShapeNames(String shapeNames) {
		this.shapeNames = shapeNames;
	}

	public String getSurfaceCraftNames() {
		return surfaceCraftNames;
	}

	public void setSurfaceCraftNames(String surfaceCraftNames) {
		this.surfaceCraftNames = surfaceCraftNames;
	}

}
