package com.norther.official.core.dto;

import java.io.Serializable;

/**
 * 通用结果返回
 * 
 * @author sean
 * 
 * @param <T>
 */
public class CommonResult<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private boolean success;

	private T data;

	public CommonResult() {
	}

	public CommonResult(Long id, boolean success, T data) {
		super();
		this.id = id;
		this.success = success;
		this.data = data;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
