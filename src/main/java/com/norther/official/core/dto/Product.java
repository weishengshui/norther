package com.norther.official.core.dto;

import java.io.Serializable;
import java.util.List;

public class Product implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//
	private String anonymous;

	//
	private String brandNames;

	//
	private String categoryNames;

	//
	private String code;

	//
	private String createTime;

	//
	private String fingerSizeNames;

	//
	private List<GoldInfo> goldInfos;

	//
	private String goldNames;

	//
	private String goldclassWeight;

	//
	private String id;

	//
	private String justTag;

	//
	private String kind;

	//
	private String logicDelTag;

	//
	private String mainStoneWeight;

	//
	private String name;

	//
	private List<ProductPicture> pictures;

	//
	private List<ProRate> proRates;

	//
	private String productColorNames;

	//
	private String ptype;

	//
	private List<String> rates;

	//
	private String readwrite;

	//
	private String scopeNames;

	//
	private String sequence;

	//
	private String seriesNames;

	//
	private List<String> services;

	//
	private String sources;

	//
	private String status;

	//
	private List<StoneInfo> stoneInfos;

	//
	private String stoneNames;

	//
	private String stoneWeight;

	//
	private String styleNames;

	//
	private String substoneWeight;

	//
	private String totalWeight;

	public String getAnonymous() {
		return anonymous;
	}

	public void setAnonymous(String anonymous) {
		this.anonymous = anonymous;
	}

	public String getBrandNames() {
		return brandNames;
	}

	public void setBrandNames(String brandNames) {
		this.brandNames = brandNames;
	}

	public String getCategoryNames() {
		return categoryNames;
	}

	public void setCategoryNames(String categoryNames) {
		this.categoryNames = categoryNames;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getFingerSizeNames() {
		return fingerSizeNames;
	}

	public void setFingerSizeNames(String fingerSizeNames) {
		this.fingerSizeNames = fingerSizeNames;
	}

	public List<GoldInfo> getGoldInfos() {
		return goldInfos;
	}

	public void setGoldInfos(List<GoldInfo> goldInfos) {
		this.goldInfos = goldInfos;
	}

	public String getGoldNames() {
		return goldNames;
	}

	public void setGoldNames(String goldNames) {
		this.goldNames = goldNames;
	}

	public String getGoldclassWeight() {
		return goldclassWeight;
	}

	public void setGoldclassWeight(String goldclassWeight) {
		this.goldclassWeight = goldclassWeight;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getJustTag() {
		return justTag;
	}

	public void setJustTag(String justTag) {
		this.justTag = justTag;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getLogicDelTag() {
		return logicDelTag;
	}

	public void setLogicDelTag(String logicDelTag) {
		this.logicDelTag = logicDelTag;
	}

	public String getMainStoneWeight() {
		return mainStoneWeight;
	}

	public void setMainStoneWeight(String mainStoneWeight) {
		this.mainStoneWeight = mainStoneWeight;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ProductPicture> getPictures() {
		return pictures;
	}

	public void setPictures(List<ProductPicture> pictures) {
		this.pictures = pictures;
	}

	public List<ProRate> getProRates() {
		return proRates;
	}

	public void setProRates(List<ProRate> proRates) {
		this.proRates = proRates;
	}

	public String getProductColorNames() {
		return productColorNames;
	}

	public void setProductColorNames(String productColorNames) {
		this.productColorNames = productColorNames;
	}

	public String getPtype() {
		return ptype;
	}

	public void setPtype(String ptype) {
		this.ptype = ptype;
	}

	public List<String> getRates() {
		return rates;
	}

	public void setRates(List<String> rates) {
		this.rates = rates;
	}

	public String getReadwrite() {
		return readwrite;
	}

	public void setReadwrite(String readwrite) {
		this.readwrite = readwrite;
	}

	public String getScopeNames() {
		return scopeNames;
	}

	public void setScopeNames(String scopeNames) {
		this.scopeNames = scopeNames;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public String getSeriesNames() {
		return seriesNames;
	}

	public void setSeriesNames(String seriesNames) {
		this.seriesNames = seriesNames;
	}

	public List<String> getServices() {
		return services;
	}

	public void setServices(List<String> services) {
		this.services = services;
	}

	public String getSources() {
		return sources;
	}

	public void setSources(String sources) {
		this.sources = sources;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<StoneInfo> getStoneInfos() {
		return stoneInfos;
	}

	public void setStoneInfos(List<StoneInfo> stoneInfos) {
		this.stoneInfos = stoneInfos;
	}

	public String getStoneNames() {
		return stoneNames;
	}

	public void setStoneNames(String stoneNames) {
		this.stoneNames = stoneNames;
	}

	public String getStoneWeight() {
		return stoneWeight;
	}

	public void setStoneWeight(String stoneWeight) {
		this.stoneWeight = stoneWeight;
	}

	public String getStyleNames() {
		return styleNames;
	}

	public void setStyleNames(String styleNames) {
		this.styleNames = styleNames;
	}

	public String getSubstoneWeight() {
		return substoneWeight;
	}

	public void setSubstoneWeight(String substoneWeight) {
		this.substoneWeight = substoneWeight;
	}

	public String getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(String totalWeight) {
		this.totalWeight = totalWeight;
	}

}
