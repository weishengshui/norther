package com.norther.official.core.dto;

import java.io.Serializable;

/**
 * 店面
 * 
 * @author Administrator
 * 
 */
public class Store implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// 店面名称
	private String name;

	// 编码
	private String code;

	private String sequence;

	private String regionId;

	// 省
	private String province;

	// 市
	private String city;

	// 区
	private String area;

	// 详细地址
	private String address;

	// 移动电话
	private String telphone;

	// 邮编
	private String zipCode;

	// 简介
	private String introduction;

	public Store() {
	}

	public Store(String name, String code, String sequence, String regionId,
			String province, String city, String area, String address,
			String telphone, String zipCode, String introduction) {
		super();
		this.name = name;
		this.code = code;
		this.sequence = sequence;
		this.regionId = regionId;
		this.province = province;
		this.city = city;
		this.area = area;
		this.address = address;
		this.telphone = telphone;
		this.zipCode = zipCode;
		this.introduction = introduction;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTelphone() {
		return telphone;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public String getRegionId() {
		return regionId;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

}
