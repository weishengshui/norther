package com.norther.official.core.dto;

import java.util.Date;

import com.norther.official.core.entity.CompanyNews;

/**
 * 企业动态查询条件
 * 
 * @author Administrator
 * 
 */
public class CompanyNewsDto extends CompanyNews {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// 发布日期区间查询条件
	private Date publishdate1;
	private Date publishdate2;

	// 首页显示条件：0不显示、1显示。
	private Integer frontShow;

	public Date getPublishdate1() {
		return publishdate1;
	}

	public void setPublishdate1(Date publishdate1) {
		this.publishdate1 = publishdate1;
	}

	public Date getPublishdate2() {
		return publishdate2;
	}

	public void setPublishdate2(Date publishdate2) {
		this.publishdate2 = publishdate2;
	}

	public Integer getFrontShow() {
		return frontShow;
	}

	public void setFrontShow(Integer frontShow) {
		this.frontShow = frontShow;
	}

}
