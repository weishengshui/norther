package com.norther.official.core.dto;

import java.io.Serializable;

public class ProductType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// 商品分类中文名
	private String cnname;

	// 商品分类英文名
	private String enname;

	// 顺序
	private String sequence;
	
	public ProductType() {
	}

	public String getCnname() {
		return cnname;
	}

	public void setCnname(String cnname) {
		this.cnname = cnname;
	}

	public String getEnname() {
		return enname;
	}

	public void setEnname(String enname) {
		this.enname = enname;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

}
