package com.norther.official.core.dto;

import java.util.Date;

import com.norther.official.core.entity.IndustoryNews;

/**
 * 行业新闻查询条件
 * 
 * @author Administrator
 * 
 */
public class IndustoryNewsDto extends IndustoryNews {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// 发布日期区间查询条件
	private Date publishdate1;
	private Date publishdate2;

	public Date getPublishdate1() {
		return publishdate1;
	}

	public void setPublishdate1(Date publishdate1) {
		this.publishdate1 = publishdate1;
	}

	public Date getPublishdate2() {
		return publishdate2;
	}

	public void setPublishdate2(Date publishdate2) {
		this.publishdate2 = publishdate2;
	}

}
