package com.norther.official.core.dto;

import com.norther.official.core.entity.Shop;

/**
 * 店铺查询条件
 * 
 * @author Administrator
 *
 */
public class ShopDto extends Shop {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
