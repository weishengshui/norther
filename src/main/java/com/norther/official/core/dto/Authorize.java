package com.norther.official.core.dto;

import java.io.Serializable;

public class Authorize implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String loginName;
	private String loginPass;
	private String createTime;

	public Authorize() {
	}

	public Authorize(String loginName, String loginPass) {
		super();
		this.loginName = loginName;
		this.loginPass = loginPass;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getLoginPass() {
		return loginPass;
	}

	public void setLoginPass(String loginPass) {
		this.loginPass = loginPass;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

}
