package com.norther.official.core.dto;

import java.io.Serializable;

public class StoneInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//
	private String certificateNames;

	//
	private String certificateNo;

	//
	private String colorsNames;

	//
	private String fluorescenceNames;

	//
	private String image;

	//
	private String mountNames;

	//
	private String num;

	//
	private String pavilionDepthPercent;

	//
	private String placeNames;

	//
	private String polishingNames;

	//
	private String sequence;

	private String size;

	private String stoneCarveNames;

	private String stoneColorNames;

	private String stoneNames;

	private String stoneNetNames;

	private String stoneShapeEnNames;

	private String stoneShapeNames;

	private String stoneType;

	private String symmetryNames;

	private String tableWidePercent;

	private String weight;

	public String getCertificateNames() {
		return certificateNames;
	}

	public void setCertificateNames(String certificateNames) {
		this.certificateNames = certificateNames;
	}

	public String getCertificateNo() {
		return certificateNo;
	}

	public void setCertificateNo(String certificateNo) {
		this.certificateNo = certificateNo;
	}

	public String getColorsNames() {
		return colorsNames;
	}

	public void setColorsNames(String colorsNames) {
		this.colorsNames = colorsNames;
	}

	public String getFluorescenceNames() {
		return fluorescenceNames;
	}

	public void setFluorescenceNames(String fluorescenceNames) {
		this.fluorescenceNames = fluorescenceNames;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getMountNames() {
		return mountNames;
	}

	public void setMountNames(String mountNames) {
		this.mountNames = mountNames;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getPavilionDepthPercent() {
		return pavilionDepthPercent;
	}

	public void setPavilionDepthPercent(String pavilionDepthPercent) {
		this.pavilionDepthPercent = pavilionDepthPercent;
	}

	public String getPlaceNames() {
		return placeNames;
	}

	public void setPlaceNames(String placeNames) {
		this.placeNames = placeNames;
	}

	public String getPolishingNames() {
		return polishingNames;
	}

	public void setPolishingNames(String polishingNames) {
		this.polishingNames = polishingNames;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getStoneCarveNames() {
		return stoneCarveNames;
	}

	public void setStoneCarveNames(String stoneCarveNames) {
		this.stoneCarveNames = stoneCarveNames;
	}

	public String getStoneColorNames() {
		return stoneColorNames;
	}

	public void setStoneColorNames(String stoneColorNames) {
		this.stoneColorNames = stoneColorNames;
	}

	public String getStoneNames() {
		return stoneNames;
	}

	public void setStoneNames(String stoneNames) {
		this.stoneNames = stoneNames;
	}

	public String getStoneNetNames() {
		return stoneNetNames;
	}

	public void setStoneNetNames(String stoneNetNames) {
		this.stoneNetNames = stoneNetNames;
	}

	public String getStoneShapeEnNames() {
		return stoneShapeEnNames;
	}

	public void setStoneShapeEnNames(String stoneShapeEnNames) {
		this.stoneShapeEnNames = stoneShapeEnNames;
	}

	public String getStoneShapeNames() {
		return stoneShapeNames;
	}

	public void setStoneShapeNames(String stoneShapeNames) {
		this.stoneShapeNames = stoneShapeNames;
	}

	public String getStoneType() {
		return stoneType;
	}

	public void setStoneType(String stoneType) {
		this.stoneType = stoneType;
	}

	public String getSymmetryNames() {
		return symmetryNames;
	}

	public void setSymmetryNames(String symmetryNames) {
		this.symmetryNames = symmetryNames;
	}

	public String getTableWidePercent() {
		return tableWidePercent;
	}

	public void setTableWidePercent(String tableWidePercent) {
		this.tableWidePercent = tableWidePercent;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

}
