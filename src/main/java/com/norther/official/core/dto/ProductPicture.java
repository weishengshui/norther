package com.norther.official.core.dto;

import java.io.Serializable;

/**
 * 产品图片
 * 
 * @author Administrator
 * 
 */
public class ProductPicture implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//
	private String defaultTag;

	//
	private String file1Path;

	//
	private String file2Path;

	//
	private String file3Path;

	//
	private String materialName;

	//
	private String name;

	//
	private String pictureTypeName;

	//
	private String sequence;

	//
	private String weightIntervalName;

	public String getDefaultTag() {
		return defaultTag;
	}

	public void setDefaultTag(String defaultTag) {
		this.defaultTag = defaultTag;
	}

	public String getFile1Path() {
		return file1Path;
	}

	public void setFile1Path(String file1Path) {
		this.file1Path = file1Path;
	}

	public String getFile2Path() {
		return file2Path;
	}

	public void setFile2Path(String file2Path) {
		this.file2Path = file2Path;
	}

	public String getFile3Path() {
		return file3Path;
	}

	public void setFile3Path(String file3Path) {
		this.file3Path = file3Path;
	}

	public String getMaterialName() {
		return materialName;
	}

	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPictureTypeName() {
		return pictureTypeName;
	}

	public void setPictureTypeName(String pictureTypeName) {
		this.pictureTypeName = pictureTypeName;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public String getWeightIntervalName() {
		return weightIntervalName;
	}

	public void setWeightIntervalName(String weightIntervalName) {
		this.weightIntervalName = weightIntervalName;
	}

}
