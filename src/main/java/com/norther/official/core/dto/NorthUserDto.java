package com.norther.official.core.dto;

import com.norther.official.core.entity.NorthUser;

/**
 * 用户查询 dto
 * 
 * @author Administrator
 * 
 */
public class NorthUserDto extends NorthUser {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// 当前用户
	private String currentAccount;
	
	// 用户状态：null表示查全部、0启用、1停用
	private Integer userStatus;

	public String getCurrentAccount() {
		return currentAccount;
	}

	public void setCurrentAccount(String currentAccount) {
		this.currentAccount = currentAccount;
	}

	public Integer getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(Integer userStatus) {
		this.userStatus = userStatus;
	}

}
