package com.norther.official.core.dto;

import java.util.Date;

import com.norther.official.core.entity.Video;

/**
 * 视频查询条件
 * 
 * @author sean
 * 
 */
public class VideoDto extends Video {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// 发布时间查询区间
	private Date publishDate1;
	private Date publishDate2;

	public Date getPublishDate1() {
		return publishDate1;
	}

	public void setPublishDate1(Date publishDate1) {
		this.publishDate1 = publishDate1;
	}

	public Date getPublishDate2() {
		return publishDate2;
	}

	public void setPublishDate2(Date publishDate2) {
		this.publishDate2 = publishDate2;
	}

}
