package com.norther.official.core.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.norther.official.core.entity.NorthUser;

/**
 * 用户 repository
 * 
 * @author sean
 * 
 */
public interface UserRepository extends BaseRepository<NorthUser, Long> {

	/**
	 * 根据账号密码查询登录用户
	 * 
	 * @param account
	 * @param password
	 * @return
	 */
	@Query("select u from NorthUser u where u.account = ?1 and u.password = ?2 and u.isDelete=0")
	NorthUser findUserLogin(String account, String password);

	/**
	 * 修改用户密码
	 * 
	 * @param id
	 * @param password
	 * @return
	 */
	@Modifying
	@Query("update NorthUser set password = ?2 where id = ?1")
	int updateUserPassword(Long id, String password);

	/**
	 * 逻辑删除用户
	 * 
	 * @param id
	 * @return
	 */
	@Modifying
	@Query("update NorthUser set isDelete = 1 where id int ?1")
	int deleteByIdIn(Long[] ids);

	/**
	 * 根据id查询未删除的用户
	 * 
	 * @param id
	 * @param i
	 * @return
	 */
	NorthUser findByIdAndIsDelete(Long id, int i);
}
