package com.norther.official.core.dao.specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.norther.official.core.dto.NorthUserDto;
import com.norther.official.core.entity.NorthUser;

/**
 * 用户查询条件
 * 
 * @author sean
 * 
 */
public class UserSpecifications {

	/**
	 * 列表查询条件
	 * 
	 * @param dto
	 * @return
	 */
	public static Specification<NorthUser> listQueryCriteria(NorthUserDto dto) {

		return new Specification<NorthUser>() {
			@Override
			public Predicate toPredicate(Root<NorthUser> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				// TODO
				Predicate p = cb.equal(
						root.get("isDelete").type().as(Integer.class), 0);
				return p;
			}
		};
	}
}
