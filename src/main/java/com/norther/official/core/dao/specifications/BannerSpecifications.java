package com.norther.official.core.dao.specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.norther.official.core.dto.BannerDto;
import com.norther.official.core.entity.Banner;

/**
 * banner 查询条件
 * 
 * @author sean
 * 
 */
public class BannerSpecifications {

	/**
	 * 列表查询条件
	 * 
	 * @param dto
	 * @return
	 */
	public static Specification<Banner> listQueryCriteria(BannerDto dto) {

		return new Specification<Banner>() {
			@Override
			public Predicate toPredicate(Root<Banner> root,
					CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p = cb.equal(
						root.get("isDelete").type().as(Integer.class), 0);
				return p;
			}
		};
	}
}
