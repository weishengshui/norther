package com.norther.official.core.dao;

import com.norther.official.core.entity.Banner;

/**
 * banner 图片 repository
 * 
 * @author sean
 * 
 */
public interface BannerRepository extends BaseRepository<Banner, Long> {

	/**
	 * 批量删除轮换图片
	 * 
	 * @param ids
	 * @return 删除的记录数
	 */
	int deleteByIdIn(Long[] ids);
}
