
-- 诺石伙伴
create table partner (
  id bigint NOT NULL identity,
  company_name varchar(255) ,
  thumb_path varchar(255) ,
  address varchar(255) ,
  phone varchar(255) ,
  create_time datetime ,
  update_time datetime ,
  is_delete integer ,
  PRIMARY KEY (id)
) ;

-- 资料表
CREATE TABLE materials (
  id bigint NOT NULL identity,
  title varchar(255) ,
  thumb_path varchar(255),
  user_id bigint ,
  brief text,
  content text,
  publish_date datetime,
  create_time datetime,
  update_time datetime,
  is_delete integer,
  PRIMARY KEY (id)
) ;

-- 视频表
CREATE TABLE video (
  id int identity PRIMARY KEY,
  title varchar(255),
  path varchar(255),
  sort integer,
  publish_date datetime ,
  create_time datetime ,
  update_time datetime ,
  is_delete integer
) ;


-- 分销商
CREATE TABLE agent (
  id bigint NOT NULL identity,
  agent_name varchar(255),
  create_time datetime,
  detail_address varchar(255),
  email varchar(255),
  fax varchar(255),
  is_delete integer,
  legal_person varchar(255),
  linkMan varchar(255),
  mobile varchar(255),
  qq varchar(255),
  telphone varchar(255),
  update_time datetime,
  distcrit_id bigint,
  PRIMARY KEY (id)
) ;

-- 图片轮换
CREATE TABLE banner (
  id bigint NOT NULL identity,
  link varchar(255),
  path varchar(255),
  sort integer,
  title varchar(255),
  create_time datetime,
  update_time datetime,
  is_delete integer,
  PRIMARY KEY (id)
) ;

-- 企业信息
CREATE TABLE company_info (
  id bigint NOT NULL identity,
  title varchar(255),
  info_type varchar(255),
  content text,
  copyright varchar(255),
  record_no varchar(255),
  phone varchar(255),
  mobile varchar(255),
  company_address varchar(255),
  zipcode varchar(255),
  qq varchar(255) ,
  email varchar(255),
  sina_weibo varchar(255),
  weixin varchar(255) ,
  two_dimensional_code varchar(255),
  statistics_code text,
  create_time datetime,
  update_time datetime,
  PRIMARY KEY (id)
) ;

-- 企业动态
CREATE TABLE company_news (
  id bigint NOT NULL identity,
  title varchar(255),
  show_front bigint default 0 ,
  thumb_path varchar(255),
  user_id bigint,
  brief text,
  content text,
  create_time datetime,
  is_delete integer,
  publishdate datetime,
  update_time datetime,
  PRIMARY KEY (id)
) ;

-- 行业新闻
CREATE TABLE industory_news (
  id bigint NOT NULL identity,
  content text,
  create_time datetime,
  is_delete integer,
  publishdate datetime,
  reporter varchar(255),
  title varchar(255),
  update_time datetime,
  PRIMARY KEY (id)
) ;

-- 登录用户
CREATE TABLE north_user (
  id bigint NOT NULL identity,
  password varchar(255),
  type bigint,
  username varchar(255),
  account varchar(255),
  status bigint default 0 ,
  role_id bigint,	
  update_time datetime,
  create_time datetime,
  is_delete integer,
  PRIMARY KEY (id)
) ;

-- 角色
CREATE TABLE north_role (
  id bigint NOT NULL identity,
  role_name varchar(255),
  admin bigint DEFAULT 0 ,
  update_time datetime,
  create_time datetime,
  is_delete integer,
  PRIMARY KEY (id)
) ;

-- 菜单
CREATE TABLE menu_module (
  id bigint NOT NULL,
  menu_name varchar(255),
  url varchar(255),
  sort integer,
  is_leaf bit default 0 ,
  parent_id bigint DEFAULT 0,  
  update_time datetime,
  create_time datetime,
  is_delete integer ,
  type bigint,
  PRIMARY KEY (id)
) ;

-- 角色菜单关系表
CREATE TABLE module_role (
	menu_id bigint,
	role_id bigint,
	PRIMARY KEY (menu_id, role_id)
)	;

-- 店铺
CREATE TABLE shop (
  id bigint NOT NULL identity,
  shop_name varchar(255),
  detail_address varchar(255),
  distcrit_id bigint , 
  market varchar(255),
  telphone varchar(255),
  zipcode varchar(255),
  description varchar(255),
  audit_opinion varchar(255),
  pass bigint ,
  manager_name varchar(255) ,
  mobile_phone varchar(255) ,
  agent_id bigint ,
  create_time datetime,
  update_time datetime ,
  is_delete integer,
  PRIMARY KEY (id),
  FOREIGN KEY (agent_id) REFERENCES agent (id)
) ;

-- 省、市、区 级联表
CREATE TABLE s_province (
  ProvinceID bigint identity NOT NULL,
  ProvinceName varchar(50),
  DateCreated datetime,
  DateUpdated datetime,
  PRIMARY KEY (ProvinceID)
) ;

CREATE TABLE s_city (
  CityID bigint NOT NULL identity,
  CityName varchar(50),
  ZipCode varchar(50),
  ProvinceID bigint,
  DateCreated datetime,
  DateUpdated datetime,
  PRIMARY KEY (CityID)
) ;

CREATE TABLE s_district (
  DistrictID bigint NOT NULL identity,
  DistrictName varchar(50),
  CityID bigint,
  DateCreated datetime,
  DateUpdated datetime,
  PRIMARY KEY (DistrictID)
) ;

